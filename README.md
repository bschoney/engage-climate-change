<!--#  Critique-->
<!--![](Critique/Required/Assets.xcassets/Cq-logo.imageset/Cq-logo.png)-->

<!-- <img src="Critique/Required/Assets.xcassets/Cq-logo.imageset/Cq-logo.png" width="350"> -->

![](screenshots/header.jpg)

## About ##

Engage Climate Change is a full stack project built for Glenn Downing's Software Engineering (CS 373) course at the University of Texas. Designed and written from the ground up, this project was completed over the span of 6 weeks and earned the highest grade out of all projects completed during the summer 2019 semeseter. All data shown on the models' pages are pulled from our API, which is documented here.

## Features ##

- **States Data**
  - See statistics about a state's size, and how that compares to recent data for their recent fossil fuel emissions and renewable energy production. 
- **Legislation Data**
  - View all congressional bills from this century marked as pertaining to environmental protection.
- **Politicians Data**
  - See information about all the different politicians to have sponsored the above bills. This includes information like their party and social media accounts.
- **About**
  - See our group's contributions to the site, as well as the tools we used to make Engage Climate Change.
- **Vizualizations**
  - Information from our API vizualized using tools including D3.
- **Random**
  - Go to a random page from our site, whether it be a state, piece of legislation, or politician.
- **Search**
  - Search for pages on our site by attributes like party, chamber, or name.
  - Note: currently there is better support if you search by state abbreviation (e.g. TX instead of Texas)

## API ##

http://api.engageclimatechange.world/

Endpoints can be found at the above address. Search queries for the results can be found at the [Flask-Restless docs](https://flask-restless.readthedocs.io/en/stable/searchformat.html) to filter and sort results. 

## Support ##

Currently the site is best supported for desktop use. Features may encounter problems on mobile devices.

## Data Sources ##

- [EIA][] used to pull emission data of each States
- [Data USA][] used to pull emission data of each States
- [ProPublica][] used to pull data of States and Politicians
- [GitLab][] used to pull data of Enviornmental Legislations

[EIA]:http://api.eia.gov/
[Data USA]:https://datausa.io/about/api/
[ProPublica]:https://api.propublica.org/
[GitLab]:https://docs.gitlab.com/ce/api/
