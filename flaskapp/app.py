from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
import flask_restless

# Configuration class to establish connection to the database
class Config(object):
	SQLALCHEMY_DATABASE_URI = 'postgresql://engagedb:373Engage@engagedb.cbryqhibnhym.us-east-1.rds.amazonaws.com:5432/engagedb' 
	SQLALCHEMY_TRACK_MODIFICATIONS = False

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
CORS(app)


### --- Database Tables --- ###

class State(db.Model):
	__tablename__ = 'state'
	id = db.Column(db.String(2), primary_key=True)
	statename = db.Column(db.String(16), unique=True)
	pop = db.Column(db.Integer)
	landarea = db.Column(db.Float)
	pop_density = db.Column(db.Integer)
	emissions_rec = db.Column(db.Integer)
	renewables_rec = db.Column(db.Float)
	emissions = db.relationship('Emission', backref='state_id', lazy=True)
	renewables = db.relationship('Renewable', backref='state_id', lazy=True)
	politicians = db.relationship('Politician', backref='state_id', lazy=True)
	bills = db.relationship('Bill', backref='state_id', lazy=True)

class Emission(db.Model):
	__tablename__ = 'emission'
	year = db.Column(db.String(4), primary_key=True)
	state = db.Column(db.String(2), db.ForeignKey('state.id'), primary_key=True)
	data = db.Column(db.Integer)
	
class Renewable(db.Model):
	__tablename__ = 'renewable'
	year_month = db.Column(db.String(6), primary_key=True)
	state = db.Column(db.String(2), db.ForeignKey('state.id'), primary_key=True)
	data = db.Column(db.Integer)

class Politician(db.Model):
	__tablename__ = 'politician'
	id = db.Column(db.String(7), primary_key=True)
	our_id = db.Column(db.Integer, unique=True)
	first_name = db.Column(db.String(15))
	last_name = db.Column(db.String(20))
	dob = db.Column(db.Date)
	party = db.Column(db.String(3))
	recent_vote = db.Column(db.Date)
	in_office = db.Column(db.Boolean)
	facebook = db.Column(db.String(30))
	twitter = db.Column(db.String(30))
	youtube = db.Column(db.String(30))
	url = db.Column(db.String(50))
	state = db.Column(db.String(2), db.ForeignKey('state.id'))
	bills = db.relationship('Bill', backref='politician_id', lazy='dynamic')
	
class Bill(db.Model):
	__tablename__ = 'bill'
	id = db.Column(db.Integer, primary_key=True, unique=True)
	name = db.Column(db.String)
	bill_id = db.Column(db.String(15), primary_key=True)
	govtrack = db.Column(db.String)
	chamber = db.Column(db.String(7))
	introduced = db.Column(db.Date)
	last_vote = db.Column(db.Date)
	latest_action = db.Column(db.Date)
	summary = db.Column(db.String)
	sponsor_party = db.Column(db.String(3))
	state = db.Column(db.String(2), db.ForeignKey('state.id'))
	sponsor = db.Column(db.String(7), db.ForeignKey('politician.id'))


manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)

### --- Home --- ###
@app.route('/')
@app.route('/index')
def index():
    result = "<h3>Endpoints:</h3><br/><p>/states<br/>/states/&lt;state_abbrev&gt;<br/><br/>\
        /politicians<br/>/politicians/&lt;id&gt;<br/><br/>/bills<br/>/bills/&lt;id&gt;<br/><br/><h3>Documentation:</h3>\
        <a href=\"https://documenter.getpostman.com/view/7911561/S1a61kyB?version=latest\">Postman Documentation</a></p>"
    return result


### --- States --- ###
manager.create_api(State, url_prefix='', methods=['GET'], collection_name='states', results_per_page=50, max_results_per_page=200)


### --- Emissions --- ###
manager.create_api(Emission, url_prefix='', methods=['GET'], primary_key='state', collection_name='emissions', results_per_page=200, max_results_per_page=1000)


### --- Renewables --- ###
manager.create_api(Renewable, url_prefix='', methods=['GET'], primary_key='state', collection_name='renewables', results_per_page=200, max_results_per_page=1000)


### --- Politicians --- ###
manager.create_api(Politician, url_prefix='', methods=['GET'], collection_name='politicians', results_per_page=200, max_results_per_page=1000)


### --- Bills --- ###
manager.create_api(Bill, url_prefix='', methods=['GET'], collection_name='bills', results_per_page=200, max_results_per_page=1000)


if __name__ == "__main__":
    app.run(host="0.0.0.0", threaded='True')
