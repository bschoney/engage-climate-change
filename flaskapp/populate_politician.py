import json
from io import FileIO
import psycopg2
from flask_sqlalchemy import SQLAlchemy
from os import path

def populate_politicians(cursor, sponsors_json):
    json_sponsors = json.loads(sponsors_json.read())
    our_id = 0
    for politician in json_sponsors:
        id = politician["member_id"]
        first_name = politician["first_name"]
        last_name = politician["last_name"]
        dob = politician["date_of_birth"]
        party = politician["current_party"]
        recent_vote = politician["most_recent_vote"]
        in_office = politician["in_office"]
        facebook = politician["facebook_account"]
        twitter = politician["twitter_account"]
        youtube = politician["youtube_account"]
        url = politician["url"]
        state = politician["roles"][0]["state"]
        sql_statement = "INSERT INTO  politician(id, our_id, first_name, last_name, dob, party, recent_vote, in_office, facebook, twitter, youtube, url, state) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(sql_statement, (id, our_id, first_name, last_name, dob, party, recent_vote, in_office, facebook, twitter, youtube, url, state))
        our_id += 1
    

if __name__ == "__main__":
    basepath = path.dirname(__file__)
    filepath = path.abspath(path.join(basepath, "..", "backend", "api", "propublica", "all_sponsors.json"))
    bills_json = FileIO(filepath)
    connection = psycopg2.connect(
        user="engagedb",
        password="373Engage",
        host="engagedb.cbryqhibnhym.us-east-1.rds.amazonaws.com",
        port="5432",
        database="engagedb"
    )
    cursor = connection.cursor()
    populate_politicians(cursor, bills_json)
    connection.commit()
    connection.close()
    cursor.close()
    bills_json.close()