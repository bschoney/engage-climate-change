"""empty message

Revision ID: e391fd2b7a7e
Revises: 708a00c433cd, eadeb5690b07
Create Date: 2019-07-07 12:09:01.247504

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e391fd2b7a7e'
down_revision = ('708a00c433cd', 'eadeb5690b07')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
