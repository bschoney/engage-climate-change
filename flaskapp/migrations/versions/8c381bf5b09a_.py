"""empty message

Revision ID: 8c381bf5b09a
Revises: df2b8800e8eb
Create Date: 2019-07-07 13:23:38.257748

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8c381bf5b09a'
down_revision = 'df2b8800e8eb'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('politician',
    sa.Column('id', sa.String(length=7), nullable=False),
    sa.Column('our_id', sa.Integer(), nullable=True),
    sa.Column('first_name', sa.String(length=15), nullable=True),
    sa.Column('last_name', sa.String(length=20), nullable=True),
    sa.Column('dob', sa.Date(), nullable=True),
    sa.Column('party', sa.String(length=3), nullable=True),
    sa.Column('recent_vote', sa.Date(), nullable=True),
    sa.Column('in_office', sa.Boolean(), nullable=True),
    sa.Column('facebook', sa.String(length=30), nullable=True),
    sa.Column('twitter', sa.String(length=30), nullable=True),
    sa.Column('youtube', sa.String(length=30), nullable=True),
    sa.Column('url', sa.String(length=50), nullable=True),
    sa.Column('state', sa.String(length=2), nullable=True),
    sa.ForeignKeyConstraint(['state'], ['state.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('our_id')
    )
    op.create_table('bill',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('bill_id', sa.String(length=11), nullable=False),
    sa.Column('govtrack', sa.String(length=50), nullable=True),
    sa.Column('house_senate', sa.String(), nullable=True),
    sa.Column('introduced', sa.Date(), nullable=True),
    sa.Column('last_vote', sa.Date(), nullable=True),
    sa.Column('summary', sa.String(), nullable=True),
    sa.Column('state', sa.String(length=2), nullable=True),
    sa.Column('sponsor', sa.String(length=7), nullable=True),
    sa.ForeignKeyConstraint(['sponsor'], ['politician.id'], ),
    sa.ForeignKeyConstraint(['state'], ['state.id'], ),
    sa.PrimaryKeyConstraint('id', 'bill_id'),
    sa.UniqueConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('bill')
    op.drop_table('politician')
    # ### end Alembic commands ###
