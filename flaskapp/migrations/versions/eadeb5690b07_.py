"""empty message

Revision ID: eadeb5690b07
Revises: b1676cc007d8
Create Date: 2019-07-06 20:26:35.273234

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'eadeb5690b07'
down_revision = 'b1676cc007d8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('state', sa.Column('statename', sa.String(length=16), nullable=True))
    op.create_unique_constraint(None, 'state', ['statename'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'state', type_='unique')
    op.drop_column('state', 'statename')
    # ### end Alembic commands ###
