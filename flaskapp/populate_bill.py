import json
from io import FileIO
import psycopg2
from os import path

def populate_bills(cursor, bills_json):
    json_bills = json.loads(bills_json.read())
    id = 0
    for bill in json_bills:
        name = bill["short_title"]
        bill_id = bill["bill_id"]
        govtrack = bill["govtrack_url"]
        chamber = bill["bill_type"]
        introduced = bill["introduced_date"]
        last_vote = bill["last_vote"]
        latest_action = bill["latest_major_action_date"]
        summary = bill["summary"]
        sponsor_party = bill["sponsor_party"]
        state = bill["sponsor_state"]
        sponsor = bill["sponsor_id"]
        sql_statement = "INSERT INTO bill(id, name, bill_id, govtrack, chamber, introduced, last_vote, latest_action, summary, sponsor_party, state, sponsor) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(sql_statement, (id, name, bill_id, govtrack, chamber, introduced, last_vote, latest_action, summary, sponsor_party, state, sponsor))
        id += 1


if __name__ == "__main__":
    basepath = path.dirname(__file__)
    filepath = path.abspath(path.join(basepath, "..", "backend", "api", "propublica", "all_bills.json"))
    bills_json = FileIO(filepath)
    connection = psycopg2.connect(
        user="engagedb",
        password="373Engage",
        host="engagedb.cbryqhibnhym.us-east-1.rds.amazonaws.com",
        port="5432",
        database="engagedb"
    )
    cursor = connection.cursor()
    populate_bills(cursor, bills_json)
    connection.commit()
    connection.close()
    cursor.close()
    bills_json.close()