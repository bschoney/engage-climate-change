import json
from io import FileIO
import psycopg2
from flask_sqlalchemy import SQLAlchemy
from os import path

statenames = {
    'AK': 'Alaska',
    'AL': 'Alabama',
    'AR': 'Arkansas',
    'AS': 'American Samoa',
    'AZ': 'Arizona',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DC': 'District of Columbia',
    'DE': 'Delaware',
    'FL': 'Florida',
    'GA': 'Georgia',
    'GU': 'Guam',
    'HI': 'Hawaii',
    'IA': 'Iowa',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'MA': 'Massachusetts',
    'MD': 'Maryland',
    'ME': 'Maine',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MO': 'Missouri',
    'MP': 'Northern Mariana Islands',
    'MS': 'Mississippi',
    'MT': 'Montana',
    'NA': 'National',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'NE': 'Nebraska',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NV': 'Nevada',
    'NY': 'New York',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'PR': 'Puerto Rico',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VA': 'Virginia',
    'VI': 'Virgin Islands',
    'VT': 'Vermont',
    'WA': 'Washington',
    'WI': 'Wisconsin',
    'WV': 'West Virginia',
    'WY': 'Wyoming'
}

def populate_emis(cursor, emissions, id):
    data = emissions["data"]
    for year in data:
        sql_statement = "INSERT INTO emission(year, state, data) VALUES (%s, %s, %s)"
        cursor.execute(sql_statement, (year, id, data[year]))

def populate_renew(cursor, renewables, id):
    data = renewables["data"]
    for year_month in data:
        sql_statement = "INSERT INTO renewable(year_month, state, data) VALUES (%s, %s, %s)"
        cursor.execute(sql_statement, (year_month, id, data[year_month]))

def populate_states_emis_renew(cursor, states_json):
    json_states = json.loads(states_json.read())
    for id in json_states:
        state = json_states[id]
        statename = statenames[id]
        try:
            pop_dens = state["density"]
            emissions = state["emmisions"]
            land_area = state["landArea"]
            pop = state["population"]
            renew = state["renewable"]
            # If AWS provides enough free space for summary, we will add it
            # summary = state["summary"]
            emissions_rec = emissions["data"][emissions["end"]]
            renew_rec = renew["data"][renew["end"]]
            sql_statement = "INSERT INTO  state(id, statename, pop, landarea, pop_density, emissions_rec, renewables_rec) VALUES (%s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(sql_statement, (id, statename, pop, land_area, pop_dens, emissions_rec, renew_rec))
            populate_emis(cursor, emissions, id)
            populate_renew(cursor, renew, id)
        except KeyError as k:
            print("Key error for {}, {}".format(id, k.args))


if __name__ == "__main__":
    basepath = path.dirname(__file__)
    filepath = path.abspath(path.join(basepath, "..", "backend", "api", "eia-emissions", "states.json"))
    states_json = FileIO(filepath)
    connection = psycopg2.connect(
        user="engagedb",
        password="373Engage",
        host="engagedb.cbryqhibnhym.us-east-1.rds.amazonaws.com",
        port="5432",
        database="engagedb"
    )
    cursor = connection.cursor()
    populate_states_emis_renew(cursor, states_json)
    connection.commit()
    connection.close()
    cursor.close()
    states_json.close()