var assert = require('assert');

describe('Array', function() {
  describe ('#indexOf', function() {
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});

describe('getEnvironmental_Legislation', function() {
	describe('getEnvironmental_Legislation()', function() {
		it('returns a list of the environmental legislation', function(){
			assert.equal(-1, [1,2,3].indexOf(4));
		});
	});
});

describe('getStates', function() {
	describe('getStates()', function() {
		it('returns a list of the states', function(){
			assert.equal(-1, [1,2,3].indexOf(4));
		});
	});
});

describe('getPoliticians', function() {
	describe('getPoliticians()', function() {
		it('returns a list of the politicians', function(){
			assert.equal(-1, [1,2,3].indexOf(4));
		});
	});
});
