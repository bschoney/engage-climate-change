import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

class countries_1 extends React.Component {
  render() {
    return (
    		<div>
				<div className="page-header" align="center">
					<h1> Texas </h1>
					<div class="container">
					<img src="http://www.orangesmile.com/common/img_city_maps/texas-map-0.jpg" height="500" w="500"/>
				</div>
				</div>
				<div className="h6">
					<p class="p-5"> Texas is the second largest state in the United States by both area and population. Geographically located in the South Central region of the country, Texas shares borders with the U.S. states of Louisiana to the east, Arkansas to the northeast, Oklahoma to the north, New Mexico to the west, and the Mexican states of Chihuahua, Coahuila, Nuevo León, and Tamaulipas to the southwest, and has a coastline with the Gulf of Mexico to the southeast.
					Houston is the most populous city in Texas and the fourth largest in the U.S., while San Antonio is the second-most populous in the state and seventh largest in the U.S. Dallas–Fort Worth and Greater Houston are the fourth and fifth largest metropolitan statistical areas in the country, respectively. Other major cities include Austin, the second-most populous state capital in the U.S., and El Paso. Texas is nicknamed "The Lone Star State" to signify its former status as an independent republic, and as a reminder of the state's struggle for independence from Mexico. The "Lone Star" can be found on the Texas state flag and on the Texan state seal. The origin of Texas's name is from the word taysha, which means "friends" in the Caddo language. Due to its size and geologic features such as the Balcones Fault, Texas contains diverse landscapes common to both the U.S. Southern and Southwestern regions. Although Texas is popularly associated with the U.S. southwestern deserts, less than 10% of Texas's land area is desert. Most of the population centers are in areas of former prairies, grasslands, forests, and the coastline. Traveling from east to west, one can observe terrain that ranges from coastal swamps and piney woods, to rolling plains and rugged hills, and finally the desert and mountains of the Big Bend.
					The term "six flags over Texas" refers to several nations that have ruled over the territory. Spain was the first European country to claim the area of Texas. France held a short-lived colony. Mexico controlled the territory until 1836 when Texas won its independence, becoming an independent Republic. In 1845, Texas joined the union as the 28th state. The state's annexation set off a chain of events that led to the Mexican–American War in 1846. A slave state before the American Civil War, Texas declared its secession from the U.S. in early 1861, and officially joined the Confederate States of America on March 2nd of the same year. After the Civil War and the restoration of its representation in the federal government, Texas entered a long period of economic stagnation. Historically four major industries shaped the Texas economy prior to World War II: cattle and bison, cotton, timber, and oil.[15] Before and after the U.S. Civil War the cattle industry, which Texas came to dominate, was a major economic driver for the state, thus creating the traditional image of the Texas cowboy. In the later 19th century cotton and lumber grew to be major industries as the cattle industry became less lucrative. It was ultimately, though, the discovery of major petroleum deposits (Spindletop in particular) that initiated an economic boom which became the driving force behind the economy for much of the 20th century. With strong investments in universities, Texas developed a diversified economy and high tech industry in the mid-20th century. As of 2015, it is second on the list of the most Fortune 500 companies with 54. With a growing base of industry, the state leads in many industries, including agriculture, petrochemicals, energy, computers and electronics, aerospace, and biomedical sciences. Texas has led the U.S. in state export revenue since 2002, and has the second-highest gross state product. If Texas were a sovereign state, it would be the 10th largest economy in the world.
					</p>
				</div>	
				<p class="p-5">
				<table class="table table-bordered table-hover table-striped table-responsive">
<thead><tr><th title="Field #1">Year</th>
<th title="Field #2">Energy Usage (Billions of Btu)</th>
</tr></thead>
<tbody><tr>
<td align="right">2017</td>
<td align="right">11959352</td>
</tr>
<tr>
<td align="right">2016</td>
<td align="right">11798391</td>
</tr>
<tr>
<td align="right">2015</td>
<td align="right">11784910</td>
</tr>
<tr>
<td align="right">2014</td>
<td align="right">11584160</td>
</tr>
<tr>
<td align="right">2013</td>
<td align="right">11675647</td>
</tr>
<tr>
<td align="right">2012</td>
<td align="right">11065703</td>
</tr>
<tr>
<td align="right">2011</td>
<td align="right">10954666</td>
</tr>
<tr>
<td align="right">2010</td>
<td align="right">10729853</td>
</tr>
<tr>
<td align="right">2009</td>
<td align="right">10044502</td>
</tr>
<tr>
<td align="right">2008</td>
<td align="right">10504596</td>
</tr>
<tr>
<td align="right">2007</td>
<td align="right">11152821</td>
</tr>
<tr>
<td align="right">2006</td>
<td align="right">11241286</td>
</tr>
<tr>
<td align="right">2005</td>
<td align="right">11236817</td>
</tr>
<tr>
<td align="right">2004</td>
<td align="right">11827486</td>
</tr>
<tr>
<td align="right">2003</td>
<td align="right">11760081</td>
</tr>
<tr>
<td align="right">2002</td>
<td align="right">11919734</td>
</tr>
<tr>
<td align="right">2001</td>
<td align="right">11649516</td>
</tr>
<tr>
<td align="right">2000</td>
<td align="right">11865411</td>
</tr>
<tr>
<td align="right">1999</td>
<td align="right">11510775</td>
</tr>
<tr>
<td align="right">1998</td>
<td align="right">11800982</td>
</tr>
<tr>
<td align="right">1997</td>
<td align="right">11617815</td>
</tr>
<tr>
<td align="right">1996</td>
<td align="right">11251918</td>
</tr>
<tr>
<td align="right">1995</td>
<td align="right">10523810</td>
</tr>
<tr>
<td align="right">1994</td>
<td align="right">10423994</td>
</tr>
<tr>
<td align="right">1993</td>
<td align="right">10283095</td>
</tr>
<tr>
<td align="right">1992</td>
<td align="right">10078666</td>
</tr>
<tr>
<td align="right">1991</td>
<td align="right">9939375</td>
</tr>
<tr>
<td align="right">1990</td>
<td align="right">9968710</td>
</tr>
<tr>
<td align="right">1989</td>
<td align="right">9873259</td>
</tr>
<tr>
<td align="right">1988</td>
<td align="right">9550036</td>
</tr>
<tr>
<td align="right">1987</td>
<td align="right">9009783</td>
</tr>
<tr>
<td align="right">1986</td>
<td align="right">8799021</td>
</tr>
<tr>
<td align="right">1985</td>
<td align="right">8854790</td>
</tr>
<tr>
<td align="right">1984</td>
<td align="right">8829428</td>
</tr>
<tr>
<td align="right">1983</td>
<td align="right">8347875</td>
</tr>
<tr>
<td align="right">1982</td>
<td align="right">8454577</td>
</tr>
<tr>
<td align="right">1981</td>
<td align="right">9178576</td>
</tr>
<tr>
<td align="right">1980</td>
<td align="right">9412323</td>
</tr>
<tr>
<td align="right">1979</td>
<td align="right">9280460</td>
</tr>
<tr>
<td align="right">1978</td>
<td align="right">8893812</td>
</tr>
<tr>
<td align="right">1977</td>
<td align="right">8428442</td>
</tr>
<tr>
<td align="right">1976</td>
<td align="right">7782303</td>
</tr>
<tr>
<td align="right">1975</td>
<td align="right">7463788</td>
</tr>
<tr>
<td align="right">1974</td>
<td align="right">8029803</td>
</tr>
<tr>
<td align="right">1973</td>
<td align="right">8167653</td>
</tr>
<tr>
<td align="right">1972</td>
<td align="right">7577336</td>
</tr>
<tr>
<td align="right">1971</td>
<td align="right">7179205</td>
</tr>
<tr>
<td align="right">1970</td>
<td align="right">6844936</td>
</tr>
<tr>
<td align="right">1969</td>
<td align="right">6590431</td>
</tr>
<tr>
<td align="right">1968</td>
<td align="right">6207978</td>
</tr>
<tr>
<td align="right">1967</td>
<td align="right">5837764</td>
</tr>
<tr>
<td align="right">1966</td>
<td align="right">5510829</td>
</tr>
<tr>
<td align="right">1965</td>
<td align="right">5173884</td>
</tr>
<tr>
<td align="right">1964</td>
<td align="right">5000292</td>
</tr>
<tr>
<td align="right">1963</td>
<td align="right">4876572</td>
</tr>
<tr>
<td align="right">1962</td>
<td align="right">4614014</td>
</tr>
<tr>
<td align="right">1961</td>
<td align="right">4405968</td>
</tr>
<tr>
<td align="right">1960</td>
<td align="right">4376513</td>
</tr>
</tbody></table></p>
				<p class="p-5">
					Links<br></br>
					<Link to="/Environmental_Legislation/Wasteful_EPA_Programs_Elimination_Act_of_2017">Wasteful EPA Programs Elimination Act of 2017</Link><br></br>
					<Link to="/Politicians/Sam_Johnson">Sam Johnson</Link><br></br>
					<a href="https://texas.gov/">Texas Government Website</a>
				</p>
			</div>

	)
}
}

export default countries_1