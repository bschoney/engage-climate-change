import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

class countries_2 extends React.Component {
  render() {
    return (
    	<div>
				<div className="page-header" align="center">
					<h1> Carbon Pollution Transparency Act of 2014 </h1>
					<div class="container">
					<img src="https://cdn3.iconfinder.com/data/icons/industry-soft/512/cloudy_online_weather_server_web_sky_cloud_computing-512.png" height="500" w="500"/>
				</div>
				</div>
				<p class="p-5">
				 	Carbon Pollution Transparency Act of 2014 - Requires the Director of the Congressional Budget Office (CBO) to calculate a carbon score for legislation. Requires the score to include projected net greenhouse gas emissions that would result from enactment and implementation of a bill or resolution and the appropriation of any amounts authorized in the legislation. Directs CBO to include the carbon score when carrying out provisions of the Congressional Budget Act of 1974 requiring a cost estimate for each bill or resolution reported by any congressional committee except the appropriations committees.	
				</p>
				<p class="p-5">
					Links<br></br>
					<Link to="/States/Vermont">Vermont</Link><br></br>
					<Link to="/Politicians/Bernard_Sanders">Bernard Sanders</Link><br></br>
					<a href="https://www.congress.gov/bill/113th-congress/senate-bill/2905/text">Act's Information Source I</a><br></br>
					<a href="https://projects.propublica.org/represent/bills/113/s2905">Act's Information Source II</a><br></br>
				</p>
		</div>
	)
}
}

export default countries_2