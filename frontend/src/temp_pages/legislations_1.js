import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

class countries_1 extends React.Component {
  render() {
    return (
    	<div>
    			<div className="page-header" align="center">
					<h1> Wasteful EPA Programs Elimination Act of 2017 </h1>
					<div class="container">
					<img src="https://environblog.jenner.com/.a/6a01310fa9d1ee970c01b7c8b9d74f970b-320wi" height="500" w="500"/>
				</div>
				</div>
				<p class="p-5">
				 	This bill terminates all existing grant programs of the Environmental Protection Agency (EPA), its National Clean Diesel Campaign, and its environmental justice programs. The EPA may not establish new grant programs. Federal funds may not be used by the EPA: to implement any ozone standard promulgated after this bill's enactment date; to regulate greenhouse gas emissions from mobile sources, or from fossil fuel-fired electric utility generating units; for the Greenhouse Gas Reporting Program; for the Global Methane Initiative; for the Climate Resilience Fund; for the Climate Resilience Evaluation Awareness Tool; for the Green Infrastructure Program; for the Climate Ready Water Utilities Initiative; or for climate research at the EPA's Office of Research and Development. The EPA must: (1) discontinue operation and maintenance of its field offices and activities carried out through those offices, and (2) dispose of or lease any underutilized property.
				</p>
				<p class="p-5">
					Links<br></br>
					<Link to="/Politicians/Sam_Johnson">Sam Johnson</Link><br></br>
					<Link to="/States/Texas">Texas</Link><br></br>
					<a href="https://www.congress.gov/bill/115th-congress/house-bill/958">Act's Information Source I</a><br></br>
					<a href="https://en.wikipedia.org/wiki/United_States_Environmental_Protection_Agency">Act's Information Source II</a><br></br>
				</p>
		</div>
	)
}
}

export default countries_1