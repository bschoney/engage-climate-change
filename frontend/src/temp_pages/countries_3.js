import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

class countries_3 extends React.Component {
  render() {
    return (
    		<div>
				<div className="page-header" align="center">
					<h1> California </h1>
					<div class="container">
					<img src="https://geology.com/cities-map/map-of-california-cities.gif" height="500" w="500"/>
				</div>
				</div>
				<div className="h6">
					<p class="p-5"> California is a state in the Pacific Region of the United States. With 39.6 million residents, California is the most populous U.S. state and the third-largest by area. The state capital is Sacramento. The Greater Los Angeles Area and the San Francisco Bay Area are the nation's second- and fifth-most populous urban regions, with 18.7 million and 9.7 million residents respectively. Los Angeles is California's most populous city, and the country's second-most populous, after New York City. California also has the nation's most populous county, Los Angeles County, and its largest county by area, San Bernardino County. The City and County of San Francisco is both the country's second-most densely populated major city after New York City and the fifth-most densely populated county, behind only four of the five New York City boroughs. California's $3.0 trillion economy is larger than that of any other state; larger than those of Texas and Florida combined; and is the largest sub-national economy in the world. If it were a country, California would be the fifth-largest economy in the world (larger than the United Kingdom, France, or India), and the 36th-most populous as of 2017. The Greater Los Angeles Area and the San Francisco Bay Area are the nation's second- and third-largest urban economies ($1.253 trillion and $907 billion respectively as of 2017), after the New York metropolitan area.[16] The San Francisco Bay Area PSA had the nation's highest GDP per capita in 2017 (~$94,000) among large PSAs, and is home to three of the world's ten largest companies by market capitalization and four of the world's ten richest people. California culture is considered a global trendsetter in popular culture, innovation, environmentalism, and politics. As a result of the state's diversity and migration, California integrates foods, languages, and traditions from other areas across the country and around the globe. It is considered the origin of the American film industry, the hippie counterculture, fast food, beach and car culture, the Internet, and the personal computer, among others. The San Francisco Bay Area and the Greater Los Angeles Area are widely seen as global centers of the technology and entertainment industries, respectively. California has a very diverse economy: 58% of the state's economy is centered on finance, government, real estate services, technology, and professional, scientific, and technical business services. Although it accounts for only 1.5% of the state's economy, California's agriculture industry has the highest output of any U.S. state. California is bordered by Oregon to the north, Nevada and Arizona to the east, and the Mexican state of Baja California to the south (with the coast being on the west). The state's diverse geography ranges from the Pacific Coast in the west to the Sierra Nevada mountain range in the east, and from the redwood–Douglas fir forests in the northwest to the Mojave Desert in the southeast. The Central Valley, a major agricultural area, dominates the state's center. Although California is well-known for its warm Mediterranean climate, the large size of the state results in climates that vary from moist temperate rainforest in the north to arid desert in the interior, as well as snowy alpine in the mountains. Over time, drought and wildfires have become more pervasive features. What is now California was first settled by various Native Californian tribes before being explored by a number of European expeditions during the 16th and 17th centuries. The Spanish Empire then claimed it as part of Alta California, within its New Spain colony. The area became a part of Mexico in 1821 following its successful war for independence but was ceded to the United States in 1848 after the Mexican–American War. The western portion of Alta California was then organized and admitted as the 31st state on September 9, 1850. The California Gold Rush starting in 1848 led to dramatic social and demographic changes, with large-scale emigration from the east and abroad with an accompanying economic boom.
					</p>
				</div>
				<p class="p-5">
				<table class="table table-bordered table-hover table-striped table-responsive">
<thead><tr><th title="Field #1">Year</th>
<th title="Field #2">Energy Usage (Billions of Btu)</th>
</tr></thead>
<tbody><tr>
<td align="right">2017</td>
<td align="right">5756484</td>
</tr>
<tr>
<td align="right">2016</td>
<td align="right">5752565</td>
</tr>
<tr>
<td align="right">2015</td>
<td align="right">5779840</td>
</tr>
<tr>
<td align="right">2014</td>
<td align="right">5686840</td>
</tr>
<tr>
<td align="right">2013</td>
<td align="right">5747884</td>
</tr>
<tr>
<td align="right">2012</td>
<td align="right">5688395</td>
</tr>
<tr>
<td align="right">2011</td>
<td align="right">5550601</td>
</tr>
<tr>
<td align="right">2010</td>
<td align="right">5740968</td>
</tr>
<tr>
<td align="right">2009</td>
<td align="right">5853711</td>
</tr>
<tr>
<td align="right">2008</td>
<td align="right">6089199</td>
</tr>
<tr>
<td align="right">2007</td>
<td align="right">6329136</td>
</tr>
<tr>
<td align="right">2006</td>
<td align="right">6229474</td>
</tr>
<tr>
<td align="right">2005</td>
<td align="right">6112977</td>
</tr>
<tr>
<td align="right">2004</td>
<td align="right">6201938</td>
</tr>
<tr>
<td align="right">2003</td>
<td align="right">5906618</td>
</tr>
<tr>
<td align="right">2002</td>
<td align="right">6093048</td>
</tr>
<tr>
<td align="right">2001</td>
<td align="right">6167411</td>
</tr>
<tr>
<td align="right">2000</td>
<td align="right">6097915</td>
</tr>
<tr>
<td align="right">1999</td>
<td align="right">5872880</td>
</tr>
<tr>
<td align="right">1998</td>
<td align="right">5800202</td>
</tr>
<tr>
<td align="right">1997</td>
<td align="right">5559875</td>
</tr>
<tr>
<td align="right">1996</td>
<td align="right">5478197</td>
</tr>
<tr>
<td align="right">1995</td>
<td align="right">5507380</td>
</tr>
<tr>
<td align="right">1994</td>
<td align="right">5724170</td>
</tr>
<tr>
<td align="right">1993</td>
<td align="right">5453415</td>
</tr>
<tr>
<td align="right">1992</td>
<td align="right">5631472</td>
</tr>
<tr>
<td align="right">1991</td>
<td align="right">5552595</td>
</tr>
<tr>
<td align="right">1990</td>
<td align="right">5691323</td>
</tr>
<tr>
<td align="right">1989</td>
<td align="right">5682018</td>
</tr>
<tr>
<td align="right">1988</td>
<td align="right">5426744</td>
</tr>
<tr>
<td align="right">1987</td>
<td align="right">5357699</td>
</tr>
<tr>
<td align="right">1986</td>
<td align="right">4820130</td>
</tr>
<tr>
<td align="right">1985</td>
<td align="right">5096089</td>
</tr>
<tr>
<td align="right">1984</td>
<td align="right">4990077</td>
</tr>
<tr>
<td align="right">1983</td>
<td align="right">4612076</td>
</tr>
<tr>
<td align="right">1982</td>
<td align="right">4708742</td>
</tr>
<tr>
<td align="right">1981</td>
<td align="right">5236651</td>
</tr>
<tr>
<td align="right">1980</td>
<td align="right">5434336</td>
</tr>
<tr>
<td align="right">1979</td>
<td align="right">5702873</td>
</tr>
<tr>
<td align="right">1978</td>
<td align="right">5368647</td>
</tr>
<tr>
<td align="right">1977</td>
<td align="right">5550607</td>
</tr>
<tr>
<td align="right">1976</td>
<td align="right">5161100</td>
</tr>
<tr>
<td align="right">1975</td>
<td align="right">4970244</td>
</tr>
<tr>
<td align="right">1974</td>
<td align="right">4870257</td>
</tr>
<tr>
<td align="right">1973</td>
<td align="right">5269429</td>
</tr>
<tr>
<td align="right">1972</td>
<td align="right">5085475</td>
</tr>
<tr>
<td align="right">1971</td>
<td align="right">4971734</td>
</tr>
<tr>
<td align="right">1970</td>
<td align="right">4805642</td>
</tr>
<tr>
<td align="right">1969</td>
<td align="right">4707981</td>
</tr>
<tr>
<td align="right">1968</td>
<td align="right">4647951</td>
</tr>
<tr>
<td align="right">1967</td>
<td align="right">4354360</td>
</tr>
<tr>
<td align="right">1966</td>
<td align="right">4253418</td>
</tr>
<tr>
<td align="right">1965</td>
<td align="right">3966722</td>
</tr>
<tr>
<td align="right">1964</td>
<td align="right">3898432</td>
</tr>
<tr>
<td align="right">1963</td>
<td align="right">3585286</td>
</tr>
<tr>
<td align="right">1962</td>
<td align="right">3402431</td>
</tr>
<tr>
<td align="right">1961</td>
<td align="right">3352257</td>
</tr>
<tr>
<td align="right">1960</td>
<td align="right">3174641</td>
</tr>
</tbody></table></p>
				<p class="p-5">
					Links<br></br>
					<Link to="/Environmental_Legislation/Water_in_the_21st_Century_Act">Water in the 21st Century Act</Link><br></br>
					<Link to="/Politicians/Grace_F_Napolitano">Grace F. Napolitano</Link><br></br>
					<a href="https://www.ca.gov/">California Government Website</a>
				</p>
			</div>
	)
}
}

export default countries_3