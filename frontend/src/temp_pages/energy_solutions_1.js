import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import {Timeline} from 'react-twitter-widgets'

class energy_solutions_1 extends React.Component {
  render() {
    return (
    		<div>
				<div className="page-header" align="center">
					<h1> Sam Johnson </h1>
				</div>
				<p class="p-5">
					Party: Republican
				</p>
				<div class="container">
					<img src="https://www.govtrack.us/static/legislator-photos/400206-200px.jpeg" alt="sam"/>
				</div>
				<p class="p-5">
					Samuel Robert Johnson (born October 11, 1930) is the former U.S. Representative for <Link to="/States/Texas">Texas</Link>'s 3rd congressional district serving in Congress from 1991 to 2019. He is a member of the Republican Party. In October–November 2015, he was the acting Chairman of the House Committee on Ways and Means, where he also served as chairman of the Social Security Subcommittee. Johnson is also a retired United States Air Force Colonel and was a decorated fighter pilot in both the Korean War and the Vietnam War where in the latter he was an American prisoner of war in North Vietnam for nearly seven years. On January 6, 2017, Johnson announced he would not run for reelection in 2018. After the death of Louise Slaughter in March of 2018, he became the oldest sitting member of the U.S. House of Representatives. He is the last Korean War veteran to serve in Congress.
				</p>
        <div>
          <Timeline dataSource={{sourceType:"profile", screenName:"SamsPressShop"}}
            options={{username:"SamJohnson", height:"400"}}/>
        </div>
				<p class="p-5">
					Links<br></br>
					<Link to="/States/Texas">Texas</Link><br></br>
					<Link to="/Environmental_Legislation/Wasteful_EPA_Programs_Elimination_Act_of_2017">Wasteful EPA Programs Elimination Act of 2017</Link>

				</p>


			</div>
	)
}
}

export default energy_solutions_1
