import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

class countries_2 extends React.Component {
  render() {
    return (
    		<div>
				<div className="page-header" align="center">
					<h1> Vermont </h1>
					<div class="container">
					<img src="https://www.visit-vermont.com/adservimage/4826.jpg" height="500" w="500"/>
				</div>
				</div>
				<div className="h6">
					<p class="p-5">Vermont is a state in the New England region of the northeastern United States. It borders the U.S. states of Massachusetts to the south, New Hampshire to the east, New York to the west, and the Canadian province of Quebec to the north. Vermont is the second-smallest by population and the sixth-smallest by area of the 50 U.S. states. The state capital is Montpelier, the least populous state capital in the United States. The most populous city, Burlington, is the least populous city to be the most populous city in a state. As of 2015, Vermont was the leading producer of maple syrup in the United States. In crime statistics, it was ranked since 2016 as the safest state in the country. For some 12,000 years indigenous peoples inhabited this area. The historic, competitive tribes known as the Algonquian-speaking Abenaki and Iroquoian-speaking Mohawk were active in the area at the time of European encounter. During the 17th century French colonists "claimed" the territory as part of France's colony of New France. After England began to settle colonies to the south along the Atlantic coast, the two nations carried out their competition in North America as well as Europe. For years each enlisted Native American allies in continued raiding and warfare between the New England and New France colonies. This produced an active trade in captives taken during such raids and held for ransom. Some captives were adopted by families into the Mohawk or Abenaki tribes.
After being defeated in 1763 in the Seven Years' War, France ceded its territory east of the Mississippi River to Great Britain. Thereafter, the nearby British colonies, especially the provinces of New Hampshire and New York, disputed the extent of the area called the New Hampshire Grants to the west of the Connecticut River, encompassing present-day Vermont. The provincial government of New York sold land grants to settlers in the region, which conflicted with earlier grants from the government of New Hampshire. The Green Mountain Boys militia protected the interests of the established New Hampshire land grant settlers against the newly arrived settlers with land titles granted by New York. Ultimately, a group of settlers with New Hampshire land grant titles established the Vermont Republic in 1777 as an independent state during the American Revolutionary War. The Vermont Republic partially abolished slavery before any of the other states.
Vermont was admitted to the newly established United States as the fourteenth state in 1791. Vermont is one of only four U.S. states that were previously sovereign states (along with California, Hawaii, Texas). During the mid 19th century, Vermont was a strong source of abolitionist sentiment, but it was also tied to King Cotton through the development of textile mills in the region, which relied on southern cotton. It sent a significant contingent of soldiers to participate in the American Civil War. In the 21st century, Protestants (30%) and Catholics (22%) make up the majority of those reporting a religious preference, with 37% reporting no religion. Other religions individually contribute no more than 2% to the total.
The geography of the state is marked by the Green Mountains, which run north–south up the middle of the state, separating Lake Champlain and other valley terrain on the west from the Connecticut River valley that defines much of its eastern border. A majority of its terrain is forested with hardwoods and conifers. A majority of its open land is in agriculture. The state's climate is characterized by warm, humid summers and cold, snowy winters. Vermont's economic activity of $26 billion in 2010 ranks it as 34th in gross state product. It has been assessed by as 42nd as a state in which to do business. In 1960, Vermonters' politics started to shift from being reliably Republican toward favoring more liberal and progressive candidates. Starting in 1963, voters have alternated between choosing Republican and Democratic governors; since 2007, they have sent only Democrats (or independents) to Congress. Since 1992, voters have consistently chosen Democrats for president. In 2000, the state legislature was the first to recognize civil unions for same-sex couples. In 2011-2012, the state officially recognized four Abenaki tribes.
					</p>
					</div>
					<p class="p-5">
					<table class="table table-bordered table-hover table-striped table-responsive">
<thead><tr><th title="Field #1">Year</th>
<th title="Field #2">Energy Usage (Billions of Btu)</th>
</tr></thead>
<tbody><tr>
<td align="right">2017</td>
<td align="right">90555</td>
</tr>
<tr>
<td align="right">2016</td>
<td align="right">91096</td>
</tr>
<tr>
<td align="right">2015</td>
<td align="right">94876</td>
</tr>
<tr>
<td align="right">2014</td>
<td align="right">91086</td>
</tr>
<tr>
<td align="right">2013</td>
<td align="right">89049</td>
</tr>
<tr>
<td align="right">2012</td>
<td align="right">84104</td>
</tr>
<tr>
<td align="right">2011</td>
<td align="right">89055</td>
</tr>
<tr>
<td align="right">2010</td>
<td align="right">90384</td>
</tr>
<tr>
<td align="right">2009</td>
<td align="right">93209</td>
</tr>
<tr>
<td align="right">2008</td>
<td align="right">86120</td>
</tr>
<tr>
<td align="right">2007</td>
<td align="right">95664</td>
</tr>
<tr>
<td align="right">2006</td>
<td align="right">96814</td>
</tr>
<tr>
<td align="right">2005</td>
<td align="right">98961</td>
</tr>
<tr>
<td align="right">2004</td>
<td align="right">104050</td>
</tr>
<tr>
<td align="right">2003</td>
<td align="right">95538</td>
</tr>
<tr>
<td align="right">2002</td>
<td align="right">92913</td>
</tr>
<tr>
<td align="right">2001</td>
<td align="right">97172</td>
</tr>
<tr>
<td align="right">2000</td>
<td align="right">98638</td>
</tr>
<tr>
<td align="right">1999</td>
<td align="right">93914</td>
</tr>
<tr>
<td align="right">1998</td>
<td align="right">90926</td>
</tr>
<tr>
<td align="right">1997</td>
<td align="right">97538</td>
</tr>
<tr>
<td align="right">1996</td>
<td align="right">92090</td>
</tr>
<tr>
<td align="right">1995</td>
<td align="right">87767</td>
</tr>
<tr>
<td align="right">1994</td>
<td align="right">87952</td>
</tr>
<tr>
<td align="right">1993</td>
<td align="right">88758</td>
</tr>
<tr>
<td align="right">1992</td>
<td align="right">90058</td>
</tr>
<tr>
<td align="right">1991</td>
<td align="right">84845</td>
</tr>
<tr>
<td align="right">1990</td>
<td align="right">78899</td>
</tr>
<tr>
<td align="right">1989</td>
<td align="right">83872</td>
</tr>
<tr>
<td align="right">1988</td>
<td align="right">84325</td>
</tr>
<tr>
<td align="right">1987</td>
<td align="right">81136</td>
</tr>
<tr>
<td align="right">1986</td>
<td align="right">74788</td>
</tr>
<tr>
<td align="right">1985</td>
<td align="right">75409</td>
</tr>
<tr>
<td align="right">1984</td>
<td align="right">71922</td>
</tr>
<tr>
<td align="right">1983</td>
<td align="right">63409</td>
</tr>
<tr>
<td align="right">1982</td>
<td align="right">58667</td>
</tr>
<tr>
<td align="right">1981</td>
<td align="right">64026</td>
</tr>
<tr>
<td align="right">1980</td>
<td align="right">66118</td>
</tr>
<tr>
<td align="right">1979</td>
<td align="right">76412</td>
</tr>
<tr>
<td align="right">1978</td>
<td align="right">82476</td>
</tr>
<tr>
<td align="right">1977</td>
<td align="right">82736</td>
</tr>
<tr>
<td align="right">1976</td>
<td align="right">83253</td>
</tr>
<tr>
<td align="right">1975</td>
<td align="right">73727</td>
</tr>
<tr>
<td align="right">1974</td>
<td align="right">76195</td>
</tr>
<tr>
<td align="right">1973</td>
<td align="right">84606</td>
</tr>
<tr>
<td align="right">1972</td>
<td align="right">82587</td>
</tr>
<tr>
<td align="right">1971</td>
<td align="right">78672</td>
</tr>
<tr>
<td align="right">1970</td>
<td align="right">78701</td>
</tr>
<tr>
<td align="right">1969</td>
<td align="right">74220</td>
</tr>
<tr>
<td align="right">1968</td>
<td align="right">71202</td>
</tr>
<tr>
<td align="right">1967</td>
<td align="right">67165</td>
</tr>
<tr>
<td align="right">1966</td>
<td align="right">67388</td>
</tr>
<tr>
<td align="right">1965</td>
<td align="right">61616</td>
</tr>
<tr>
<td align="right">1964</td>
<td align="right">52305</td>
</tr>
<tr>
<td align="right">1963</td>
<td align="right">55871</td>
</tr>
<tr>
<td align="right">1962</td>
<td align="right">55491</td>
</tr>
<tr>
<td align="right">1961</td>
<td align="right">54368</td>
</tr>
<tr>
<td align="right">1960</td>
<td align="right">50129</td>
</tr>
</tbody></table></p>
				<p class="p-5">
					Links<br></br>
					<Link to="/Politicians/Bernard_Sanders">Bernard Sanders</Link><br></br>
					<Link to="/Environmental_Legislation/Carbon_Pollution_Transparency_Act_of_2014">Carbon Pollution Transparency Act of 2014</Link><br></br>
					<a href="https://www.vermont.gov/">Vermont Government Website</a>

				</p>
			</div>
	)
}
}

export default countries_2