import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import {Timeline} from 'react-twitter-widgets'

class energy_solutions_3 extends React.Component {
  render() {
    return (
    	<div>
				<div className="page-header" align="center">
					<h1> Grace F. Napolitano </h1>
				</div>
				<p class="p-5">
					Party: Democrat
				</p>
				<div class="container">
					<img src="https://www.congress.gov/img/member/114_rp_ca_32_napolitano_grace.jpg" alt="sam"/>
				</div>
				<p class="p-5">
					Graciela Flores "Grace" Napolitano born December 4, 1936) is the U.S. Representative for <Link to="/States/California">California</Link>'s 32nd congressional district, serving in Congress since 1999. She is a member of the Democratic Party. She previously served in the California State Assembly and the Norwalk City Council.
Napolitano previously represented the 34th district from 1999 to 2003, and the 38th district from 2003 to 2013. Due to redistricting, Napolitano ran for, and won re-election in the 2012 United States elections in California's 32nd congressional district against Republican candidate David Miller. In the 2014 midterm elections, Napolitano was reelected, defeating Republican challenger Arturo Alas.
				</p>
        <div>
          <Timeline dataSource={{sourceType:"profile", screenName:"gracenapolitano"}}
            options={{username:"GraceNapolitano", height:"400"}}/>
        </div>
				<p class="p-5">
					Links<br></br>
					<Link to="/States/California">California</Link><br></br>
					<Link to="/Environmental_Legislation/Water_in_the_21st_Century_Act">Water in the 21st Century Act</Link><br></br>
				</p>
		</div>
	)
}
}

export default energy_solutions_3
