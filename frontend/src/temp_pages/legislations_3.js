import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

class countries_3 extends React.Component {
  render() {
    return (
    	<div>
				<div className="page-header" align="center">
					<h1> Water in the 21st Century Act </h1>
					<div class="container">
					<img src="https://watereuse.org/wp-content/uploads/2015/01/RWUn-icon.png" height="500" w="500"/>
				</div>
				</div>
				<p class="p-5">
				 	Water in the 21st Century Act or W21 - Establishes within the Environmental Protection Agency (EPA) a WaterSense program to identify and promote water efficient products, buildings, landscapes, facilities, processes, and services. Requires the EPA to identify other voluntary approaches to encourage recycling and reuse technologies to improve water efficiency or lower water use and to implement those approaches, if appropriate. Establishes a State Residential Water Efficiency and Conservation Incentives Program to provide financial incentives for consumers to purchase and install products, buildings, landscapes, facilities, processes, and services labeled under the WaterSense program. Requires the EPA to make grants to owners or operators of water systems to address, mitigate, and adapt to any ongoing or forecasted impact of climate change on a region's water quality or quantity. Authorizes the Department of the Interior to provide financial assistance for water recycling, water infrastructure, enhanced energy efficiency in water systems, desalination projects, permanent water storage, and integrated water management in specified states. Authorizes the transfer of title to nonfederal entities of reclamation projects in need of rehabilitation that are authorized before enactment of this Act under certain conditions. Requires the U.S. Geological Survey (USGS) to establish an open water data system. Reauthorizes the Water Resources Research Act of 1984 and the Water Desalination Act of 1996 through FY2020. Requires the U.S. Army Corps of Engineers, after receiving a request from a nonfederal sponsor, to review the operation of a reservoir and update the water control manual to incorporate improved weather and runoff forecasting methods, if appropriate. Directs the EPA to develop voluntary national drought resilience guidelines relating to preparedness planning and investments for water users and providers. Requires the U.S. Fish and Wildlife Service to prepare a salmon drought plan for <Link to="/States/California">California</Link>.
				</p>
				<p class="p-5">
					Links<br></br>
					<Link to="/States/California">California</Link><br></br>
					<Link to="/Politicians/Grace_F_Napolitano">Grace F. Napolitano</Link><br></br>
					<a href="https://www.congress.gov/bill/114th-congress/house-bill/291">Act's Information Source I</a><br></br>
					<a href="https://www.govtrack.us/congress/bills/114/hr291">Act's Information Source II</a><br></br>
				</p>
		</div>
	)
}
}

export default countries_3