import React from 'react'
import { Link } from 'react-router-dom'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import {Timeline} from 'react-twitter-widgets'

class energy_solutions_2 extends React.Component {
  render() {
    return (
    	<div>
				<div className="page-header" align="center">
					<h1> Bernard Sanders </h1>
				</div>
				<p class="p-5">
					Party: Independent
				</p>
				<div class="container">
					<img src="https://cdn.cnn.com/cnnnext/dam/assets/190417121259-bernie-sanders-fox-townhall-04152019-exlarge-169.jpg" alt="sam"/>
				</div>
				<p class="p-5">
					Bernard Sanders (born September 8, 1941) is an American politician who has served as the junior United States Senator from <Link to="/States/Vermont">Vermont</Link> since 2007. Vermont's at-large Congressman from 1991 to 2007, he is the longest serving independent in U.S. congressional history and a member of the Democratic caucus. Sanders ran unsuccessfully for the 2016 Democratic nomination for president and is running again in 2020. A self-described democratic socialist and progressive, Sanders is known for his opposition to economic inequality. On domestic policy, he broadly supports labor rights, and has supported universal and single-payer healthcare, paid parental leave, and tuition-free tertiary education. On foreign policy, he broadly supports reducing military spending, pursuing more diplomacy and international cooperation, and putting greater emphasis on labor rights and environmental concerns when negotiating international trade agreements.
Sanders was born and raised in the Brooklyn borough of New York City, and attended Brooklyn College before graduating from the University of Chicago in 1964. While a student he was an active protest organizer for the Congress of Racial Equality and the Student Nonviolent Coordinating Committee during the civil rights movement. After settling in Vermont in 1968, Sanders ran unsuccessful third-party political campaigns in the early to mid-1970s. As an independent, he was elected mayor of Burlington in 1981 by a margin of ten votes. He was reelected three times. He won election to the U.S. House of Representatives in 1990, representing Vermont's at-large congressional district; he later co-founded the Congressional Progressive Caucus. Sanders served as a U.S. Representative for 16 years before being elected to the U.S. Senate in 2006. He was reelected to the Senate in 2012 and 2018.
In April 2015 Sanders announced his campaign for the 2016 Democratic nomination for President of the United States. Initially considered a long shot, he went on to win 23 primaries and caucuses and approximately 43% of pledged delegates, to Hillary Clinton's 55%. His campaign was noted for its supporters' enthusiasm, as well as for his rejection of large donations from corporations, the financial industry, and any associated Super PAC. In July 2016 he formally endorsed Clinton in her general election campaign against Republican Donald Trump, while urging his supporters to continue the "political revolution" his campaign began. In February 2019 Sanders announced a second presidential campaign, joining multiple other Democratic candidates pursuing the party nomination.
				</p>
        <div>
          <Timeline dataSource={{sourceType:"profile", screenName:"SenSanders"}}
            options={{username:"BernieSanders", height:"400"}}/>
        </div>
				<p class="p-5">
					Links<br></br>
					<Link to="/States/Vermont">Vermont</Link><br></br>
					<Link to="/Environmental_Legislation/Carbon_Pollution_Transparency_Act_of_2014">Carbon Pollution Transparency Act of 2014</Link><br></br>
				</p>
		</div>
	)
}
}

export default energy_solutions_2
