import React, {
    Component
} from 'react';
import axios from 'axios';

class Random extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            printId: -1
        };
    }

    componentDidMount() {
        this.state.printId = -2
    }

    render() {
        const {
            isLoaded,
            printId
        } = this.state
        var slug = "/"

        var billIDs = [53, 71, 12, 97, 106, 72, 45, 20, 73, 19, 44, 51, 50, 70, 66, 14, 27, 13, 10, 107, 65, 42, 74, 2, 91, 108, 62, 60, 59, 57, 95, 89, 38, 28, 55, 35, 29, 88, 80, 76, 86, 85, 81, 82, 101, 100, 79, 32, 31, 78, 75, 49, 48, 46, 47, 23, 22, 21, 98, 92, 8, 9, 11, 105, 7, 0, 64, 3, 110, 56, 40, 37, 33, 94, 26, 111, 87, 61, 115, 69, 5, 4, 68, 116, 117, 103, 1, 96, 41, 84, 63, 39, 90, 99, 58, 36, 34, 30, 83, 109, 54, 104, 77, 52, 102, 24, 93, 114, 6, 113, 18, 17, 16, 112, 43, 15, 67, 25]
        var stateIDs = ["AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"]
        var polIDs = ["B000711", "B001250", "B001261", "B001267", "B001278", "B001292", "C000127", "C001036", "C001047", "C001066", "C001078", "C001090", "C001112", "D000064", "D000197", "D000563", "D000620", "D000624", "E000297", "F000457", "G000551", "G000564", "G000565", "G000578", "H001028", "H001034", "H001042", "H001064", "H001068", "I000024", "J000126", "J000174", "J000299", "K000148", "K000336", "K000367", "L000304", "L000480", "L000551", "L000562", "L000563", "L000579", "L000588", "M000133", "M000303", "M000355", "M000639", "M001166", "M001169", "M001176", "M001180", "M001200", "N000032", "N000179", "O000006", "P000034", "P000608", "P000609", "R000571", "R000598", "R000606", "R000616", "S000018", "S000033", "S000148", "S001154", "S001170", "S001175", "S001186", "T000469", "U000039", "V000128", "W000215", "W000796", "W000797", "W000798", "W000802"]

        var modelNum = Math.floor((Math.random() * 3) + 1);

        if (modelNum === 1) {
            var instanceNum = Math.floor((Math.random() * stateIDs.length) + 1) - 1;
            slug = slug + "states/" + stateIDs[instanceNum]
        } else if (modelNum === 2) {
            var instanceNum = Math.floor((Math.random() * stateIDs.length) + 1) - 1;
            slug = slug + "Environmental_Legislation/" + billIDs[instanceNum]
        } else if (modelNum === 3) {
            var instanceNum = Math.floor((Math.random() * stateIDs.length) + 1) - 1;
            slug = slug + "politicians/" + polIDs[instanceNum]
        } else {
            return false
        }

        return (
            <div class="page-body">{window.location.assign(slug)}</div>
        )
    }
}

export default Random
