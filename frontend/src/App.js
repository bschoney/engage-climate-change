import React, {
    Component
} from 'react';
import './App.css';
import Navigation from './Navigation';

import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'

import Home from './Home/Home';
import About from './About/About'
import Search from './Search/Search'
import Politicians from './Politicians/Politicians'
import PoliticiansInstances from './Politicians/PoliticiansInstances'
import States from './States/States'

import Visualizations from './Visualizations/Visualizations'
import EmissionsMap from './Visualizations/Ours/Emissions/EmissionsMapContainer'
import RenewablesMap from './Visualizations/Ours/Renewables/RenewablesMapContainer'
import EmissionsHisto from './Visualizations/Ours/Emissions/EmissionsHistoContainer'
import RenewablesHisto from './Visualizations/Ours/Renewables/RenewablesHistoContainer'
import BillsCloud from './Visualizations/Ours/Bills/BillsCloudContainer'


import DiseasesCloud from './Visualizations/Theirs/Diseases/DiseasesCloudContainer'
import FoodCloud from './Visualizations/Theirs/Food/FoodCloudContainer'
import AllScatter from './Visualizations/Theirs/Scatter/AllScatterContainer'


import Random from './Random/Random'

import StatesInstances from './States/StatesInstances'
import StateCard from './States/StateCard'
import Environmental_Legislation from './Environmental_Legislation/Environmental_Legislation'
import EL_Instances from './Environmental_Legislation/EL_Instances'


class App extends Component {
    state = {
        data: [],
        stateData: []
    }


    // Code is invoked after the component is mounted/inserted into the DOM tree.
    componentDidMount() {
        const api = "https://api.engageclimatechange.world/politicians"
        fetch(api)
            .then(result => result.json())
            .then(result => result)
            .catch(function(error) {
                console.log('Looks like there was a problem: \n', error);
            });
    }

    render() {
        const pol = this.state


        return (
            <div>
        <Navigation />
        <div className="container"/>
        <Router>
          <Route exact path ="/" component = {Home} />
          <Route exact path="/About" component = {About}/>
          <Route exact path="/Search/:id" component = {Search}/>

          <Route exact path="/Visualizations/" component = {Visualizations}/>
          <Route exact path="/Visualizations/Ours/Emissions_Map/" component = {EmissionsMap}/>
          <Route exact path="/Visualizations/Ours/Renewables_Map/" component = {RenewablesMap}/>
          <Route exact path="/Visualizations/Ours/Renewables_Histo/" component = {RenewablesHisto}/>
          <Route exact path="/Visualizations/Ours/Emissions_Histo/" component = {EmissionsHisto}/>
          <Route exact path="/Visualizations/Ours/Bills_Cloud/" component = {BillsCloud}/>

          <Route exact path="/Visualizations/Theirs/Diseases_Cloud/" component = {DiseasesCloud}/>
          <Route exact path="/Visualizations/Theirs/Food_Cloud/" component = {FoodCloud}/>
          <Route exact path="/Visualizations/Theirs/All_Scatters/" component = {AllScatter}/>

          <Route exact path="/Random/" component = {Random}/>

          <Route exact path="/States" component = {States}/>
          <Route exact path="/States/:id" component = {StatesInstances}/>
          <Route exact path="/Politicians" render={(props) => (<Politicians Politician_Card_Data={pol} {...props}/>) }/>
          <Route exact path="/Politicians/:id" render={(props) => (<PoliticiansInstances Politician_Data={pol} {...props}/>) }/>
          <Route exact path="/Environmental_Legislation" component = {Environmental_Legislation}/>
          <Route exact path="/Environmental_Legislation/:id" component = {EL_Instances}/>

        </Router>
      </div>
        );
    }
}

export default App
