import React from 'react'
import "./Home.css"
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import './Background.css'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

class Home extends React.Component {
    render() {
        return (
            <div class="background-home">
            <div class="jumbotron jumbotron-fluid jumbotron_1">
                <div class="container">
                    <br></br><br></br>
                    <div className="page-header">
                    <Card style={{background: 'rgba(251, 251, 251, .6)'}}>
                      <CardContent>
                        <h1 class="font-weight-bold centerHead"> Welcome to Engage Climate Change! </h1>
                        <h5 class="p-1 centerHead">Learn more about enviornmental impacts through US politicians, legislation, and emissions data</h5> 
                      </CardContent>
                    </Card>
                    </div>
                </div>
            </div>
            <div class="jumbotron jumbotron-fluid jumbotron_2 no-bottom-margin">
            <div className="page-header" align="center">
                <h2 class="m-4 font-weight-bold">About</h2>
            </div>
            <div class="container">
               <h5>
                  Engage Climate Change is a full stack project built for Glenn Downing's Software Engineering (CS 373) course at the University of Texas. Designed and written from the ground up, this project was completed over the span of 6 weeks and earned the highest grade out of all projects completed during the summer 2019 semeseter. All data shown on the models' pages are pulled from our API, which is documented <a href="https://documenter.getpostman.com/view/7911561/S1a61kyB?version=latest">here</a>.
                  <br/><br/>
                  <a href="/About">Click here to learn more about the contributors and tools →</a>
               </h5>
            </div>
            <br/>
            <div className="page-header" align="center">
                <h2 class="m-4 font-weight-bold">Motivation</h2>
            </div>
            <div class="container">
               <h5>
                  The motivation for Engage Climate Change is to inform people of the environmental impact of the United States and each individual state, the actions taken to improve and reduce this impact, and the political figures who have sponsored such actions. We hope to provide a platform for the general public to get informed on environmental legislation passed in their state and by their state representatives. In doing so, we hope to be a resource for supporting good environmental policy and informed voters by making it easy for people to support state representatives as well as representatives in the US Congress with whom they agree on such policies.
               </h5>
            </div>
            <br/><br/><br/>

                <div class="container">
                    <div className="page-header" align="center">
                        <h1 class="font-weight-bold"> Start Exploring! </h1>
                    </div>
                    <br></br><br></br><br></br>
                    <div class="d-flex justify-content-around darken-Font">
                        <div>
                            <a href="/States"><img src="https://image.flaticon.com/icons/svg/861/861665.svg" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">States</h4>
                        </div>
                        <div>
                            <a href="/Environmental_Legislation"><img src="https://image.flaticon.com/icons/svg/1183/1183866.svg" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Legislation</h4>
                        </div>
                        <div>
                            <a href="/Politicians"><img src="https://image.flaticon.com/icons/svg/1672/1672320.svg" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Politicians</h4>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default Home
