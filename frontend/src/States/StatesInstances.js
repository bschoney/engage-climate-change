import React from 'react'
import {
    Link
} from 'react-router-dom'
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'
import {
    withStyles
} from "@material-ui/core/styles";
import '../Components/CardContainer.css'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";


class StatesInstances extends React.Component {
    constructor(props) {
        super(props)
        let curID = props.match.params.id.toUpperCase()

        this.state = {
            isLoaded: false,
            propsPath: curID,
            jsonData: []
        }
        this.fetchData = this.fetchData.bind(this);
        this.setState({
            propsPath: curID
        })

    }

    componentDidMount() {
        this.fetchData()
            .then(this.setState({
                isLoaded: true
            }))
    }

    async fetchData() {
        await fetch('https://api.engageclimatechange.world/states/' + this.props.match.params.id, {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then((result) => {
                this.setState({
                    jsonData: result
                });
            });
    }

    buildImgCarousel() {
      let stateID = this.state.propsPath.toLowerCase()
      let firstimg = "https://www.50states.com/images/redesign/flags/" + stateID + "-largeflag.png"
      let secondimg = "https://www.50states.com/images/redesign/seals/" + stateID + "-largeseal.png"
      let thirdimg = "https://www.50states.com/images/redesign/maps/" + stateID + "-largemap.png"

      let myCarousel = (
        <div id="carouselStates" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="rounded mx-auto d-block" src={firstimg} alt="First slide"/>
            </div>
            <div class="carousel-item">
              <img class="rounded mx-auto d-block" src={secondimg} alt="Second slide"/>
            </div>
            <div class="carousel-item">
              <img class="rounded mx-auto d-block" src={thirdimg} alt="Third slide"/>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselStates" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselStates" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      )

      return myCarousel
    }

    buildBodyData() {
        const {
            jsonData
        } = this.state

        /* Sets up Emission Data */
        var emissionsTableData = this.state.jsonData.emissions
        var x = [];
        var y = [];
        for (var key in emissionsTableData) {
            y.push(emissionsTableData[key].data)
            x.push(emissionsTableData[key].year)
        }

        var emissionsData = []
        for (var i = x.length - 1; i > x.length - 21; i--) {
            emissionsData.push(
                <tr>
          <td align="right">{x[i]}</td>
          <td align="right">{y[i] ? (<>{y[i].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</>) : (<></>)}</td>
        </tr>
            );
        }

        /* Sets up Renewable Data */
        var renewableTableData = this.state.jsonData.renewables
        var f = [];
        var s = [];

        for (var key in renewableTableData) {
            var str = renewableTableData[key].year_month
            var a = ""
            var b = ""

            b = str.substring(0, 4)
            a = str.substring(4, 6)

            var newStr = "" + a + "/" + b

            f.push(newStr)
            s.push(renewableTableData[key].data)
        }

        var renewableData = []

        let temp = jsonData.landarea
        try {
          // temp = temp.toString
        }
        catch(err) {

        }

        for (var i = f.length - 1; i > f.length - 21; i--) {
            renewableData.push(
                <tr>
          <td align="right">{f[i]}</td>
          <td align="right">{s[i] ? (<>{s[i].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</>) : (<></>)}</td>
        </tr>
            );
        }

        let myTable = (
            <div class="container">
          <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
            <CardContent>
              <div className="page-header" align="center">
                  <h3 class="text-white"><b>{jsonData.statename ? (<>{jsonData.statename}</>, jsonData.statename[jsonData.statename.length -1] != 's' ? (<>{jsonData.statename}'s</>) : (<>{jsonData.statename}'</>)) : (<>{""}</>)} Statistical Data</b></h3>
              </div><hr></hr>
              <table class="table table-bordered table-hover table-striped table-dark">
              <thead class="thead-dark"><tr>
                <th title="Field #1" width="10%">Statistic</th>
                <th title="Field #2" width="10%">Datum</th>
                <th title="Field #3" width="10%">Units</th>
              </tr></thead>
              <tbody>
                <tr>
                  <td align="left"><b>Land Area</b></td>
                  <td align="right">{jsonData.landarea ? (<>{jsonData.landarea.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</>) : (<></>)}</td>
                  <td align="right">Sq. miles</td>
                </tr>
                <tr>
                  <td align="left"><b>Population</b></td>
                  <td align="right">{jsonData.pop ? (<>{jsonData.pop.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</>) : (<></>)}</td>
                  <td align="right">People</td>
                </tr>
                <tr>
                  <td align="left"><b>Population Density</b></td>
                  <td align="right">{jsonData.pop_density ? (<>{jsonData.pop_density.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</>) : (<></>)}</td>
                  <td align="right">People per sq. mile</td>
                </tr>
                <tr>
                  <td align="left"><b>Fossil Fuel Total Consumption</b></td>
                  <td align="right">{jsonData.emissions_rec ? (<>{jsonData.emissions_rec.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</>) : (<></>)}</td>
                  <td align="right">Billion Btu</td>
                </tr>
                <tr>
                  <td align="left"><b>Net Generation of Renewables</b></td>
                  <td align="right">{jsonData.renewables_rec ? (<>{jsonData.renewables_rec.toString().split(".")[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "." +   jsonData.renewables_rec.toString().split(".")[1].substring(0, 2 )}</>) : (<></>)}</td>
                  <td align="right">Gigawatthours</td>
                </tr>

              </tbody>
              </table>
            </CardContent>
          </Card>
          <br></br><br></br>
          <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
            <CardContent>
              <div className="page-header" align="center">
                  <h3 class="text-white"><b>{jsonData.statename}'s Historical Emissions and Renewables Data</b></h3>
              </div><hr></hr>
              <div class="row">
                <div class="col">
                  <table class="table table-bordered table-hover table-striped table-dark">
                    <thead class="thead-dark"><tr>
                      <th title="Field #1">Date (Year)</th>
                      <th title="Field #2">Energy Usage (Billions of Btu - British Thermal Unit)</th>
                    </tr></thead>
                    <tbody>
                      {emissionsData}
                    </tbody>
                  </table>
                </div>
                <div class="col">
                  <table class="table table-bordered table-hover table-striped table-dark">
                    <thead class="thead-dark"><tr>
                      <th title="Field #1">Date (M/Y)</th>
                      <th title="Field #2">Renewable Energy Generated (Gigawatt Hours)</th>
                    </tr></thead>
                    <tbody>
                      {renewableData}
                    </tbody>
                  </table>
                </div>
              </div>
            </CardContent>
          </Card>
          <br></br><br></br>
          <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
            <CardContent>
              <div className="page-header" align="center">
                <h3 class="text-white"><b>State's Related Links</b></h3>
              </div><hr></hr>
              {this.buildLinks()}
            </CardContent>
          </Card>
        </div>
        )

        return myTable;
    }

    buildLinks() {
        var myBills = this.state.jsonData.bills;
        var myBill = "";
        var myBillLink = "/Enviornmental_Legislation/"
        try {
            var myBillNum = Math.floor((Math.random() * myBills.length) + 0)
            myBill = myBills[myBillNum].name;
            myBillLink = "/Environmental_Legislation/" + myBills[myBillNum].id
        } catch {
            myBill = "";
            myBillLink = "/Enviornmental_Legislation/"
        }

        var myPols = this.state.jsonData.politicians;
        var myPol = "";
        var myPolLink = "/Politicians/"
        try {
            var myPolNum = Math.floor((Math.random() * myPols.length) + 0)
            myPol = myPols[myPolNum].first_name + " " + myPols[myPolNum].last_name;
            myPolLink = "/Politicians/" + myPols[myPolNum].id
        } catch {
            myPol = "";
            myPolLink = "/Politicians/"
        }

        var shortBill = ""
        try {
            shortBill = myBill.replace(/(.{115})..+/, "$1…");
        } catch {
            shortBill = "(Missing Title)"
        }

        if (myPol.length != 0) {

            var result = (
                <div class="row d-flex justify-content-center">
    <Link to={myBillLink}>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card-link card-white border-0 shadow">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Related:</h5>
          <h7 class="card-title mb-0">{shortBill}</h7>
          <hr></hr>
          <p>Legislation sponsored from this state</p>
        </div>
      </div>
    </div>
    </Link>


    <Link to={myPolLink}>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card-link card-white border-0 shadow">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Related:</h5>
          <h7 class="card-title mb-0">{myPol}</h7>
          <hr></hr>
          <p>Politician from this state</p>
        </div>
      </div>
    </div>
    </Link>
  </div>

            );
        } else {
            var result = (
                <div class="row d-flex justify-content-center">
      <Link class="black-link">
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card-link card-white border-0 shadow">
          <div class="card-body text-center">
            <h5 class="card-title mb-0">No related legislation</h5>
            <h7 class="card-title mb-0">This state has not been a part of any relevant legislation</h7>
            <hr></hr>
            <p>Try a another state!</p>
          </div>
        </div>
      </div>
      </Link>

      <Link class="black-link">
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card-link card-white border-0 shadow">
          <div class="card-body text-center">
            <h5 class="card-title mb-0">No related politician</h5>
            <h7 class="card-title mb-0">This state has no politicians who have sponsored enviromental protection legislation</h7>
            <hr></hr>
            <p>Try a another state!</p>
          </div>
        </div>
      </div>
      </Link>
    </div>
            );
        }

        return result
    }

    render() {

        const {
            isLoaded,
            jsonData
        } = this.state
        const getStatePic = "https://www.50states.com/images/redesign/flags/" + this.state.propsPath.toLowerCase() + "-largeflag.png"

        if (!isLoaded) {
            return (<div> <p>Loading ...</p> </div>);
        } else {
            return (
        <div class="background-ST-Instances page-body background-fixed">
        <div class="padding-Models">
          <div class="container">
            <div className="page-header" align="center"><br></br>
              <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
              <CardContent>
                <h1 class="text-white">{jsonData.statename}</h1>
              </CardContent>
              </Card>
            </div>
          </div>
          <br></br><br></br>
          {this.buildImgCarousel()}
          <br></br><br></br>
          {this.buildBodyData()}
          <br></br><br></br>
        </div>
        </div>
            );
        }
    }
}

export default StatesInstances