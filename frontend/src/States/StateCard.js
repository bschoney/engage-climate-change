import React, {
    Component
} from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import Highlighter from "react-highlight-words";
import '../Components/CardContainer.css'
import '../Components/FlipCard.css'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

class StateCard extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    render() {
        const holder = this.props.sentToStateCard
        const searchQuery = this.props.sendSearchQuery
        const toPath = "/States/" + holder.id
        const getStatePic = "https://www.50states.com/images/redesign/flags/" + holder.id.toLowerCase() + "-largeflag.png"

        const buildHighLight = () => {
            if (searchQuery != null && searchQuery != '') {
                return (
                    <div class="container-highlight card">
          <h6>
          State: 
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[searchQuery]}
            autoEscape = {true}
            textToHighlight ={" " + holder.statename + ", "}
          />
          Abbr:
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[searchQuery]}
            autoEscape = {true}
            textToHighlight ={" " + holder.id}
          />
          </h6>
          </div>
                )
            }
        }

        let possessive = "'"
        if (holder.statename[holder.statename.length -1] != 's') {
          possessive += 's'
        } 
        var comma_renewables_rec = holder.renewables_rec
        comma_renewables_rec = holder.renewables_rec.toString().split(".")[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "." +   holder.renewables_rec.toString().split(".")[1].substring(0, 2 )

        return (
            <a href={toPath}>
        <div class="flip-card">
          <div class="flip-card-inner">
            <div class="flip-card-front">
              <img class="flip-img" src={getStatePic} alt=""/>
              <div class="card-body">
                {<h5 class="card-title">{holder.statename}</h5>}
              </div>
            </div>
            <div class="flip-card-back">
              <div class="card-instances"><div class="card-body">
                <h6 class="card-subtitle mb-2 text-muted" align="center">{holder.statename}{possessive} Statistical Data</h6><hr></hr>
                <h6 class="card-subtitle mb-2 text-muted"><b>Landarea:</b> {holder.landarea.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} Sq. Miles</h6>
                <h6 class="card-subtitle mb-2 text-muted"><b>State population:</b> {holder.pop.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} People</h6>
                <h6 class="card-subtitle mb-2 text-muted"><b>Population Density:</b> {holder.pop_density.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} People per Sq. Miles</h6>
                <h6 class="card-subtitle mb-2 text-muted"><b>Fossil Fuel Total Consumption:</b> {holder.emissions_rec.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} Billion Btu</h6>
                <h6 class="card-subtitle mb-2 text-muted"><b>Net Generation of Renewables:</b> {comma_renewables_rec} Gigawatthours</h6>
                <hr></hr>
              </div></div>
            </div>
          </div>
        </div>
        {buildHighLight()}
      </a>
        )
    }
}

export default StateCard