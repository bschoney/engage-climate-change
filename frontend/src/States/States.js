import React from 'react'
import {
    Link
} from 'react-router-dom'
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'
import '../Components/CardContainer.css'
import StateCard from './StateCard'
import Pagination from "react-js-pagination";

import Input from "@material-ui/core/Input";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const STATESURL = 'https://api.engageclimatechange.world/states'

class States extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cardSource: [],
            isLoaded: false,
            numCardsPerPage: 10,
            currentPage: 1,
            jsonData: [],
            sortAssignment: [[''], ['']],
            sortQueryValue: ['asc', 'statename'],
            sortQueryMap: '"order_by":[]',
            filterAssignment: {
                'pop': '',
                'pop_density': '',
                'landarea': '',
                'emissions_rec': '',
                'renewables_rec': ''
            },
            filterQueryValue: {
                'pop': [null, null],
                'pop_density': [null, null],
                'landarea': [null, null],
                'emissions_rec': [null, null],
                'renewables_rec': [null, null]
            },
            filterQueryMap: '"filters":[{"and":[]}',
            searchQueryValue: null,
            searchQueryMap: '{"or":[]}',
        };

        this.fetchData = this.fetchData.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.fetchData()
            //.then(this.createCards())
            .then(this.setState({
                isLoaded: true
            }))
    }

    async fetchData() {
        let resultData = []
        fetch(this.formatURL(), {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then((result) => {
                let source = result.objects
                for (var key in source) {
                    resultData.push(source[key])
                }
            })
            .then(() => this.setState({
                jsonData: resultData
            }))
    }

    formatURL() {
        const {
            sortQueryMap,
            filterQueryMap,
            searchQueryMap
        } = this.state;
        /*
        if (filterQuery != '') {
          filterQuery = ',' + filterQuery
        }

        onChange={(e) => {this.setState({searchQuery: e.target.value.trim()})}}
                    onKeyDown={event => this.handleSearch(event.target.value)}
        */
        return (STATESURL + '?q={' + sortQueryMap + ',' + filterQueryMap + ',' + searchQueryMap + ']}')
    }

    // Handles specific onClick conditions for Pagination
    async handleClick(event) {
        await this.setState({
            currentPage: event
        })
    }

    handleSortQuery(value) {
        let valueArray = value.split(" ")
        const {
            sortQueryValue
        } = this.state;
        let sortDirection = sortQueryValue[0];
        let sortField = sortQueryValue[1];

        let newSortAssignment = this.state.sortAssignment.slice()
        if (valueArray[0] == 0) {
            sortDirection = valueArray[1]
            newSortAssignment[0] = value
        } else {
            sortField = valueArray[1]
            newSortAssignment[1] = value
        }

        let sortQuery = '"order_by":[{"field":"' + sortField + '","direction":"' + sortDirection + '"}]'
        this.setState({
            sortQueryValue: [sortDirection, sortField],
            sortQueryMap: sortQuery,
            sortAssignment: newSortAssignment
        }, () => this.fetchData());
    }

    handleFilterQuery(value) {
        let valueArray = value.split(" ")
        const {
            filterQueryValue,
            filterAssignment
        } = this.state;
        let filterQuery = ''
        let filterArray = []

        /* Updates filterQueryValue with new values */
        for (var keys in filterQueryValue) {
            if (keys == valueArray[0]) {
                filterQueryValue[keys][0] = valueArray[1]
                filterQueryValue[keys][1] = valueArray[2]
                filterAssignment[keys] = value
            }
        }

        /* Makes filterQuery*/
        for (var keys in filterQueryValue) {
            if (filterQueryValue[keys][0] != null) {
                filterArray.push('{"name":"' + keys + '","op":"ge","val":' + filterQueryValue[keys][0] + '},{"name":"' + keys + '","op":"le","val":' + filterQueryValue[keys][1] + '}')
            }
        }

        /* Adds appropriate commas to queries */
        for (var i = 0; i < filterArray.length; i++) {
            filterQuery += filterArray[i];
            if (i !== filterArray.length - 1) {
                filterQuery += ','
            }
        }

        let result = '"filters":[{"and":[' + filterQuery + ']}'
        this.setState({
            filterQueryMap: result
        }, () => this.fetchData());
    }

    handleSearchQuery(value) {
        let mySearchQuery = ''
        let searchArray = []

        /* Makes the search query */
        if (value != null) {
            searchArray.push('{"name":"id","op":"ilike","val":"%25' + value + '%25"}')
            searchArray.push('{"name":"statename","op":"ilike","val":"%25' + value + '%25"}')
        }

        /* Adds the commas to the query at appropriate positions */
        for (var i = 0; i < searchArray.length; i++) {
            mySearchQuery += searchArray[i];
            if (i !== searchArray.length - 1) {
                mySearchQuery += ','
            }
        }

        let myValue = value
        if (myValue == '') {
            myValue = null
        }

        let myQuery = '{"or":[' + mySearchQuery + ']}'
        this.setState({
            searchQueryValue: myValue,
            searchQueryMap: myQuery
        }, () => this.fetchData());
    }


    createCards() {
        let source = this.state.jsonData;
        var cards = [];

        for (var key in source) {
            cards.push(<StateCard sentToStateCard={source[key]} sendSearchQuery={this.state.searchQueryValue}/>);
        };
        this.state.cardSource = cards;
    }

    render() {
        this.createCards()
        const {
            cardSource,
            isLoaded,
            numCardsPerPage,
            currentPage,
            currentNumCards,
            searchQueryValue,
            sortAssignment,
            filterAssignment,
        } = this.state;
        // Pagination
        const lastPageNum = currentPage * numCardsPerPage;
        const firstPageNum = lastPageNum - numCardsPerPage;
        const currentCards = cardSource.slice(firstPageNum, lastPageNum);
        const renderCards = currentCards.map((cardSource, index) => {
            return <div>{cardSource}</div>
        });


        if (!isLoaded) {
            return (<div> <p>Loading ...</p> </div>);
        } else {
            return (
                <div class="background-State page-body background-fixed">
        <div class="padding-Models">
          <div className="container">
            <div className="page-header">
              <h1 class="text-white center-title">States</h1>
              <br></br>
            </div>
          </div>
          <div class="container-white-states">
          <div class="container">
            <div class="row">
              <div class="col">
              <TextField
                style={{width: "100%"}}
                id="stateSearch"
                placeholder="Search ..."
                onChange={event => this.handleSearchQuery(event.target.value)}
              />
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Sort Direction</InputLabel>
                  <Select
                    value={sortAssignment[0]}
                    onChange={event => this.handleSortQuery(event.target.value)}
                  >
                    <MenuItem value={'0 asc'}>Any</MenuItem>
                    <MenuItem value={'0 asc'}>Ascending</MenuItem>
                    <MenuItem value={'0 desc'}>Descending</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Sort By Attribute Name</InputLabel>
                  <Select
                    value={sortAssignment[1]}
                    onChange={event => this.handleSortQuery(event.target.value)}
                  >
                    <MenuItem value={'1 statename'}>Any</MenuItem>
                    <MenuItem value={'1 statename'}>State Name</MenuItem>
                    <MenuItem value={'1 pop'}>Population</MenuItem>
                    <MenuItem value={'1 pop_density'}>Population Density</MenuItem>
                    <MenuItem value={'1 landarea'}>Landarea</MenuItem>
                    <MenuItem value={'1 emissions_rec'}>Emissions</MenuItem>
                    <MenuItem value={'1 renewables_rec'}>Renewables</MenuItem>
                  </Select>
                </FormControl>
              </div>
          <div class="container">  
            <div class="row">
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Population</InputLabel>
                  <Select
                    value={filterAssignment["pop"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'pop 0 999999999999'}>Any</MenuItem>
                    <MenuItem value={'pop 0 100000'}>Less than 100k</MenuItem>
                    <MenuItem value={'pop 100000 1000000'}>100k - 1000k</MenuItem>
                    <MenuItem value={'pop 1000000 10000000'}>1000k - 10000k</MenuItem>
                    <MenuItem value={'pop 10000000 999999999999'}>Greater than 10000k</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Population Density</InputLabel>
                  <Select
                    value={filterAssignment["pop_density"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'pop_density 0 999999999999'}>Any</MenuItem>
                    <MenuItem value={'pop_density 0 10'}>Less than 10</MenuItem>
                    <MenuItem value={'pop_density 10 100'}>10 - 100</MenuItem>
                    <MenuItem value={'pop_density 100 1000'}>100 - 1000</MenuItem>
                    <MenuItem value={'pop_density 1000 999999999999'}>Greater than 1000</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Landarea</InputLabel>
                  <Select
                    value={filterAssignment["landarea"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'landarea 0 999999999999'}>Any</MenuItem>
                    <MenuItem value={'landarea 0 1000'}>Less than 1k</MenuItem>
                    <MenuItem value={'landarea 1000 10000'}>1k - 10k</MenuItem>
                    <MenuItem value={'landarea 10000 100000'}>10k - 100k</MenuItem>
                    <MenuItem value={'landarea 100000 999999999999'}>Greater than 100k</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Emissions</InputLabel>
                  <Select
                    value={filterAssignment["emissions_rec"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'emissions_rec 0 999999999999'}>Any</MenuItem>
                    <MenuItem value={'emissions_rec 0 100000'}>Less than 100k</MenuItem>
                    <MenuItem value={'emissions_rec 100000 1000000'}>100k - 1000k</MenuItem>
                    <MenuItem value={'emissions_rec 1000000 10000000'}>1000k - 10000k</MenuItem>
                    <MenuItem value={'emissions_rec 10000000 999999999999'}>Greater than 10000k</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Renewables</InputLabel>
                  <Select
                    value={filterAssignment["renewables_rec"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'renewables_rec 0'}>Any</MenuItem>
                    <MenuItem value={'renewables_rec 0 10'}>Less than 10</MenuItem>
                    <MenuItem value={'renewables_rec 10 100'}>10 - 100</MenuItem>
                    <MenuItem value={'renewables_rec 100 1000'}>100 - 1000</MenuItem>
                    <MenuItem value={'renewables_rec 1000 999999999999'}>Greater than 1000</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>
          </div>
          </div>
        </div>
          </div>
          <div className="container">
            <div className="card-container">
              {renderCards}
              <br/>
            </div>
          </div>
          <h3 class="text-white text-center"> Found {cardSource.length} sources </h3>
          <Pagination
                activePage={currentPage}
                itemsCountPerPage={numCardsPerPage}
                totalItemsCount={cardSource.length}
                onChange={this.handleClick}
                itemClass="page-item"
                linkClass="page-link"
                innerClass="pagination justify-content-center"
          />
        </div>
        </div>
            );
        };
    }
}

export default States