import React, { Component } from 'react'
import "./Visualizations.css"
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import ReactDOM from 'react-dom';
import './VBackground.css'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";



import Scatterplot from "./Theirs/Scatter/ReviewsScatter"
import FoodCloud from "./Theirs/Food/FoodCloud"
import DiseaseCloud from "./Theirs/Diseases/DiseasesCloud"


import EmissionsMap from "./Ours/Emissions/EmissionsMap"
import RenewablesMap from "./Ours/Renewables/RenewablesMap"
import EmissionsHisto from "./Ours/Emissions/EmissionsHisto"
import RenewablesHisto from "./Ours/Renewables/RenewablesHisto"
import BillsCloud from "./Ours/Bills/BillsCloud"
    
class Home extends  Component {
    render() {
        return (
            <div class="">
                <div class="background-VIS page-body background-fixed">
                    <div class="padding-Models" align="center">
                    <div className="page-header" align="center">
                        <br></br>
                        <h1 class="font-weight-bold text-white">Our Visualizations</h1>
                    </div>
                    <br></br>
                    <div class="d-flex justify-content-around darken-Font">
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Emissions_Histo"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/em.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Emissions Histogram</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Renewables_Histo"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/rec.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Renewables Histogram</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Bills_Cloud"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/cloud_bill.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Legislation Word Cloud</h4>
                        </div>
                        </CardContent>
                      </Card>
                    </div>  
                    <br></br><br></br>
                    <div class="d-flex justify-content-around darken-Font">
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Emissions_Map"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/usa_em.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Emissions Map</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Renewables_Map"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/usa_rec.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Renewables Map</h4>
                        </div>
                        </CardContent>
                      </Card>
                    </div>
                    
                    {/*
                    <div className="page-header" align="center">
                        <br></br>
                        <br></br>
                        <h1 class="font-weight-bold text-white">Their Visualizations</h1>
                        <h4 class="font-weight-bold text-white">(Their API has been shut down, so these no longer work)</h4>
                    </div>
                    <br></br>
                    <div class="d-flex justify-content-around darken-Font">
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Theirs/Diseases_Cloud"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/cloud_skull.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Diseases Word Cloud</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Theirs/Food_Cloud"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/cloud_food.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Food Word Cloud</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Theirs/All_Scatters"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/scatter.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Scatter Plots</h4>
                        </div>
                        </CardContent>
                      </Card>
                    </div>*/}



                    <hr/>
                    <h2 class="text-white">Emissions Map</h2>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <div>
                        <EmissionsMap></EmissionsMap>
                        </div>
                    </CardContent>
                  </Card>
                  <hr/>
                    <h2 class="text-white">Renewables Map</h2>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <div>
                        <RenewablesMap></RenewablesMap>
                        </div>
                    </CardContent>
                  </Card>
                  <hr/>
                    <h2 class="text-white">Emissions Histogram</h2>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <div>
                        <EmissionsHisto></EmissionsHisto>
                        </div>
                    </CardContent>
                  </Card>
                  <hr/>
                    <h2 class="text-white">Renewables Histogram</h2>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <div>
                        <RenewablesHisto></RenewablesHisto>
                        </div>
                    </CardContent>
                  </Card>
                  <hr/>
                    <h2 class="text-white">Legislation Word Cloud</h2>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <div>
                        <BillsCloud></BillsCloud>
                        </div>
                    </CardContent>
                  </Card>
                  <hr/>
                  {/*
                    <h2 class="text-white">Providers' Histograms</h2>
                  }
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <Scatterplot></Scatterplot>
                    </CardContent>
                  </Card>
                  <hr/>
                    <h2 class="text-white">Providers' Diseases Word Cloud</h2>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <DiseaseCloud></DiseaseCloud> 
                    </CardContent>
                  </Card>
                  <hr/>
                    <h2 class="text-white">Providers' Foods Word Cloud</h2>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                        <FoodCloud></FoodCloud> 
                    </CardContent>
                  </Card>
                  <hr/>

                  */}
                </div>
            </div>
                <div class="background-VIS page-body">
                    <div class="padding-Models" align="center">
                    </div>
                </div>


        </div>
        )
    }
}



export default Home
