import React from 'react'
import "./Visualizations.css"
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import './VBackground.css'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
    
class Home extends React.Component {
    render() {
        return (
            <div class="page-body">
                <div class="background-VIS page-body background-fixed">
                    <div class="padding-Models" align="center">
                    <div className="page-header" align="center">
                        <br></br>
                        <h2 class="font-weight-bold text-white">Our Visualizations</h2>
                    </div>
                    <br></br>
                    <div class="d-flex justify-content-around darken-Font">
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Emissions_Histo"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/em.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Emissions Histogram</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Renewables_Histo"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/rec.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Renewables Histogram</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Bills_Cloud"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/cloud_bill.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Bills Word Cloud</h4>
                        </div>
                        </CardContent>
                      </Card>
                    </div>  
                    <br></br><br></br>
                    <div class="d-flex justify-content-around darken-Font">
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Emissions_Map"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/usa_em.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Emissions Map</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Ours/Renewables_Map"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/usa_rec.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Renewables Map</h4>
                        </div>
                        </CardContent>
                      </Card>
                    </div>
                    <div className="page-header" align="center">
                        <br></br>
                        <br></br>
                        <h2 class="font-weight-bold text-white">Their Visualizations</h2>
                    </div>
                    <br></br>
                    <div class="d-flex justify-content-around darken-Font">
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Theirs/Diseases_Cloud"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/cloud_skull.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Diseases Word Cloud</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <a href="/Visualizations/Theirs/Food_Cloud"><img src="https://gitlab.com/bschoney/engage-climate-change/raw/master/frontend/src/Visualizations/cloud_food.png" alt="Gavel" height="200" w="200"></img></a>
                            <br></br>
                            <br></br>
                            <h4 align="center">Food Word Cloud</h4>
                        </div>
                        </CardContent>
                      </Card>
                      <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                        <CardContent>
                        <div align="center">
                            <img src="https://www.onlygfx.com/wp-content/uploads/2017/10/grunge-x-7-236x300.png" alt="Gavel" height="200" w="200"></img>
                            <br></br>
                            <br></br>
                            <h4 align="center">n/a (yet)</h4>
                        </div>
                        </CardContent>
                      </Card>
                    </div>
                    <br></br>
                </div>
            </div>
        </div>
        )
    }
}

export default Home
