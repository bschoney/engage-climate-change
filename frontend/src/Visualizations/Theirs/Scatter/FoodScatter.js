import React, { Component } from 'react';
import './Visualization.css';
import 'react-d3-components'
import axios from 'axios';
import {
   ScatterChart, CartesianGrid, XAxis, YAxis, Tooltip, Scatter, Label
} from 'recharts';

class ProvViz extends Component {
      constructor(props) {
        super(props);
        this.state = {
            isLoaded: true,
            items: null,
            sodiumData: [],
            reivewsData: [],
            diseaseData: [],
        };
    }

    componentDidMount() {
        this.getData()
    }

    getData() {


        axios.get('https://cors-anywhere.herokuapp.com/http://api.foodforthoughtt.me/food').then(response => {
          var dataLocal = []
            for (var foodIndex in response.data) {
                let food = response.data[foodIndex]
                if(food.sodium < 600) {
                  let restDatum = { x: 100, y: 200, z: 200 };
                  restDatum["x"] = food.sodium
                  restDatum["y"] = food.calories
                  restDatum["z"] = food.name
                  dataLocal.push(restDatum)
                }
            }
            this.setState({
                isLoaded: true,
                sodiumData: dataLocal
            });
                // console.log("log", this.state.data)

        }).catch(error => {
            console.log("log error", error)
        });

        return false
      }

    render() {
        if (this.state.isLoaded) {
            
        }
        const CustomTooltip = ({ active, payload, label }) => {
            console.log(payload);
            if (active) {
                return (
                    <div className="custom-tooltip" style={{background:'#d9e5f7'}}>
                        <p className="label">{payload[0].payload.z}</p>
                        <p className="label">{`${payload[0].name} : ${payload[0].value}${payload[0].unit}`}</p>
                        <p className="label">{`${payload[1].name} : ${payload[1].value}${payload[0].unit}`}</p>
                    </div>
                );
            }
            return null
        };
        const {
            diseaseData,
            sodiumData,
            reviewsData,
        } = this.state;

        console.log("log", this.state.data)


        return(
          <div class="page-body">

              <ScatterChart
                  className="graduation-rate-scatter"
                  width={400}
                  height={400}
                  margin={{
                      top: 20, right: 20, bottom: 20, left: 20,
                  }}
              >
                  <CartesianGrid />
                  <XAxis type="number" dataKey="x" name="Sodium Amount" unit="">
                      <Label value="Sodium Amount" offset={0} position="bottom" />
                  </XAxis>
                  <YAxis type="number" dataKey="y" name="Calorie Amount" unit=""
                         label={{ value: 'Calorie Amount', angle: -90, position: 'insideLeft' }}/>
                  <Tooltip viewBox content={<CustomTooltip />} />
                  <Scatter name="Sodium Amount vs. Calorie Amount" data={sodiumData}  fill="#dedb2f" />
              </ScatterChart>

          </div>

        );
    }
  }

  export default ProvViz;
