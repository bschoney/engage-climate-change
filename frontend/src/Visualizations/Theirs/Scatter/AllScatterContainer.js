import '../../VBackground.css'
import React, { Component } from 'react'
import ReviewsScatter from "./ReviewsScatter"
import DiseaseScatter from "./DiseaseScatter"
import FoodScatter from "./FoodScatter"
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";


class AllScatterContainer extends  Component {
    render() {
        return (
            <div class="background-VIS page-body background-fixed">
                <div class="padding-Models" align="center">
                <br/>
                    <h1 class="text-white">All Scatter Plots</h1>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                	<ReviewsScatter/>
                	</CardContent>
                	</Card>
                	<br/>

                </div>
            </div>
       	)
    }
}

export default AllScatterContainer