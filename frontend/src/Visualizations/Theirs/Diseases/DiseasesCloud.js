import React, {
    Component
} from 'react';
import * as d3 from "d3";
import ReactDOM, {render} from 'react-dom';
import WordCloud from 'react-d3-cloud';
import axios from 'axios';

class Cloud extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: true,
            items: null,
            diseaseWordCount: {},
            diseaseWCFormatted: []
        };
    }

    componentDidMount() {
        this.getDiseaseData()
    }

    getDiseaseData() {
        let diseaseWCFormatted = []
        let diseaseWordCount = {}
        let oneDone = false

        axios.get('https://cors-anywhere.herokuapp.com/http://api.foodforthoughtt.me/restaurant').then(response => {
            for (var restaurant in response.data) {
                let restaurantDiseases = response.data[restaurant].diseases.split(", ").join(',').split(' ').join(',').split(',')

                for (var disease in restaurantDiseases) {
                    let diseaseName = restaurantDiseases[disease].toUpperCase()
                    if (diseaseName in diseaseWordCount) {
                        diseaseWordCount[diseaseName] += 1
                    } else {
                        diseaseWordCount[diseaseName] = 1
                    }
                }
            }

            if (oneDone) {
                for (var word in diseaseWordCount) {
                    var formattedEntry = {
                        text: word,
                        value: diseaseWordCount[word]
                    }
                    diseaseWCFormatted.push(formattedEntry)
                }
                this.setState({
                    isLoaded: false,
                    diseaseWordCount: diseaseWordCount,
                    diseaseWCFormatted: diseaseWCFormatted
                });
            }
            oneDone = true
        }).catch(error => {
            console.log("log error", error)
        });

        axios.get('https://cors-anywhere.herokuapp.com/http://api.foodforthoughtt.me/food').then(response => {
            for (var restaurant in response.data) {
                let restaurantDiseases = response.data[restaurant].diseases.split(", ").join(',').split(' ').join(',').split(',')

                for (var disease in restaurantDiseases) {
                    let diseaseName = restaurantDiseases[disease].toUpperCase()
                    if (diseaseName in diseaseWordCount) {
                        diseaseWordCount[diseaseName] += 1
                    } else {
                        diseaseWordCount[diseaseName] = 1
                    }
                }
            }

            if (oneDone) {
                for (var word in diseaseWordCount) {
                    var formattedEntry = {
                        text: word,
                        value: diseaseWordCount[word]
                    }
                    diseaseWCFormatted.push(formattedEntry)
                }
                this.setState({
                    isLoaded: false,
                    diseaseWordCount: diseaseWordCount,
                    diseaseWCFormatted: diseaseWCFormatted
                });
            }
            oneDone = true
        }).catch(error => {
            console.log("log error", error)
        });

        return false;
    }

    render() {
        const fontSizeMapper = word => Math.log2(word.value) * 5;
        const rotate = word => word.value % -90;
        const {
            isLoaded,
            diseaseWordCount,
            diseaseWCFormatted
        } = this.state;

        return (
          <WordCloud
    data={diseaseWCFormatted}
    fontSizeMapper={fontSizeMapper}
    rotate={rotate}
    width={400}
    height={400}
  />
        );
    }
}

export default Cloud;
