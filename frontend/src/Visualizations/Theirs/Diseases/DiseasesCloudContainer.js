import '../../VBackground.css'
import React, { Component } from 'react'
import DiseasesCloud from "./DiseasesCloud"
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";


class DiseasesCloudContainer extends  Component {
    render() {
        return (
            <div class="background-VIS page-body background-fixed">
                <div class="padding-Models" align="center">
                <br/>
                    <h1 class="text-white">Diseases Word Cloud</h1>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                	<DiseasesCloud/>
                	</CardContent>
                	</Card>
                	<br/>

                </div>
            </div>
       	)
    }
}

export default DiseasesCloudContainer