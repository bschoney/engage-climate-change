import React, {
    Component
} from 'react';
import * as d3 from "d3";
import ReactDOM, {render} from 'react-dom';
import WordCloud from 'react-d3-cloud';
import axios from 'axios';

class Cloud extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: true,
            items: null,
            foodWordCount: {},
            foodWCFormatted: []
        };
    }

    componentDidMount() {
        this.getFoodData()
    }

    getFoodData() {
        let foodWCFormatted = []
        let foodWordCount = {}
        let oneDone = false

        axios.get('https://cors-anywhere.herokuapp.com/http://api.foodforthoughtt.me/disease').then(response => {
            for (var restaurant in response.data) {
                let restaurantFoods = response.data[restaurant].foods.split(", ").join(',').split(' ').join(',').split(',')

                for (var food in restaurantFoods) {
                    let foodName = restaurantFoods[food].toUpperCase()
                    if (foodName in foodWordCount) {
                        foodWordCount[foodName] += 1
                    } else {
                        foodWordCount[foodName] = 1
                    }
                }
            }

            if (oneDone) {
                for (var word in foodWordCount) {
                    var formattedEntry = {
                        text: word,
                        value: foodWordCount[word]
                    }
                    foodWCFormatted.push(formattedEntry)
                }

                this.setState({
                    isLoaded: false,
                    foodWordCount: foodWordCount,
                    foodWCFormatted: foodWCFormatted
                });
            }
            oneDone = true
        }).catch(error => {
            console.log("log error", error)
        });

        axios.get('https://cors-anywhere.herokuapp.com/http://api.foodforthoughtt.me/restaurant').then(response => {
            for (var restaurant in response.data) {
                let restaurantFoods = response.data[restaurant].foods.split(", ").join(',').split(' ').join(',').split(',')

                for (var food in restaurantFoods) {
                    let foodName = restaurantFoods[food].toUpperCase()
                    if (foodName in foodWordCount) {
                        foodWordCount[foodName] += 1
                    } else {
                        foodWordCount[foodName] = 1
                    }
                }
            }

            if (oneDone) {
                for (var word in foodWordCount) {
                    var formattedEntry = {
                        text: word,
                        value: foodWordCount[word]
                    }
                    foodWCFormatted.push(formattedEntry)
                }

                this.setState({
                    isLoaded: false,
                    foodWordCount: foodWordCount,
                    foodWCFormatted: foodWCFormatted
                });
            }
            oneDone = true
        }).catch(error => {
            console.log("log error", error)
        });

        return false;
    }


    render() {
        const fontSizeMapper = word => Math.log2(word.value) * 5;
        const rotate = word => word.value % -90;
        const {
            isLoaded,
            foodWordCount,
            foodWCFormatted
        } = this.state;

        return (
          <WordCloud
    data={foodWCFormatted}
    fontSizeMapper={fontSizeMapper}
    rotate={rotate}
    width={400}
    height={400}
  />
        );
    }
}

export default Cloud;
