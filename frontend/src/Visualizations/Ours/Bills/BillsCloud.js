import React, {
    Component
} from 'react';
import * as d3 from "d3";
import ReactDOM, {render} from 'react-dom';
import WordCloud from 'react-d3-cloud';
import axios from 'axios';

class Cloud extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: true,
            items: null,
            wordCount: {},
            wordCountFormatted: []

        };
    }

    componentDidMount() {
        this.getBillsData()
    }

    getBillsData() {
        let wordCountFormatted = []
        let wordCount = {}

        axios.get('https://api.engageclimatechange.world/bills').then(response => {
            for (var billIndex in response.data.objects) {
                let summary = response.data.objects[billIndex].summary.replace(/;/g, "").replace(/\(/g, "").replace(/\)/g, "").replace(/:/g, "").replace(/\./g, " ").replace(/-/g, " ").replace(/  /g, " ").split(' ')

                for (var wordIndex in summary) {
                    let word = summary[wordIndex].toUpperCase()
                    if (word === "AND" || word === "THE" || word === "FOR" || word === "THAT" ||word.length < 3) {
                        continue
                    }
                    if (word in wordCount) {
                        wordCount[word] += 1
                    } else {
                        wordCount[word] = 1
                    }
                }
            }

            for (var word in wordCount) {
                var formattedEntry = {
                    text: word,
                    value: wordCount[word]
                }
                wordCountFormatted.push(formattedEntry)
            }
            this.setState({
                isLoaded: false,
                wordCount: wordCount,
                wordCountFormatted: wordCountFormatted
            });

        }).catch(error => {
            console.log("log error", error)
        });

        return false;
    }



    render() {
        const fontSizeMapper = word => Math.log2(word.value) * 3;
        const rotate = word => word.value % 40;
        const {
            isLoaded,
            wordCount,
            wordCountFormatted
        } = this.state;

        return (
          <WordCloud
    data={wordCountFormatted}
    fontSizeMapper={fontSizeMapper}
    rotate={rotate}
    width={800}
    height={800}
  />
        );
    }
}

export default Cloud;
