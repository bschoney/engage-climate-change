import '../../VBackground.css'
import React, { Component } from 'react'
import EmissionsMap from "./EmissionsMap"
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";


class EmissionsMapContainer extends  Component {
    render() {
        return (
            <div class="background-VIS page-body background-fixed">
                <div class="padding-Models" align="center">
                <br/>
                    <h1 class="text-white">Emissions Map</h1>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                	<EmissionsMap/>
                	</CardContent>
                	</Card>
                	<br/>

                </div>
            </div>
       	)
    }
}

export default EmissionsMapContainer