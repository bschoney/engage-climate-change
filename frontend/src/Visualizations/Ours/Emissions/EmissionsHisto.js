import React, {
    createRef,
    Component
} from "react";
import * as d3 from "d3";
import axios from 'axios';
import {
    VictoryChart,
    VictoryBar,
    VictoryTheme
} from "victory";


class EmissionsHisto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            data: []
        };
    }

    drawChart() {
        return (
            <div className='main' style={{ marginTop: '2%' }}>
                  <h2 className='row justify-content-center'>Fossil Fuel Total Consumption in 2017 (Trillion Btu)</h2>
                  <VictoryChart domainPadding={1} width={1600}>
                      <VictoryBar
                          style={{ data: { fill: "#913e0d" } }}
                          data={this.state.data}
                      />
                  </VictoryChart>
              </div>
        );
    }

    getEmissionData() {
        axios.get('https://api.engageclimatechange.world/states').then(response => {
            let states = response.data.objects
            let recents = []
            let dataSet = []
            for (var statesKey in states) {
                let emissions_rec = states[statesKey].emissions_rec;
                recents.push(emissions_rec)
            }

            var counted = 0
            var underInt = 2
            var underVal = 200000

            recents.sort(function(a, b) {
                return a - b; // Ascending
            });
            var currentSet = {
                x: (underVal / 1000 - 200).toString() + "-" + (underVal / 1000).toString(),
                y: 0.0
            }
            for (var datum in recents) {
                if (recents[datum] > 3800000) {
                    break
                }
                while (recents[datum] > underVal) {
                    dataSet.push(currentSet)
                    underVal += 200000
                    underInt += 2
                    currentSet = {
                        x: (underVal / 1000 - 200).toString() + "-" + (underVal / 1000).toString(),
                        y: 0.0
                    }
                }
                currentSet.y += 1.0
                counted += 1
            }
            if (recents.length > counted) {
                dataSet.push({
                    x: ">" + (underVal / 1000 - 200).toString(),
                    y: recents.length - counted
                })
            } else {
                dataSet.push(currentSet)
            }
            this.setState({
                isLoaded: true,
                data: dataSet
            });
        });
        return false;
    }

    componentDidMount() {
        this.getEmissionData();
    }

    render() {
        if (this.state.isLoaded) {
            this.drawChart()
        }
        return this.drawChart();
    }
}

export default EmissionsHisto;
