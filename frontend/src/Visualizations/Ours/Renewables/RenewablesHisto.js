import React, {
    createRef,
    Component
} from "react";
import * as d3 from "d3";
import axios from 'axios';
import {
    VictoryChart,
    VictoryBar,
    VictoryTheme
} from "victory";


class RenewablesHisto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            data: []
        };
    }

    drawChart() {
        return (
            <div className='main' style={{ marginTop: '2%' }}>
                  <h2 className='row justify-content-center'>Net Generation of Renewables in April 2019 (Gigawatthours)</h2>
                  <VictoryChart domainPadding={1} width={1200}>
                      <VictoryBar theme={VictoryTheme.material}
                          style={{ data: { fill: "#4dd2db" } }}
                          data={this.state.data}
                      />
                  </VictoryChart>
              </div>
        );
    }

    getRenewableData() {
        axios.get('https://api.engageclimatechange.world/states').then(response => {
            let states = response.data.objects
            let recents = []
            let dataSet = []
            for (var statesKey in states) {
                let renewables_rec = states[statesKey].renewables_rec;
                recents.push(renewables_rec)
            }

            var counted = 0
            var underInt = 1
            var underVal = 100


            recents.sort(function(a, b) {
                return a - b; // Ascending
            });
            var currentSet = {
                x: (underVal - 100).toString() + "-" + underVal.toString(),
                y: 0.0
            }
            for (var datum in recents) {
                if (recents[datum] > 1600) {
                    break
                }
                while (recents[datum] > underVal) {
                    dataSet.push(currentSet)
                    underVal += 100
                    underInt += 1
                    currentSet = {
                        x: (underVal - 100).toString() + "-" + underVal.toString(),
                        y: 0.0
                    }
                }
                currentSet.y += 1.0
                counted += 1
            }

            if (recents.length > counted) {
                dataSet.push({
                    x: ">" + (underVal - 100).toString(),
                    y: recents.length - counted
                })
            } else {
                dataSet.push(currentSet)
            }

            this.setState({
                isLoaded: true,
                data: dataSet
            });
        });
        return false;
    }

    componentDidMount() {
        this.getRenewableData();
    }

    render() {
        if (this.state.isLoaded) {
            this.drawChart()
        }
        return this.drawChart();
    }
}

export default RenewablesHisto;
