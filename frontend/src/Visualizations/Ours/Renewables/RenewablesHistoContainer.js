import '../../VBackground.css'
import React, { Component } from 'react'
import RenewablesHisto from "./RenewablesHisto"
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";


class RenewablesHistoContainer extends  Component {
    render() {
        return (
            <div class="background-VIS page-body background-fixed">
                <div class="padding-Models" align="center">
                <br/>
                    <h1 class="text-white">Renewables Histogram</h1>
                  <Card style={{background: 'rgba(251, 251, 251, .7)'}}>
                    <CardContent>
                	<RenewablesHisto/>
                	</CardContent>
                	</Card>
                	<br/>

                </div>
            </div>
       	)
    }
}

export default RenewablesHistoContainer