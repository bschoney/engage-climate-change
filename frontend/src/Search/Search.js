import React from 'react'
import {
    Link
} from 'react-router-dom'
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'

import Pagination from "react-js-pagination";
import EL_Card from './EL_Card'
import POL_Card from './POL_Card'
import ST_Card from './ST_Card'
import '../Components/TableEdit.css'
import Input from "@material-ui/core/Input";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const ELURL = "https://api.engageclimatechange.world/bills"
const POLURL = "https://api.engageclimatechange.world/politicians"
const STURL = "https://api.engageclimatechange.world/states"

class Environmental_Legislation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            query: decodeURIComponent(window.location.pathname.split("/").pop()),
            isLoaded: false,
            EL_cardSource: [],
            EL_numCardsPerPage: 3,
            EL_currentPage: 1,
            EL_jsonData: [],
            EL_introduced: null,
            EL_latest_action: null,
            EL_last_vote: null,
            EL_chamber: null,
            EL_supportive_state: null,
            EL_sponsor_party: null,

            POL_cardSource: [],
            POL_numCardsPerPage: 3,
            POL_currentPage: 1,
            POL_jsonData: [],
            POL_introduced: null,
            POL_latest_action: null,
            POL_last_vote: null,
            POL_chamber: null,
            POL_supportive_state: null,
            POL_sponsor_party: null,


            ST_cardSource: [],
            ST_numCardsPerPage: 3,
            ST_currentPage: 1,
            ST_jsonData: [],
            ST_introduced: null,
            ST_latest_action: null,
            ST_last_vote: null,
            ST_chamber: null,
            ST_supportive_state: null,
            ST_sponsor_party: null
        };
        this.fetchELData = this.fetchELData.bind(this);
        this.fetchPOLData = this.fetchPOLData.bind(this);
        this.fetchSTData = this.fetchSTData.bind(this);

        this.EL_handleClick = this.EL_handleClick.bind(this);
        this.EL_handleIntroduced = this.EL_handleIntroduced.bind(this);
        this.EL_handleLatest_Action = this.EL_handleLatest_Action.bind(this);
        this.EL_handleLast_Vote = this.EL_handleLast_Vote.bind(this);
        this.EL_handleChamber = this.EL_handleChamber.bind(this);
        this.EL_handleSupportive_State = this.EL_handleSupportive_State.bind(this);
        this.EL_handleSponsor_Party = this.EL_handleSponsor_Party.bind(this);


        this.POL_handleClick = this.POL_handleClick.bind(this);
        this.POL_handleIntroduced = this.POL_handleIntroduced.bind(this);
        this.POL_handleLatest_Action = this.POL_handleLatest_Action.bind(this);
        this.POL_handleLast_Vote = this.POL_handleLast_Vote.bind(this);
        this.POL_handleChamber = this.POL_handleChamber.bind(this);
        this.POL_handleSupportive_State = this.POL_handleSupportive_State.bind(this);
        this.POL_handleSponsor_Party = this.POL_handleSponsor_Party.bind(this);


        this.ST_handleClick = this.ST_handleClick.bind(this);
        this.ST_handleIntroduced = this.ST_handleIntroduced.bind(this);
        this.ST_handleLatest_Action = this.ST_handleLatest_Action.bind(this);
        this.ST_handleLast_Vote = this.ST_handleLast_Vote.bind(this);
        this.ST_handleChamber = this.ST_handleChamber.bind(this);
        this.ST_handleSupportive_State = this.ST_handleSupportive_State.bind(this);
        this.ST_handleSponsor_Party = this.ST_handleSponsor_Party.bind(this);
    }

    componentDidMount() {
        this.fetchELData().then(this.fetchPOLData()).then(this.fetchSTData())
            .then(this.setState({
                isLoaded: true
            }));
    }

    async fetchELData() {
        let resultData = []
        fetch(this.formatELURL(), {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            .then(res => res.json())
            .then((result) => {
                let source = result.objects
                for (var key in source) {
                    resultData.push(source[key])
                }
            })
            .then(() => this.setState({
                EL_jsonData: resultData
            }))
    }

    async fetchPOLData() {
        let resultData = []
        fetch(this.formatPOLURL(), {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            .then(res => res.json())
            .then((result) => {
                let source = result.objects
                for (var key in source) {
                    resultData.push(source[key])
                }
            })
            .then(() => this.setState({
                POL_jsonData: resultData
            }))
    }

    async fetchSTData() {
        let resultData = []
        fetch(this.formatSTURL(), {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            .then(res => res.json())
            .then((result) => {
                let source = result.objects
                for (var key in source) {
                    resultData.push(source[key])
                }
            })
            .then(() => this.setState({
                ST_jsonData: resultData
            }))
    }

    formatELURL() {
        const {
            EL_introduced,
            EL_latest_action,
            EL_last_vote,
            EL_chamber,
            EL_supportive_state,
            EL_sponsor_party
        } = this.state;
        let searchQuery = ''
        let searchArray = []

        // Title, summary, state
        searchArray.push('{"name": "name", "op": "ilike", "val": "%25' + this.state.query + '%25"}')
        searchArray.push('{"name": "name", "op": "ilike", "val": "' + this.state.query + '"}')
        // No summary since not displayed
        // searchArray.push('{"name": "summary", "op": "ilike", "val": "%25'+ this.state.query + '%25"}')
        searchArray.push('{"name": "state", "op": "ilike", "val": "%25' + this.state.query + '%25"}')
        // With and without wildcard chars, because ????? it works though
        searchArray.push('{"name": "politician_id", "op": "has", "val": {"name": "first_name", "op": "ilike", "val": "%25' + this.state.query + '%25"}}')
        searchArray.push('{"name": "politician_id", "op": "has", "val": {"name": "first_name", "op": "ilike", "val": "' + this.state.query + '"}}')
        searchArray.push('{"name": "politician_id", "op": "has", "val": {"name": "last_name", "op": "ilike", "val": "%25' + this.state.query + '%25"}}')
        searchArray.push('{"name": "politician_id", "op": "has", "val": {"name": "last_name", "op": "ilike", "val": "' + this.state.query + '"}}')

        // Sponsor party
        if (this.state.query.toLowerCase().includes("democrat")) {
            searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "D"}')
        }
        if (this.state.query.toLowerCase().includes("republican")) {
            searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "R"}')
        }
        if (this.state.query.toLowerCase().includes("independent")) {
            searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "i"}')
            searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "id"}')
        }

        // Chamber
        if (this.state.query.toLowerCase().includes("senate")) {
            searchArray.push('{"name": "chamber", "op": "ilike", "val": "s"}')
            searchArray.push('{"name": "chamber", "op": "ilike", "val": "sjres"}')
            searchArray.push('{"name": "chamber", "op": "ilike", "val": "sres"}')
        }
        if (this.state.query.toLowerCase().includes("house") || this.state.query.toLowerCase().includes("respresentatives")) {
            searchArray.push('{"name": "chamber", "op": "ilike", "val": "hr"}')
            searchArray.push('{"name": "chamber", "op": "ilike", "val": "hres"}')
            searchArray.push('{"name": "chamber", "op": "ilike", "val": "hconres"}')
        }

        for (var i = 0; i < searchArray.length; i++) {
            searchQuery += searchArray[i];
            if (i !== searchArray.length - 1) {
                searchQuery += ','
            }
        }

        return (ELURL + '?q={"filters":[{"or":[' + searchQuery + ']}]}')
    }

    formatPOLURL() {
        const {
            POL_introduced,
            POL_latest_action,
            POL_last_vote,
            POL_chamber,
            POL_supportive_state,
            POL_sponsor_party
        } = this.state;
        let searchQuery = ''
        let searchArray = []

        // First and last name
        // With and without wildcard chars, because ????? it works though
        searchArray.push('{"name": "first_name", "op": "ilike", "val": "%25' + this.state.query + '%25"}')
        searchArray.push('{"name": "last_name", "op": "ilike", "val": "%25' + this.state.query + '%25"}')
        searchArray.push('{"name": "state", "op": "ilike", "val": "%25' + this.state.query + '%25"}')

        // Party
        if (this.state.query.toLowerCase().includes("democrat")) {
            searchArray.push('{"name": "party", "op": "ilike", "val": "D"}')
        }
        if (this.state.query.toLowerCase().includes("republican")) {
            searchArray.push('{"name": "party", "op": "ilike", "val": "R"}')
        }
        if (this.state.query.toLowerCase().includes("independent")) {
            searchArray.push('{"name": "party", "op": "ilike", "val": "i"}')
            searchArray.push('{"name": "party", "op": "ilike", "val": "id"}')
        }

        // Chamber
        if (this.state.query.toLowerCase().includes("senate")) {
            searchArray.push('{"name": "bills", "op": "any", "val": {"name": "chamber", "op": "ilike", "val": "s"}}')
            searchArray.push('{"name": "bills", "op": "any", "val": {"name": "chamber", "op": "ilike", "val": "sjres"}}')
            searchArray.push('{"name": "bills", "op": "any", "val": {"name": "chamber", "op": "ilike", "val": "sres"}}')
        }
        if (this.state.query.toLowerCase().includes("house") || this.state.query.toLowerCase().includes("respresentatives")) {
            searchArray.push('{"name": "bills", "op": "any", "val": {"name": "chamber", "op": "ilike", "val": "hr"}}')
            searchArray.push('{"name": "bills", "op": "any", "val": {"name": "chamber", "op": "ilike", "val": "hres"}}')
            searchArray.push('{"name": "bills", "op": "any", "val": {"name": "chamber", "op": "ilike", "val": "hconres"}}')
        }

        for (var i = 0; i < searchArray.length; i++) {
            searchQuery += searchArray[i];
            if (i !== searchArray.length - 1) {
                searchQuery += ','
            }
        }

        return (POLURL + '?q={"filters":[{"or":[' + searchQuery + ']}]}')
    }

    formatSTURL() {
        const {
            ST_introduced,
            ST_latest_action,
            ST_last_vote,
            ST_chamber,
            ST_supportive_state,
            ST_sponsor_party
        } = this.state;
        let searchQuery = ''
        let searchArray = []

        // State name and abbreviation
        searchArray.push('{"name": "statename", "op": "ilike", "val": "%25' + this.state.query + '%25"}')
        searchArray.push('{"name": "id", "op": "ilike", "val": "%25' + this.state.query + '%25"}')

        for (var i = 0; i < searchArray.length; i++) {
            searchQuery += searchArray[i];
            if (i !== searchArray.length - 1) {
                searchQuery += ','
            }
        }

        return (STURL + '?q={"filters":[{"or":[' + searchQuery + ']}]}')
    }

    createELCards() {
        let source = this.state.EL_jsonData;
        var cards = [];

        for (var key in source) {
            cards.push(<EL_Card sentToELCard={source[key]} sendQuery={this.state.query}/>);
        };
        this.state.EL_cardSource = cards;
    }

    createPOLCards() {
        let source = this.state.POL_jsonData;
        var cards = [];

        for (var key in source) {
            cards.push(<POL_Card sentToPOLCard={source[key]} sendQuery={this.state.query}/>);
        };
        this.state.POL_cardSource = cards;
    }

    createSTCards() {
        let source = this.state.ST_jsonData;
        var cards = [];

        for (var key in source) {
            cards.push(<ST_Card sentToSTCard={source[key]} sendQuery={this.state.query}/>);
        };
        this.state.ST_cardSource = cards;
    }

    // Handles specific onClick conditions for Pagination
    EL_handleClick(event) {
        this.setState({
            EL_currentPage: event
        })
    }
    EL_handleIntroduced(value) {
        this.setState({
            EL_introduced: value
        }, () => this.fetchELData());
    }
    EL_handleLatest_Action(value) {
        this.setState({
            EL_latest_action: value
        }, () => this.fetchELData());
    }
    EL_handleLast_Vote(value) {
        this.setState({
            EL_last_vote: value
        }, () => this.fetchELData());
    }
    EL_handleChamber(value) {
        this.setState({
            EL_chamber: value
        }, () => this.fetchELData());
    }
    EL_handleSupportive_State(value) {
        this.setState({
            EL_supportive_state: value
        }, () => this.fetchELData());
    }
    EL_handleSponsor_Party(value) {
        this.setState({
            EL_sponsor_party: value
        }, () => this.fetchELData());
    }

    // Handles specific onClick conditions for Pagination
    POL_handleClick(event) {
        this.setState({
            POL_currentPage: event
        })
    }
    POL_handleIntroduced(value) {
        this.setState({
            POL_introduced: value
        }, () => this.fetchPOLData());
    }
    POL_handleLatest_Action(value) {
        this.setState({
            POL_latest_action: value
        }, () => this.fetchPOLData());
    }
    POL_handleLast_Vote(value) {
        this.setState({
            POL_last_vote: value
        }, () => this.fetchPOLData());
    }
    POL_handleChamber(value) {
        this.setState({
            POL_chamber: value
        }, () => this.fetchPOLData());
    }
    POL_handleSupportive_State(value) {
        this.setState({
            POL_supportive_state: value
        }, () => this.fetchPOLData());
    }
    POL_handleSponsor_Party(value) {
        this.setState({
            POL_sponsor_party: value
        }, () => this.fetchPOLData());
    }

    // Handles specific onClick conditions for Pagination
    ST_handleClick(event) {
        this.setState({
            ST_currentPage: event
        })
    }
    ST_handleIntroduced(value) {
        this.setState({
            ST_introduced: value
        }, () => this.fetchSTData());
    }
    ST_handleLatest_Action(value) {
        this.setState({
            ST_latest_action: value
        }, () => this.fetchSTData());
    }
    ST_handleLast_Vote(value) {
        this.setState({
            ST_last_vote: value
        }, () => this.fetchSTData());
    }
    ST_handleChamber(value) {
        this.setState({
            ST_chamber: value
        }, () => this.fetchSTData());
    }
    ST_handleSupportive_State(value) {
        this.setState({
            ST_supportive_state: value
        }, () => this.fetchSTData());
    }
    ST_handleSponsor_Party(value) {
        this.setState({
            ST_sponsor_party: value
        }, () => this.fetchSTData());
    }

    render() {
        this.createELCards()
        this.createPOLCards()
        this.createSTCards()
        const {
            isLoaded,
            EL_cardSource,
            EL_numCardsPerPage,
            EL_currentPage,
            POL_cardSource,
            POL_numCardsPerPage,
            POL_currentPage,
            ST_cardSource,
            ST_numCardsPerPage,
            ST_currentPage
        } = this.state;

        // Pagination
        const EL_lastPageNum = EL_currentPage * EL_numCardsPerPage;
        const EL_firstPageNum = EL_lastPageNum - EL_numCardsPerPage;
        const EL_currentCards = EL_cardSource.slice(EL_firstPageNum, EL_lastPageNum);
        const EL_renderCards = EL_currentCards.map((EL_cardSource, index) => {
            return EL_cardSource
        });

        const POL_lastPageNum = POL_currentPage * POL_numCardsPerPage;
        const POL_firstPageNum = POL_lastPageNum - POL_numCardsPerPage;
        const POL_currentCards = POL_cardSource.slice(POL_firstPageNum, POL_lastPageNum);
        const POL_renderCards = POL_currentCards.map((POL_cardSource, index) => {
            return POL_cardSource
        });

        const ST_lastPageNum = ST_currentPage * ST_numCardsPerPage;
        const ST_firstPageNum = ST_lastPageNum - ST_numCardsPerPage;
        const ST_currentCards = ST_cardSource.slice(ST_firstPageNum, ST_lastPageNum);
        const ST_renderCards = ST_currentCards.map((ST_cardSource, index) => {
            return ST_cardSource
        });


        return (
            <div class="background-Search">
        <div class="myBg">
        <div className="container">
          <div className="page-header">
          <br></br>
          <br></br>
          <br></br>
           <h1 class="text-white center-title">Search results for <b>"{this.state.query}"</b></h1>
          </div>
        </div>
        <br></br>
        <h3 class="text-white">Environmental Legislation <span class="smol-text">{EL_cardSource.length} sources</span></h3>
        <table class="table table-bordered table-hover table-striped  table-light">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Legislation Title</th>
              <th scope="col">Introduced</th>
              <th scope="col">Recent Action</th>
              <th scope="col">Recent Vote Date</th>
              <th scope="col">Chamber</th>
              <th scope="col">Supportive State</th>
              <th scope="col">Sponsor</th>
              <th scope="col">Sponsor Party</th>
            </tr>
          </thead>
          <tbody>
            {EL_renderCards}
          </tbody>
        </table>
        <Pagination
          activePage={EL_currentPage}
          itemsCountPerPage={EL_numCardsPerPage}
          totalItemsCount={EL_cardSource.length}
          onChange={this.EL_handleClick}
          itemClass="page-item"
          linkClass="page-link"
          innerClass="pagination justify-content-center"
        />


        <h3 class="text-white">Politicians <span class="smol-text">{POL_cardSource.length} sources</span></h3>
        <table class="table table-bordered table-hover table-striped table-light">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Politician</th>
              <th scope="col">State</th>
              <th scope="col">Party</th>
              <th scope="col">Chamber</th>
              <th scope="col">Date of Birth</th>
              <th scope="col">Recent Vote Date</th>
              <th scope="col">In Office?</th>
            </tr>
          </thead>
          <tbody>
            {POL_renderCards}
          </tbody>
        </table>
        <Pagination
          activePage={POL_currentPage}
          itemsCountPerPage={POL_numCardsPerPage}
          totalItemsCount={POL_cardSource.length}
          onChange={this.POL_handleClick}
          itemClass="page-item"
          linkClass="page-link"
          innerClass="pagination justify-content-center"
        />


        <h3 class="text-white">States <span class="smol-text">{ST_cardSource.length} sources</span></h3>
        <table class="table table-bordered table-hover table-striped table-light">
          <thead class="thead-dark">
            <tr>
              <th scope="col">State</th>
              <th scope="col">State Abbr.</th>
            </tr>
          </thead>
          <tbody>
            {ST_renderCards}
          </tbody>
        </table>
        <Pagination
          activePage={ST_currentPage}
          itemsCountPerPage={ST_numCardsPerPage}
          totalItemsCount={ST_cardSource.length}
          onChange={this.ST_handleClick}
          itemClass="page-item"
          linkClass="page-link"
          innerClass="pagination justify-content-center"
        />
      </div>
      </div>
        )
    }
}

export default Environmental_Legislation
