import React, {
    Component
} from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import Highlighter from "react-highlight-words";
import '../Components/CardContainer.css'
import '../Components/TableEdit.css'

class POL_Card extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    truncate(s) {
        if (!s) {
            return "Article has no name avaliable"
        }

        const truncValue = 70
        if (s.length > truncValue) {
            var a = "" + s.substring(0, truncValue) + "..."
            return a
        } else {
            var spacing = truncValue - s.length
            var newS = s

            for (var i = 0; i < spacing; i++) {
                newS += " "
            }
            return newS;
        }
    }

    reformat_date(time) {
        let result = "";
        result += time.substring(5, 7);
        result += ("-" + time.substring(8, 10));
        result += ("-" + time.substring(0, 4));
        return result;
    }

    render() {
        const holder = this.props.sentToPOLCard
        const query = this.props.sendQuery
        const pathID = "/Politicians/" + holder.id
        let chamber = "Not Available"
        let party = ""
        let in_office = "No"

        if (holder.bills.count != 0 && holder.bills[0].chamber[0] === 'h') {
            chamber = "House of Representatives"
        } else {
            chamber = "Senate"
        }

        if (holder.in_office) {
            in_office = "Yes"
        }

        if (holder.party == "R") {
            party = "Republican"
        } else if (holder.party == "D") {
            party = "Democrat"
        } else {
            party = "Independent"
        }

        const buildHighPTitle = (val) => {
            return (
                <th scope="row" align="center"><a href={pathID}>
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={val}
          />
        </a></th>
            )
        }

        const buildHighP = (val) => {
            return (
                <td align="center">
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={val}
          />
        </td>
            )
        }

        return (
            <tr>
          {buildHighPTitle(holder.first_name + ' ' + holder.last_name)}
          {buildHighP(holder.state)}
          {buildHighP(party)}
          {buildHighP(chamber)}
          <td align="center">{this.reformat_date(holder.dob)}</td>
          <td align="center">{this.reformat_date(holder.recent_vote)}</td>
          <td align="center">{in_office}</td>
        </tr>
        )
    }
}

export default POL_Card
