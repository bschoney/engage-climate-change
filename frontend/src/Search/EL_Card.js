import React, {
    Component
} from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import Highlighter from "react-highlight-words";
import '../Components/CardContainer.css'
import '../Components/TableEdit.css'

class EL_Card extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    truncate(s) {
        if (!s) {
            return "Article has no name avaliable"
        }

        const truncValue = 70
        if (s.length > truncValue) {
            var a = "" + s.substring(0, truncValue) + "..."
            return a
        } else {
            var spacing = truncValue - s.length
            var newS = s

            for (var i = 0; i < spacing; i++) {
                newS += " "
            }
            return newS;
        }
    }

    reformat_date(time) {
        let result = "";
        result += time.substring(5, 7);
        result += ("-" + time.substring(8, 10));
        result += ("-" + time.substring(0, 4));
        return result;
    }

    render() {
        const holder = this.props.sentToELCard
        const query = this.props.sendQuery
        const pathID = "/Environmental_Legislation/" + holder.id
        let articleName = holder.name
        let myChamber = ""
        let last_vote = holder.last_vote
        let sponsor_p = ""
        let sponsor_name = ""
        if (holder.chamber[0] == "s") {
            myChamber = "Senate"
        } else {
            myChamber = "House of Representatives"
        }

        if (!articleName) {
            articleName = "-Article's Title is currently unavailable-"
        }

        if (!holder.last_vote) {
            last_vote = "Unknown"
        } else {
            last_vote = this.reformat_date(holder.last_vote)
        }
        if (holder.sponsor_party[0] == "R") {
            sponsor_p = "Republican"
        } else if (holder.sponsor_party[0] == "D") {
            sponsor_p = "Democrat"
        } else {
            sponsor_p = "Independent"
        }

        const buildHighBTitle = (val) => {
            return (
                <th scope="row" align="center"><a href={pathID}>
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={val}
          />
        </a></th>
            )
        }

        const buildHighB = (val) => {
            return (
                <td align="center">
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={val}
          />
        </td>
            )
        }

        return (
            <tr>
          {buildHighBTitle(articleName)}
          <td align="center">{this.reformat_date(holder.introduced)}</td>
          <td align="center">{this.reformat_date(holder.latest_action)}</td>
          <td align="center">{last_vote}</td>
          {buildHighB(myChamber)}
          {buildHighB(holder.state)}
          {buildHighB(holder.politician_id.first_name + ' ' + holder.politician_id.last_name)}
          {buildHighB(sponsor_p)}
        </tr>
        )
    }
}

export default EL_Card
