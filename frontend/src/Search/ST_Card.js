import React, {
    Component
} from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import Highlighter from "react-highlight-words";
import '../Components/CardContainer.css'
import '../Components/TableEdit.css'

class POL_Card extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    truncate(s) {
        if (!s) {
            return "Article has no name avaliable"
        }

        const truncValue = 70
        if (s.length > truncValue) {
            var a = "" + s.substring(0, truncValue) + "..."
            return a
        } else {
            var spacing = truncValue - s.length
            var newS = s

            for (var i = 0; i < spacing; i++) {
                newS += " "
            }
            return newS;
        }
    }

    reformat_date(time) {
        let result = "";
        result += time.substring(5, 7);
        result += ("-" + time.substring(8, 10));
        result += ("-" + time.substring(0, 4));
        return result;
    }


    render() {
        const holder = this.props.sentToSTCard
        const query = this.props.sendQuery
        const pathID = "/States/" + holder.id

        const buildHighSTitle = (val) => {
            return (
                <th scope="row" align="center"><a href={pathID}>
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={val}
          />
        </a></th>
            )
        }

        const buildHighS = (val) => {
            return (
                <td align="center">
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={val}
          />
        </td>
            )
        }

        return (
            <tr>
          {buildHighSTitle(holder.statename)}
          {buildHighS(holder.id)}
        </tr>
        )
    }
}

export default POL_Card
