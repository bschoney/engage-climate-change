import React from 'react'
import {
    Link
} from 'react-router-dom'
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'
import '../Components/CardContainer.css'
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

class EL_Instances extends React.Component {
    constructor(props) {
        super(props)
        let pathing = this.props.match.params.id

        this.state = {
            isLoaded: false,
            propsPath: pathing,
            jsonData: []
        }
        this.setState({
            propsPath: pathing
        })
    }

    componentDidMount() {
        this.fetchData()
            .then(this.setState({
                isLoaded: true
            }))
    }

    async fetchData() {
        await fetch('https://api.engageclimatechange.world/bills/' + this.props.match.params.id, {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then((result) => {
                this.setState({
                    jsonData: result
                });
            });
    }

    reformat_date(time) {
        let result = "";
        if (!time) {
            result = "Information currently unavaliable"
        } else {
            result += time.substring(5, 7);
            result += ("-" + time.substring(8, 10));
            result += ("-" + time.substring(0, 4));
        }
        return result;
    }

    buildTitle() {
        const {
            jsonData
        } = this.state
        let articleTitle = jsonData.name

        if (!articleTitle) {
            articleTitle = "Article's Title is currently unavaliable at the moment."
        }

        let myTitle = (
            <div class="container">
                <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
                    <CardContent>
                        <div className="page-header" align="center">
                            <h4 class="text-white"><b>{articleTitle}</b></h4>
                        </div>
                    </CardContent>
                </Card>
            </div>
        )

        return myTitle
    }

    buildLinks() {
        const {
            jsonData
        } = this.state

        let politician_holder = jsonData.politician_id
        let politician_data = []
        for (var keys in politician_holder) {
            politician_data.push(politician_holder[keys])
        }

        let nameCreator = politician_data[2] + " " + politician_data[5]
        let nameLink = "/Politicians/" + jsonData.sponsor;
        let stateLink = "/States/" + jsonData.state;

        let myLinks = (
            <div class="container">
                <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
                    <CardContent>
                        <div className="page-header" align="center">
                            <h3 class="text-white"><b class="border-bottom">Legislation's Related Links</b></h3>
                        </div><br></br>
                        <div class="row d-flex justify-content-center">
                            <Link to={stateLink}>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card-link card-white border-0 shadow">
                                        <div class="card-body text-center">
                                            <h5 class="card-title mb-0">Related:</h5>
                                            <h7 class="card-title mb-0">{jsonData.state}</h7>
                                            <hr></hr>
                                            <p>State that brought forth this legislation</p>
                                        </div>
                                    </div>
                                </div>
                            </Link>

                            <Link to={nameLink}>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card-link card-white border-0 shadow">
                                        <div class="card-body text-center">
                                            <h5 class="card-title mb-0">Related:</h5>
                                            <h7 class="card-title mb-0">{nameCreator}</h7>
                                            <hr></hr>
                                            <p>Politician that sponsored this legislation</p>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </CardContent>
                </Card>
            </div>
        )

        return myLinks
    }

    buildSummary() {
        const {
            jsonData
        } = this.state

        let summary = ""
        if (jsonData.summary == "") {
            summary = "Summary unavaliable at the moment."
        } else {
            summary = jsonData.summary
        }

        let mySummary = (
            <div class="container">
                <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
                    <CardContent>
                        <div className="page-header" align="center">
                            <h3 class="text-white"><b class="border-bottom">Summary</b></h3>
                        </div><br></br>
                        <h6 class="text-white">{summary}</h6>
                    </CardContent>
                </Card>
            </div>
        )

        return mySummary
    }

    buildSpecsTable() {
        const {
            jsonData
        } = this.state

        let myChamber = ""
        let last_vote = ""
        let sponsor_p = ""

        if (jsonData.chamber == "s") {
            myChamber = "Senate"
        } else {
            myChamber = "House of Representatives"
        }

        if (!jsonData.last_vote) {
            last_vote += "Information unavaliable"
        } else {
            last_vote += this.reformat_date(jsonData.last_vote)
        }

        if (jsonData.sponsor_party == "R") {
            sponsor_p = "Republican"
        } else if (jsonData.sponsor_party == "D") {
            sponsor_p = "Democrat"
        } else {
            sponsor_p = "Independent"
        }

        let politician_holder = jsonData.politician_id
        let politician_data = []
        for (var keys in politician_holder) {
            politician_data.push(politician_holder[keys])
        }

        let nameCreator = politician_data[2] + " " + politician_data[5]
        let nameLink = "/Politicians/" + jsonData.sponsor;
        let stateLink = "/States/" + jsonData.state;

        const date = (time) => {
            var tokenized = time.split(" ", 4)
            var result = tokenized[0]
            for (var i = 1; i < 4; i++) {
                result = result.concat(" " + tokenized[i])
            }
            return result
        }

        let myTable = (
            <div class="container">
            <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
                <CardContent>
                    <div className="page-header" align="center">
                        <h3 class="text-white"><b>Specifications</b></h3>
                    </div><hr></hr>
                    <table class="table table-bordered table-hover table-striped table-responsive table-dark">
                        <thead class="thead-dark"><tr>
                            <th title="Field #1" width="10%">Legislation's Attribute</th>
                            <th title="Field #2" width="10%">Legislation's Specifications</th>
                        </tr></thead>
                        <tbody>
                            <tr>
                                <td align="left"><b>First Introduction Date</b></td>
                                <td align="right">{this.reformat_date(jsonData.introduced)}</td>
                            </tr>
                            <tr>
                                <td align="left"><b>Date of Last Action</b></td>
                                <td align="right">{this.reformat_date(jsonData.latest_action)}</td>
                            </tr>
                            <tr>
                                <td align="left"><b>Date of Last Vote</b></td>
                                <td align="right">{last_vote}</td>
                            </tr>
                            <tr>
                                <td align="left"><b>Supportive Chamber</b></td>
                                <td align="right">{myChamber}</td>
                            </tr>
                            <tr>
                                <td align="left"><b>Supportive Party</b></td>
                                <td align="right">{sponsor_p}</td>
                            </tr>
                            <tr>
                                <td align="left"><b>Supportive State</b></td>
                                <td align="right">{jsonData.state}</td>
                            </tr>
                            <tr>
                                <td align="left"><b>Supportive Politician</b></td>
                                <td align="right">{nameCreator}</td>
                            </tr>
                        </tbody>
                    </table>
                </CardContent>
            </Card>
            </div>
        )

        return myTable
    }

    render() {
        const {
            isLoaded,
        } = this.state

        if (!isLoaded) {
            return (<div> <p>Loading ...</p> </div>);
        } else {
            return (
                <div class="background-EL-Instances background-fixed">
                    <div class="padding-Models">
                    <br></br><br></br><br></br><br></br>
                    {this.buildTitle()}<br></br><br></br>
                    {this.buildSummary()}<br></br><br></br>
                    {this.buildSpecsTable()}<br></br><br></br>
                    {this.buildLinks()}<br></br><br></br>
                    </div>
                </div>
            );
        }
    }
}

export default EL_Instances