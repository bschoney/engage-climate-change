import React from 'react'
import {
    Link
} from 'react-router-dom'
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'

import Pagination from "react-js-pagination";
import EL_Card from './EL_Card'
import '../Components/TableEdit.css'
import Input from "@material-ui/core/Input";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const ELURL = 'https://api.engageclimatechange.world/bills'
const ANY = 'is_null'

class Environmental_Legislation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cardSource: [],
            isLoaded: false,
            numCardsPerPage: 10,
            currentPage: 1,
            jsonData: [],
            searchQueryValue: null,
            searchQueryMap: '{"or":[]}',
            sortAssignment: [[''], ['']],
            sortQueryValue: ['asc', 'name'],
            sortQueryMap: '"order_by":[]',
            filterAssignment: {
                'intrdouced': '',
                'last_vote': '',
                'latest_action': '',
                'state': '',
                'sponsor_party': '',
                'chamber': ''
            },
            filterQueryValue: {
                'introduced': [null, null],
                'last_vote': [null, null],
                'latest_action': [null, null],
                'state': [null],
                'sponsor_party': [null, null],
                'chamber': [null, null]
            },
            filterQueryMap: '"filters":[{"and":[]}',
        };
        this.handleClick = this.handleClick.bind(this);
        this.fetchData = this.fetchData.bind(this);
    }

    componentDidMount() {
        this.fetchData()
            .then(this.setState({
                isLoaded: true
            }));
    }

    async fetchData() {
        let resultData = []
        fetch(this.formatURL(), {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then((result) => {
                let source = result.objects
                for (var key in source) {
                    resultData.push(source[key])
                }
            })
            .then(() => this.setState({
                jsonData: resultData
            }))
    }

    formatURL() {
        const {
            sortQueryMap,
            filterQueryMap,
            searchQueryMap
        } = this.state;

        let sortQuery = ''
        let filterQuery = ''
        let filterArray = []

        return (ELURL + '?q={' + sortQueryMap + ',' + filterQueryMap + ',' + searchQueryMap + ']}')
    }

    createCards() {
        let source = this.state.jsonData;
        var cards = [];

        for (var key in source) {
            cards.push(<EL_Card sentToELCard={source[key]} sendSearchQuery={this.state.searchQueryValue}/>);
        };
        this.state.cardSource = cards;
    }

    handleSortQuery(value) {
        let valueArray = value.split(" ")
        const {
            sortQueryValue
        } = this.state;
        let sortDirection = sortQueryValue[0];
        let sortField = sortQueryValue[1];

        let newSortAssignment = this.state.sortAssignment.slice()
        if (valueArray[0] == 0) {
            sortDirection = valueArray[1]
            newSortAssignment[0] = value
        } else {
            sortField = valueArray[1]
            newSortAssignment[1] = value
        }

        let sortQuery = '"order_by":[{"field":"' + sortField + '","direction":"' + sortDirection + '"}]'
        this.setState({
            sortQueryValue: [sortDirection, sortField],
            sortQueryMap: sortQuery,
            sortAssignment: newSortAssignment
        }, () => this.fetchData());
    }

    handleFilterQuery(value) {
        let valueArray = value.split(" ")
        const {
            filterQueryValue,
            filterAssignment
        } = this.state;
        let filterQuery = ''
        let filterArray = []

        /* Updates filterQueryValue with new values */
        for (var keys in filterQueryValue) {
            if (keys == valueArray[0]) {
                filterQueryValue[keys][0] = valueArray[1]
                filterQueryValue[keys][1] = valueArray[2]
                filterAssignment[keys] = value
            }
        }

        /* Makes filterQuery*/
        for (var keys in filterQueryValue) {
            if (filterQueryValue[keys][0] != null && filterQueryValue[keys][0] != 'any') {
                if (filterQueryValue[keys][0] == 'null') {
                    filterArray.push('{"name":"' + keys + '","op": "is_null"}')
                } else {
                    filterArray.push('{"name":"' + keys + '","op":"ge","val":"' + filterQueryValue[keys][0] + '"},{"name":"' + keys + '","op":"le","val":"' + filterQueryValue[keys][1] + '"}')
                }
            }
        }

        /* Adds appropriate commas to queries */
        for (var i = 0; i < filterArray.length; i++) {
            filterQuery += filterArray[i];
            if (i !== filterArray.length - 1) {
                filterQuery += ','
            }
        }

        let result = '"filters":[{"and":[' + filterQuery + ']}'
        this.setState({
            filterQueryMap: result
        }, () => this.fetchData());
    }

    handleSearchQuery(value) {
        let mySearchQuery = ''
        let searchArray = []

        if (value != null && value != '') {
            // Sponsor party
            if (value.toLowerCase().includes("democrat")) {
                searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "D"}')
            }
            if (value.toLowerCase().includes("republican")) {
                searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "R"}')
            }
            if (value.toLowerCase().includes("independent")) {
                searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "i"}')
                searchArray.push('{"name": "sponsor_party", "op": "ilike", "val": "id"}')
            }

            // Chamber
            if (value.toLowerCase().includes("senate")) {
                searchArray.push('{"name": "chamber", "op": "ilike", "val": "s"}')
                searchArray.push('{"name": "chamber", "op": "ilike", "val": "sjres"}')
                searchArray.push('{"name": "chamber", "op": "ilike", "val": "sres"}')
            }
            if (value.toLowerCase().includes("house") || value.toLowerCase().includes("respresentatives")) {
                searchArray.push('{"name": "chamber", "op": "ilike", "val": "hr"}')
                searchArray.push('{"name": "chamber", "op": "ilike", "val": "hres"}')
                searchArray.push('{"name": "chamber", "op": "ilike", "val": "hconres"}')
            }

            // State
            searchArray.push('{"name": "state", "op": "ilike", "val": "%25' + value + '%25"}')

            // Pol Name
            searchArray.push('{"name": "politician_id", "op": "has", "val": {"name": "first_name", "op": "ilike", "val": "%25' + value + '%25"}}')
            searchArray.push('{"name": "politician_id", "op": "has", "val": {"name": "last_name", "op": "ilike", "val": "%25' + value + '%25"}}')

            // Article Title 
            searchArray.push('{"name":"name","op":"ilike","val":"%25' + value + '%25"}')
        }

        /* Adds the commas to the query at appropriate positions */
        for (var i = 0; i < searchArray.length; i++) {
            mySearchQuery += searchArray[i];
            if (i !== searchArray.length - 1) {
                mySearchQuery += ','
            }
        }

        let myValue = value
        if (myValue == '') {
            myValue = null
        }

        let myQuery = '{"or":[' + mySearchQuery + ']}'
        this.setState({
            searchQueryValue: myValue,
            searchQueryMap: myQuery
        }, () => this.fetchData());
    }

    // Handles specific onClick conditions for Pagination
    handleClick(event) {
        this.setState({
            currentPage: event
        })
    }

    createDates(name_id, beg, last) {
        var result = [];
        for (var year = beg; year <= last; year++) {
            result.push(<MenuItem value={'' + name_id + ' ' + year + '-01-01' + ' ' + year + '-12-31'}>{year}</MenuItem>);
        }
        return result
    }

    render() {
        this.createCards()
        const {
            cardSource,
            isLoaded,
            numCardsPerPage,
            currentPage,
            searchQueryValue,
            sortAssignment,
            filterAssignment,
        } = this.state;

        // Pagination
        const lastPageNum = currentPage * numCardsPerPage;
        const firstPageNum = lastPageNum - numCardsPerPage;
        const currentCards = cardSource.slice(firstPageNum, lastPageNum);
        const renderCards = currentCards.map((cardSource, index) => {
            return cardSource
        });

        /* 
    Holders things
  
    onChange={(e) => {this.setState({searchQuery: e.target.value.trim()})}}
                onKeyDown={event => this.handleSearch(event.target.value)}

    */
        return (
            <div class="background-EL page-body background-fixed">
      <div class="padding-Models">
        <div className="container">
          <div className="page-header">
              <h1 class="text-white center-title">Environmental Legislation</h1>
              <br></br>
          </div>
        </div>
        <div class="container-white-bills">
        <div class="container">
            <div class="row">
              <div class="col">
              <TextField
                style={{width: "100%"}}
                id="ELSearch"
                placeholder="Search ..."
                onChange={event => this.handleSearchQuery(event.target.value)}
              />
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Sort Direction</InputLabel>
                  <Select
                    value={sortAssignment[0]}
                    onChange={event => this.handleSortQuery(event.target.value)}
                  >
                    <MenuItem value={'0 asc'}>Any</MenuItem>
                    <MenuItem value={'0 asc'}>Ascending</MenuItem>
                    <MenuItem value={'0 desc'}>Descending</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Sort By Attribute Name</InputLabel>
                  <Select
                    value={sortAssignment[1]}
                    onChange={event => this.handleSortQuery(event.target.value)}
                  >
                    <MenuItem value={'1 name'}>Any</MenuItem>
                    <MenuItem value={'1 name'}>Name</MenuItem>
                    <MenuItem value={'1 introduced'}>Introduction Date</MenuItem>
                    <MenuItem value={'1 latest_action'}>Latest Action Date</MenuItem>
                    <MenuItem value={'1 last_vote'}>Last Vote Date</MenuItem>
                    <MenuItem value={'1 chamber'}>Chamber</MenuItem>
                    <MenuItem value={'1 state'}>Supportive State</MenuItem>
                    <MenuItem value={'1 sponsor_party'}>Sponsor Party</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Introduction Date</InputLabel>
                  <Select
                    value={filterAssignment["introduced"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'introduced 2001-01-01 2019-12-31'}>Any</MenuItem>
                    {this.createDates('introduced', 2001, 2019)}
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Latest Action Date</InputLabel>
                  <Select
                    value={filterAssignment["latest_action"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'latest_action 2001-01-01 2019-12-31'}>Any</MenuItem>
                    {this.createDates('latest_action', 2001, 2019)}
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Last Vote Date</InputLabel>
                  <Select
                    value={filterAssignment["last_vote"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'last_vote any any'}>Any</MenuItem>
                    <MenuItem value={'last_vote null null'}>Unknown Date</MenuItem>
                    {this.createDates('last_vote', 2001, 2019)}
                  </Select>
                </FormControl>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Chamber</InputLabel>
                  <Select
                    value={filterAssignment["chamber"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'chamber a z'}>Any</MenuItem>
                    <MenuItem value={'chamber s t'}>House of Senate</MenuItem>
                    <MenuItem value={'chamber h i'}>House of Representatives</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Supportive State</InputLabel>
                  <Select
                    value={filterAssignment["state"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'state AA ZZ'}>Any</MenuItem>
                    <MenuItem value={'state AL AL'}>AL</MenuItem>
                    <MenuItem value={'state AK AK'}>AK</MenuItem>
                    <MenuItem value={'state AZ AZ'}>AZ</MenuItem>
                    <MenuItem value={'state AR AR'}>AR</MenuItem>
                    <MenuItem value={'state CA CA'}>CA</MenuItem>
                    <MenuItem value={'state CO CO'}>CO</MenuItem>
                    <MenuItem value={'state CT CT'}>CT</MenuItem>
                    <MenuItem value={'state DE DE'}>DE</MenuItem>
                    <MenuItem value={'state FL FL'}>FL</MenuItem>
                    <MenuItem value={'state GA GA'}>GA</MenuItem>
                    <MenuItem value={'state HI HI'}>HI</MenuItem>
                    <MenuItem value={'state ID ID'}>ID</MenuItem>
                    <MenuItem value={'state IL IL'}>IL</MenuItem>
                    <MenuItem value={'state IN IN'}>IN</MenuItem>
                    <MenuItem value={'state IA IA'}>IA</MenuItem>
                    <MenuItem value={'state KS KS'}>KS</MenuItem>
                    <MenuItem value={'state KY KY'}>KY</MenuItem>
                    <MenuItem value={'state LA LA'}>LA</MenuItem>
                    <MenuItem value={'state MD MD'}>MD</MenuItem>
                    <MenuItem value={'state ME ME'}>ME</MenuItem>
                    <MenuItem value={'state MA MA'}>MA</MenuItem>
                    <MenuItem value={'state MI MI'}>MI</MenuItem>
                    <MenuItem value={'state MN MN'}>MN</MenuItem>
                    <MenuItem value={'state MS MS'}>MS</MenuItem>
                    <MenuItem value={'state MO MO'}>MO</MenuItem>
                    <MenuItem value={'state MT MT'}>MT</MenuItem>
                    <MenuItem value={'state NE NE'}>NE</MenuItem>
                    <MenuItem value={'state NV NV'}>NV</MenuItem>
                    <MenuItem value={'state NH NH'}>NH</MenuItem>
                    <MenuItem value={'state NJ NJ'}>NJ</MenuItem>
                    <MenuItem value={'state NM NM'}>NM</MenuItem>
                    <MenuItem value={'state NY NY'}>NY</MenuItem>
                    <MenuItem value={'state NC NC'}>NC</MenuItem>
                    <MenuItem value={'state ND ND'}>ND</MenuItem>
                    <MenuItem value={'state OH OH'}>OH</MenuItem>
                    <MenuItem value={'state OK OK'}>OK</MenuItem>
                    <MenuItem value={'state OR OR'}>OR</MenuItem>
                    <MenuItem value={'state PA PA'}>PA</MenuItem>
                    <MenuItem value={'state RI RI'}>RI</MenuItem>
                    <MenuItem value={'state SC SC'}>SC</MenuItem>
                    <MenuItem value={'state SD SD'}>SD</MenuItem>
                    <MenuItem value={'state TN TN'}>TN</MenuItem>
                    <MenuItem value={'state TX TX'}>TX</MenuItem>
                    <MenuItem value={'state UT UT'}>UT</MenuItem>
                    <MenuItem value={'state VT VT'}>VT</MenuItem>
                    <MenuItem value={'state VA VA'}>VA</MenuItem>
                    <MenuItem value={'state WA WA'}>WA</MenuItem>
                    <MenuItem value={'state WV WV'}>WV</MenuItem>
                    <MenuItem value={'state WI WI'}>WI</MenuItem>
                    <MenuItem value={'state WY WY'}>WY</MenuItem>


                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Sponsor Party</InputLabel>
                  <Select
                    value={filterAssignment["sponsor_party"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'sponsor_party a z'}>Any</MenuItem>
                    <MenuItem value={'sponsor_party d e'}>Democrats</MenuItem>
                    <MenuItem value={'sponsor_party r s'}>Republicans </MenuItem>
                    <MenuItem value={'sponsor_party i j'}>Independents</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>
          </div>
        </div>
        <br></br>
        <table class="table table-bordered table-hover table-striped table-responsive table-light">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Article Title</th>
              <th scope="col">Introduction</th>
              <th scope="col">Latest Action</th>
              <th scope="col">Last Voted</th>
              <th scope="col">Chamber</th>
              <th scope="col">Supportive State</th>
              <th scope="col">Sponsor</th>
              <th scope="col">Sponsor Party</th>
              <th scope="col">Learn More</th>
            </tr>
          </thead>
          <tbody>
            {renderCards}
          </tbody>
        </table>
        <h3 class="text-center text-white"> Found {cardSource.length} sources </h3>
          <Pagination
            activePage={currentPage}
            itemsCountPerPage={numCardsPerPage}
            totalItemsCount={cardSource.length}
            onChange={this.handleClick}
            itemClass="page-item"
            linkClass="page-link"
            innerClass="pagination justify-content-center"
          />
        
      </div>
      </div>

        )
    }
}

export default Environmental_Legislation
