import React, {Component} from 'react'
import {
  BrowserRouter as Router,
  Route, Link,
} from 'react-router-dom'
import Highlighter from "react-highlight-words";
import '../Components/CardContainer.css'
import '../Components/TableEdit.css'

class EL_Card extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    
  }

  reformat_date(time) {
    let result = "";
    result += time.substring(5,7);
    result += ("-" + time.substring(8, 10));
    result += ("-" + time.substring(0, 4));
    return result;
  }

  render(){
    const holder = this.props.sentToELCard
    const query = this.props.sendSearchQuery
    const pathID = "/Environmental_Legislation/" + holder.id
    let articleName = holder.name
    let myChamber = ""
    let last_vote = holder.last_vote
    let sponsor_p = ""
    let sponsor_name=""
    if (holder.chamber[0] == "s") {
      myChamber = "Senate"
    } else {
      myChamber = "House of Representatives"
    }

    if (!articleName) {
      articleName = "-Article's Title is currently unavailable-"
    }

    if (!holder.last_vote) {
      last_vote = "Unknown"
    } else {
      last_vote = this.reformat_date(holder.last_vote)
    }
    if (holder.sponsor_party[0] == "R") {
      sponsor_p = "Republican"
    } else if (holder.sponsor_party[0] == "D"){
      sponsor_p = "Democrat"
    } else {
      sponsor_p = "Independent"
    }

    const buildHighLight = (val) => {
      if (query != null || query != '') {
        return (
          <td align="center">
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={val}
          />
          </td>
        )
      } else {
        return (
          <td align="center">{val}</td>
        )
      }
    }

    const highLightTitle = () => {
      if (query != null || query != '') {
        return (
          <th scope="row" align="center">
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[query]}
            autoEscape = {true}
            textToHighlight ={articleName}
          />
          </th>
        )
      } else {
        return (
          <th scope="row" align="center">{articleName}</th>
        )
      }
    }

    return(

        <tr>
          {highLightTitle()}
          <td align="center">{this.reformat_date(holder.introduced)}</td>
          <td align="center">{this.reformat_date(holder.latest_action)}</td>
          <td align="center">{last_vote}</td>
          {buildHighLight(myChamber)}
          {buildHighLight(holder.state)}
          {buildHighLight(holder.politician_id.first_name + ' ' + holder.politician_id.last_name)}
          {buildHighLight(sponsor_p)}
          <td align="center"><a href={pathID}>Article Link</a></td>
        </tr>
    )
  }
}

export default EL_Card
