import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';
import {
    Redirect
} from 'react-router-dom'
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import ecc from './ecc2.png'
import './Components/CardContainer.css'


const NavItem = props => {
    const pageURI = window.location.pathname + window.location.search
    const liClassName = (props.path === pageURI) ? "nav-item active" : "nav-item";
    const aClassName = props.disabled ? "nav-link disabled" : "nav-link"
    return (
        <li className={liClassName}>
      <a href={props.path} className={aClassName}>
        {props.name}
        {(props.path === pageURI) ? (<span className="sr-only">(current)</span>) : ''}
      </a>
    </li>
    );
}


class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searching: false,
            searchQuery: ''
        };
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a href="/">
              <img class="ecc-label" src={ecc} alt="" height="50" w="50">
              </img>
            </a>
          <a className="navbar-brand" href="/">Engage Climate Change</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <NavItem path="/States" name="States" />
              <NavItem path="/Environmental_Legislation" name="Environmental Legislation" />
              <NavItem path="/Politicians" name="Politicians" />
              <NavItem path="/About" name="About" />
              <NavItem path="/Visualizations" name="Visualizations" />
              <NavItem path="/Random" name="Random" />
            </ul>

            <TextField
            id="engageSearch"
            style={{
                color: "white"
            }}
            InputProps={{
                    style: {
                        color: "white"
                    }
                }}
            placeholder="Search"
            onChange={(e) => {this.state.searchQuery = e.target.value.trim()}}
            onKeyDown={(e) => {if (e.keyCode ===13 && this.state.searchQuery != "") {window.location.assign("/search/" + this.state.searchQuery)}}}
            />
          </div>
        </nav>
        )
    }
}

export default Navigation
