import React from 'react'
import {
    Link
} from 'react-router-dom'
import './TeamPage.css'
import '../Components/CardContainer.css'

import tonypic from './tony.jpg'
import barettpic from './barrett.jpg'
import anshpic from './ansh.jpg'
import davidpic from './david.jpg'
import benpic from './ben.jpg'
import jackpic from './jack.jpg'

class About extends React.Component {
    constructor(props) {
        super(props)

        var barrett = [0, 0, 0]
        var ansh = [0, 0, 0]
        var tony = [0, 0, 0]
        var ben = [0, 0, 0]
        var david = [0, 0, 0]

        this.state = {
            barrett: barrett,
            tony: tony,
            ansh: ansh,
            ben: ben,
            david: david,

        };
    }

    componentDidMount() {
        this.getAllCommits(1)
            .then(this.getAllClosedIssues(1))
            .then(this.getAllOpenedIssues(1))

    }

    async getAllCommits(page) {
        fetch("https://gitlab.com/api/v4/projects/12956347/repository/commits?all=true&per_page=100&page=" + page)
            .then(res => res.json())
            .then((result) => {
                    if (Object.entries(result).length !== 0) {
                        result.forEach(commit => {
                            switch (commit.committer_name.toLowerCase()) {
                                case "bschoney":
                                case "barrett schonefeld":
                                    this.state.barrett[0] += 1;
                                    break;
                                case "gonelli":
                                case "atgion":
                                    this.state.tony[0] += 1;
                                    break;
                                case "davidcdo12":
                                    this.state.david[0] += 1;
                                    break;
                                case "anshpujara":
                                case "ansh pujara":
                                    this.state.ansh[0] += 1;
                                    break;
                                case "bolea":
                                case "ben olea":
                                    this.state.ben[0] += 1;
                                    break;
                                default:
                            }
                        });
                        this.forceUpdate();

                        this.getAllCommits(page + 1);
                    }
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    }

    async getAllClosedIssues(page) {
        fetch("https://gitlab.com/api/v4/projects/12956347/issues?per_page=100&page=" + page)
            .then(res => res.json())
            .then((result) => {
                    if (Object.entries(result).length !== 0) {
                        result.forEach(issue => {
                            if (issue.closed_by !== null) {
                                switch (issue.closed_by.username.toLowerCase()) {
                                    case "bschoney":
                                        this.state.barrett[1] += 1;
                                        break;
                                    case "gonelli":
                                        this.state.tony[1] += 1;
                                        break;
                                    case "davidcdo12":
                                        this.state.david[1] += 1;
                                        break;
                                    case "anshpujara":
                                        this.state.ansh[1] += 1;
                                        break;
                                    case "bolea":
                                        this.state.ben[1] += 1;
                                        break;
                                    default:

                                }
                            }
                        });
                        this.forceUpdate();

                        this.getAllClosedIssues(page + 1);
                    }
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    }

    async getAllOpenedIssues(page) {
        fetch("https://gitlab.com/api/v4/projects/12956347/issues?per_page=100&page=" + page)
            .then(res => res.json())
            .then((result) => {
                    if (Object.entries(result).length !== 0) {
                        result.forEach(issue => {
                            if (issue.closed_by !== null) {
                                switch (issue.author.username.toLowerCase()) {
                                    case "bschoney":
                                        this.state.barrett[2] += 1;
                                        break;
                                    case "gonelli":
                                        this.state.tony[2] += 1;
                                        break;
                                    case "davidcdo12":
                                        this.state.david[2] += 1;
                                        break;
                                    case "anshpujara":
                                        this.state.ansh[2] += 1;
                                        break;
                                    case "bolea":
                                        this.state.ben[2] += 1;
                                        break;
                                    default:

                                }
                            }
                        });
                        this.forceUpdate();

                        this.getAllClosedIssues(page + 1);
                    }
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    }

    render() {
        const {
            barrett,
            tony,
            david,
            ansh,
            ben
        } = this.state;
        return (

            <div class="page-body">
<div class="container">
  <div className="page-header" align="center">
    <br/>
    <h1>Meet the Engage Team</h1>
  </div><br></br>
  <div class="row d-flex justify-content-center">
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card card-about border-0 shadow">
        <img src={barettpic} class="card-img-top-about"/>
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Barrett Schonefeld</h5>
          <div class="card-text text-black-50">Back-end, Database, AWS</div>
          <h7 class="card-title mb-0">Commits: {barrett[0]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Opened: {barrett[2]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Closed: {barrett[1]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Unit Tests: 0</h7>
          <hr></hr>
          <p>I'm super funny, very smart, and highly competent.</p>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card card-about border-0 shadow">
        <img src={davidpic} class="card-img-top-about"/>
        <div class="card-body text-center">
          <h5 class="card-title mb-0">David Do</h5>
          <div class="card-text text-black-50">Front-End, React.js</div>
          <h7 class="card-title mb-0">Commits: {david[0]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Opened: {david[2]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Closed: {david[1]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Unit Tests: 0</h7>
          <hr></hr>
          <p>I'm a senior who loves to relax and have fun.</p>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card card-about border-0 shadow">
        <img src={tonypic} class="card-img-top-about"/>
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Tony Gonelli</h5>
          <div class="card-text text-black-50">Front-End, API, React.js</div>
          <h7 class="card-title mb-0">Commits: {tony[0]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Opened: {tony[2]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Closed: {tony[1]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Unit Tests: 7</h7>
          <hr></hr>
          <p>JavaScript gave me Stockholm syndrome.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="row d-flex justify-content-center">
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card card-about border-0 shadow">
        <img src={anshpic} class="card-img-top-about"/>
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Ansh Pujara</h5>
          <div class="card-text text-black-50">Tests</div>
          <h7 class="card-title mb-0">Commits: {ansh[0]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Opened: {ansh[2]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Closed: {ansh[1]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Unit Tests: 27</h7>
          <hr></hr>
          <p>I have 99 problems and JavaScript is all of them.</p>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card card-about border-0 shadow">
        <img src={benpic} class="card-img-top-about"/>
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Benjamin Olea</h5>
          <div class="card-text text-black-50">React.js</div>
          <h7 class="card-title mb-0">Commits: {ben[0]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Opened: {ben[2]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Closed: {ben[1]}</h7>
          <br></br>
          <h7 class="card-title mb-0">Unit Tests: 0</h7>
          <hr></hr>
          <p>Code, eat, play guitar, sleep, repeat.</p>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card card-about border-0 shadow">
        <img src={jackpic} class="card-img-top-about"/>
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Jack Su</h5>
          <div class="card-text text-black-50">N/A</div>
          <h7 class="card-title mb-0">Commits: 0</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Closed: 0</h7>
          <br></br>
          <h7 class="card-title mb-0">Issues Closed: 0</h7>
          <br></br>
          <h7 class="card-title mb-0">Unit Tests: 0</h7>
          <hr></hr>
          <p>N/a</p>
        </div>
      </div>
    </div>
  </div>
  <div className="container">
    <div class="d-flex justify-content-center">
      <div className="page-header">
        <p>{barrett[0] + david[0] + ansh[0] + ben[0] + tony[0]} total commits</p>
        <p>{barrett[1] + david[1] + ansh[1] + ben[1] + tony[1]} total issues closed</p>
        <p>34 total unit tests</p>
      </div>
    </div>
  </div> 


      <div class="container ">
        <div className="page-header" align="center"><br></br><br></br><h1>Tools</h1></div><br></br>
        <div class="row">
          <div class="col">
          <a href="https://aws.amazon.com/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1200px-Amazon_Web_Services_Logo.svg.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Amazon AWS</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Provides multiple cloud computing platforms</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://www.getpostman.com/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://user-images.githubusercontent.com/7853266/44114706-9c72dd08-9fd1-11e8-8d9d-6d9d651c75ad.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Postman</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">API Development and Documentation Environment</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://reactjs.org/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">React</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">JavaScript library for user interfaces</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://getbootstrap.com/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="http://obscureproblemsandgotchas.com/wp-content/uploads/2018/06/bootstrap-stack-e1530246058846.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Bootstrap</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">CSS framework for website development</p>
              </div>
            </div>
          </a>
          </div>
        </div>
        <br></br>



        <div class="row">
          <div class="col">
          <a href="https://www.postgresql.org/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/220px-Postgresql_elephant.svg.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">PostgreSQL</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Relational database management system</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://www.sqlalchemy.org/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://quintagroup.com/cms/python/images/sqlalchemy-logo.png/@@images/eca35254-a2db-47a8-850b-2678f7f8bc09.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">SQLAlchemy</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">SQL toolkit for Python</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="http://initd.org/psycopg/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://avatars.githubusercontent.com/u/2947270?v=3" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Psycopg2</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light"> PostgreSQL adapter for the Python</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="http://flask.pocoo.org/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://www.probytes.net/wp-content/uploads/2018/10/flask-logo-png-transparent.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Flask</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">A micro web framework written in Python</p>
              </div>
            </div>
          </a>
          </div>
        </div>
        <br></br>


        <div class="row">
          <div class="col">
          <a href="https://material-ui.com/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://material-ui.com/static/images/material-ui-logo.svg"/> 
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Material UI</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">CSS Framework with custom styles and widgets</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://www.docker.com/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEX///86jLRamrzr8/jc6PA3irNRmr0yh7Hw9vnk8PU7jLTd6/EXf6s7jrVtqMUuhrDD2uaAscy81eOmytzS5e6Pu9L3+/yxzt4df6xNlrpiocF6rMgQe6rO4uxSlbmDtc6dwdWrydpboMGixNmLudAAdqe+2eei33KOAAAIaUlEQVR4nO2d6XajOBBG2YTYi32zCTjtyfs/4gCO04klG7EFmKn7o08fHRv4XIWqtJQiSQiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIMuQBPUDQa1u/VCLUuTAoG39UIuiU0v+CXHtrR9qUXQqo8KDgwqPDyo8Pqjw+KDC44MKjw8qPD6o8PgUQB4A+b81xvdDFmPrh/rfkxos6dNmTuvuTdiUHkMlpZzmMpakgP2wZ859hHQJHc85OfQR4ktpAExz7nc9zWMryHP70uS6rsQAHvt/2eoUMmFBBn+VaJE5jj7bDV4hrJCso1AriAzv8UJqeGxsw7S/P+grpg0bK8xut6eOv5Qgho29tPi8INVXS422tWGck/vV9bXWsLZVeP17dyhWyh029VJDIX+vBCvFxU1tWFnfFBKaLSfrG5sqjH5cjqwzENvSSx/vAt6Cwr7Y0oaq8nC5/GNBZXe2VGi65OFiygoZ6pZeGsuPCtfobLa0oU+Ye5TLB8UtFYbsvWH5/HRLhRmwRly+O/X+cR7InVAyvDPT/E/b0blsszM9iHEUErK4m6omS/q0mdNqTs+1OArlvFpQ3OaErEIrb7Z+qiVh+9L2RTxNvlxT6I8UbfS5ss16JaUJ59Ot/3icT9uS6bGt7wKvZ+yyCum3eJH2CCu85sz8IESSdGKbaduXntjZRIc/m0hsyWabqSUwucTkNF1Xo3R9sxr7WZMEp5ZrcwkroVQngcdthTKNumjBNL+MFry9iabC9omugEL1nb0L0U0jq8tCpn/3dxJXCbJhWyZM4CM3hcxNRsfDqQrTmr2L7L63mh7bCeQQ+AMi5yp8kZdOVShdOAplyul/+vtD8PqaO7ThzzH+MOAGR1NosF98LXFVhWt4KeehXkGKlznwHm0o2ecRRiTQjPy5dqCQc8HnDA0ed+mlbeImbEQyVPewTxu2yZOwwGbgWvtUKNlvgkYcFLhTL239lDNIZCGQDKZtO7VhOyQQUEic+pB5aU+acUYYj7cuQoEr7dRL42DQSwmUQnNB7OhpDza8FkMBsc24G7HF0yRnv8tVSGT/cwPBz+ZuBKxzPt0p5AgfVphqNQwYkFCqDHcxd4UO83XCt+EThcBX2NlQn2BDNazJgL5Wf5mJr2SoGktrfZPTbDxt5rRqaWsMbvMr/OSNMxDs93N2/7YD/PzsvP/Rdr9rjiU1zCoqHYdnPqIoSuG6ulJ6QZK9Gkeo+0SL/fBy9XTCTFLcPbsyTS2ObVMdMJ36puySopD7jYFPX7uTaJ9S5Y9blffC43rhgw1FwvpNIdNrHgKqCO8Z+jimQkhEBUqZw0zyHgBCxSPDMRVCLSxQivIDKiRkxC7aZtQU3U4YtautYRdb9o87Zu33iDYEb8xi+RHfQxi1l/2AfenIte3scDYk40wo+YfLaUakMz0xM1+xc0ZMPd4wj2ZDpxknUJIOFi1oMXpblfh6zi5whMeFX4xZlNueccH+xvVIbkqKCYVr2YEUWk40XqAUD6917AbqThDI7vLfL4RM2k9qnA6jkDbTNuD+OYpCKCfW5FUHeRGJM7XocOxuqq2AKf3ojfoQ8UJ8Gp+lOsLwYlaBTDojNSUA/MWvhSEwq0ZaeKsRe2PHC7WqhrX9nMC8Ki6mLEwUWnxIdqhJJmcP9qLQ6b1Mj+FNtIEVpoGcX1NJW7c/zoO5dc28ihsB4CQlOXG6Wk7uJuylAH124TZnj4sIeWa7XVlHfAm1FdMGKBcoFm0mBYy86gcm1HUSU3TX4HhAWeKkD3WSm+ahfSvVySO7WEshnRcnvmimSIRaCvJuuRKMcC2BUCxUKKqWU7oK1zdLAHDDaV8XgBaLlRhOmsygupaGSWRK3vNtIbOgsFwNZTopJlKITNX0i5VymsVctCee1NmQ3JHzfCUXXfpIqHpUxLj/HFb7v5VWr2DpcySNUszXCKGUFpyKzoUBb/G9hvFATLN6eW3XWdaZHa08YiL5dYXNlBHfT7vhI+kHgjlAkYRxdyjiiklMf09ndrLN5TafYVnfik5bba779qaUp2sUVt/u+rHmzABdtBP9htGPhd0kzL7wfb+KbdNgf9EVl3RAWe24MrPbm03EUl2x+pUJEOe04tmIqku7TeFCPrLSVpw2CVxPX0vV1TRQV2RahFNXMB8C3ponP/YSrU6i3Ih89Ly4QHAmrk2MwXahn0QTmEOPFvZTSlc3YE+l9IdolsNpPaeWZA7gXn6pZELrxwrUHT4uTBXM9ESg+en3Tvtuh1KkzysGw0bKlv5M1EfKX3HQO0bTz/S347Oh995eZDKYwLvAaR3Lkjm9p8qD+a8934okJ8kGNUvqbWYCnGjg5unMsEjpW7NNTVYadWGjK8occCC1npG/EVCiVc+vfolWdufaksEolSbniWaE3I22/aOPlUK7Z4eznL18kDaEjjcjodbpV/tPLmqkA7G6STU9eVUH2XrqKDNabXqv/4l/u//koibQn8FMwD29WMVLM3fEkBjys/ex3evHkL2TW7+aO3qkcQbDNyJXaF6YAHGDNU5XnYMaBt2BU1ZvyfIaPvGu+Dqw3G11h3K5dbYP7/yJoSXFuZ/5bbtW4iqnJrTZIJaap+c9DqGtDxTXasbZiWtjN57VyusCSNtP5A64RVk3F7+qvlVlR7wpSUKAkvbD/u6rrQ0/Ctxudwm5PXd/jl53CNydn0XzlvVpOdCDJowP8teOU1O7vLvdU9NPnfJnaXxfu3uf7Sd9mbxzBrnwmthWR5xouA9MP6o9pWht1p1593UQ4t+T/Qql9Oomqzgv63FIzdgPs0uTXE/BnTpJmkuWhX7Vmm3rB1yM9CdbPw6CIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIMh/l38BKOPJ5pI2YEMAAAAASUVORK5CYII=" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Docker</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Open-source platform for OS virtualization</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://flask-restless.readthedocs.io/en/stable/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://www.probytes.net/wp-content/uploads/2018/10/flask-logo-png-transparent.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Flask Restless</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Database Model for RESTFul APIs</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://www.seleniumhq.org/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://www.seleniumhq.org/images/big-logo.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Selenium</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">A micro web framework written in Python</p>
              </div>
            </div>
          </a>
          </div>
        </div>



        <br></br><br></br>
        <div className="page-header" align="center"><h1>Data Sources</h1></div><br></br>
        <div class="row">
        <div class="col">
          <a href="http://api.eia.gov/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://pbs.twimg.com/profile_images/489460437214171136/x9Ca1CfZ_400x400.jpeg" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">EIA</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Used to pull emission data of each States</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://datausa.io/about/api/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://upload.wikimedia.org/wikipedia/commons/4/4b/Data_USA_Logo_%28blue%29.png"/>
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">Data USA</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Used to pull data of States and Politicians</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://api.propublica.org/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://pbs.twimg.com/media/DBGEjVkVoAEtGlj.png" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">ProPublica</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Used to pull data of Enviornmental Legislations</p>
              </div>
            </div>
          </a>
          </div>
          <div class="col">
          <a href="https://docs.gitlab.com/ce/api/">
            <div class="card card-tool">
              <div class="align-items-start d-flex justify-content-center">
                <img class="card-img-top-tools" src="https://www.vectorlogo.zone/logos/gitlab/gitlab-tile.svg" />
              </div>
              <div class="card-body align-items-center d-flex justify-content-center">
                <p class="font-weight-bold">GitLab</p>
              </div>
              <div class="align-items-end d-flex justify-content-center">
                <p class="small font-italic font-weight-light">Used to pull commits and issues from each member</p>
              </div>
            </div>
          </a>
          </div>
        </div>
        <br></br><br></br><br></br>
        <div className="page-header" align="center">
        <br/>
          <h1>More Project Info</h1>
          <p>
            <a href="https://gitlab.com/bschoney/engage-climate-change">Team's GitLab Repository</a><br></br>
            <a href="https://documenter.getpostman.com/view/7911561/S1a61kyB?version=latest">Team's Postman Documentation</a>
          </p>
        </div>
      </div>
  </div>
</div>
        );
    }
}

export default About
