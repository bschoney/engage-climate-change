import React from 'react'
import {
    Link
} from 'react-router-dom'
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'
import {
    Timeline
} from 'react-twitter-widgets'
import {
    FacebookProvider,
    Page
} from 'react-facebook';

import {
    withStyles
} from "@material-ui/core/styles";
import '../Components/CardContainer.css'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

class PoliticiansInstances extends React.Component {
    constructor(props) {
        super(props)
        let curID = this.props.match.params.id
        var randomBill

        this.state = {
            isLoaded: false,
            myTextField: null,
            myTable: null,
            myLinks: null,
            myBody: null,
            politicians: [],
            secondCall: [],
            wikiImage: "",
        };
        this.buildBody = this.buildBody.bind(this)
    }
    
    buildBody() {
        const finder = this.props.match.params.id
        var id = this.props.match.params.id.split("_")[2]
        var holder = this.state.politicians
        var yUser = holder.youtube
        var twitter = holder.twitter
        var fb = holder.facebook
        var wholeName = holder.first_name + " " + holder.last_name
        var bill = ""

        const youtube = (yUser) => {
            if (yUser != null) {
                return (
                    <iframe align="right" src={"https://www.youtube.com/embed/?listType=user_uploads&list=" + yUser} width="400" height="300"></iframe>
                )

            } else {
                return (
                    <div class="col-xl-3 col-md-6 mb-4">
                      <div class="card-link card-white border-0 shadow">
                        <div class="card-body text-center">
                          <h5 class="card-title mb-0">No Youtube data to present</h5>
                          <hr></hr>
                        </div>
                      </div>
                    </div>
                )
            }
        }

        const facebookwidget = (facebook) => {
            if (facebook != null) {
                return (
                    <FacebookProvider appId="908709349494568">
           <Page href={"https://www.facebook.com/" + fb}  tabs="timeline" />
         </FacebookProvider>
                )

            } else {
                return (
                    <div class="col-xl-3 col-md-6 mb-4">
                      <div class="card-link card-white border-0 shadow">
                        <div class="card-body text-center">
                          <h5 class="card-title mb-0">No Facebook data to present</h5>
                          <hr></hr>
                        </div>
                      </div>
                    </div>
                )
            }
        }

        const party = (holder) => {
            if (holder.party == "R") {
                return "Republican"
            } else {
                return "Democrat"
            }
        }

        const isOffice = (holder) => {
            if (holder.in_office) {
                return "Yes"
            } else {
                return "No"
            }
        }

        const date = (time) => {
            var tokenized = time.split(" ", 4)
            var result = tokenized[0]
            for (var i = 1; i < 4; i++) {
                result = result.concat(" " + tokenized[i])
            }
            return result
        }

        

   var result = (
   <div>
    <div class="background-POL-Instances page-body background-fixed">
      <div class="padding-Models">
        <div class="container">
          <div className="page-header" align="center"><br></br>
            <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
              <CardContent>
                <h1 class="text-white">{wholeName}</h1>
              </CardContent>
            </Card>
            <br></br><br></br>
            <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
              <CardContent>
                <table class="table table-bordered table-hover table-striped table-dark">
                  <thead class="thead-dark"><tr>
                    <th title="Field #1" width="10%">Attribute</th>
                    <th title="Field #2" width="10%">Data</th>
                  </tr></thead>
                  <tbody>
                    <tr>
                      <td align="left"><b>Party</b></td>
                      <td align="right">{party(holder)}</td>
                    </tr>
                    <tr>
                      <td align="left"><b>State</b></td>
                      <td align="right">{holder.state_id.statename}</td>
                    </tr>
                    <tr>
                      <td align="left"><b>In-Office</b></td>
                      <td align="right">{isOffice(holder)}</td>
                    </tr>
                    <tr>
                      <td align="left"><b>Date of Birth</b></td>
                      <td align="right">{holder.dob}</td>
                    </tr>
                    <tr>
                      <td align="left"><b>Most Recent Vote</b></td>
                      <td align="right">{holder.recent_vote}</td>
                    </tr>
                  </tbody>
                </table>
              </CardContent>
            </Card>
            <br></br><br></br>
            <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
              <CardContent>
                <div className="page-header" align="center">
                  <h3 class="text-white"><b>Politician's YouTube Channel</b></h3>
                </div><hr></hr>
                <div className = "row d-flex justify-content-center">
                  {youtube(yUser)}
                </div>
              </CardContent>
            </Card>
            <br></br><br></br>
            <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
              <CardContent>
                <div className="page-header" align="center">
                  <h3 class="text-white"><b>Politician's Facebook Page and Twitter</b></h3>
                </div><hr></hr>
                <div class="row">
                  <div class="col">
                    {facebookwidget(fb)}
                  </div>
                  <div class="col">
                    <Timeline dataSource={{sourceType:"profile", screenName:twitter}}
                      options={{username:twitter, height:"400", width:"300"}}/>
                  </div>
                </div>
              </CardContent>
            </Card>
            <br></br><br></br>
            <Card style={{background: 'rgba(51, 51, 51, .7)'}}>
              <CardContent>
                <div className="page-header" align="center">
                  <h3 class="text-white"><b>Politician's Related Links</b></h3>
                </div><hr></hr>
                <div class="row d-flex justify-content-center">
                  <Link to={"/States/" + holder.state}>
                    <div class="col-xl-3 col-md-6 mb-4">
                      <div class="card-link card-white border-0 shadow">
                        <div class="card-body text-center">
                          <h5 class="card-title mb-0">Related:</h5>
                          <h7 class="card-title mb-0">{holder.state_id.statename}</h7>
                          <hr></hr>
                          <p>State represented by this politician</p>
                        </div>
                      </div>
                    </div>
                  </Link>

                  <Link to={"/Environmental_Legislation/" + holder.bills[0].id}>
                    <div class="col-xl-3 col-md-6 mb-4">
                      <div class="card-link card-white border-0 shadow">
                      <div class="card-body text-center">
                        <h5 class="card-title mb-0">Related:</h5>
                        <h7 class="card-title mb-0">{holder.bills[0].name}</h7>
                        <hr></hr>
                        <p>Legislation sponsored by this politician</p>
                      </div>
                    </div>
                  </div>
                  </Link>
                </div>
              </CardContent>
            </Card>
            <br></br><br></br>
          </div>
        </div>
      </div>
    </div>  
     </div>
   );
   this.setState({myBody:result})
 }

    componentDidMount() {
        this.fetchData()
            .then(this.setState({
                isLoaded: true
            }))
            .then(this.buildBody)
    }

    async fetchData() {
        let dbID = this.props.match.params.id
        const api = "https://api.engageclimatechange.world/politicians/" + dbID
        await fetch(api)
            .then(result => result.json())
            .then(result => {
                this.setState({
                    politicians: result,
                })
            })
            .catch(function(error) {
                console.log('Looks like there was a problem: \n', error);
            });

    }

    render() {
        const {
            isLoaded,
            myBody
        } = this.state
        if (!isLoaded) {
            return (<div> <p>Loading ...</p> </div>);
        } else {
            return (
                <div class="page-body background-fixed">
          {myBody}
        </div>
            );
        }
    }
}

export default PoliticiansInstances
