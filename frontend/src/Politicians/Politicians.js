import React from 'react'
import {
    Link
} from 'react-router-dom'
import PoliticianCard from './PoliticianCard'
import '../Components/CardContainer.css'
import Pagination from "react-js-pagination";

import Input from "@material-ui/core/Input";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const POLURL = "https://api.engageclimatechange.world/politicians"

class Politicians extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cardSource: [],
            isLoaded: false,
            numCardsPerPage: 10,
            currentPage: 1,
            jsonData: null,
            politicians: [],
            searchQueryValue: null,
            searchQueryMap: '{"or":[]}',
            sortAssignment: [[''], ['']],
            sortQueryValue: ['asc', 'first_name'],
            sortQueryMap: '"order_by":[]',
            filterAssignment: {
                'party': '',
                'state': '',
                'recent_vote': '',
                'dob': '',
                'in_office': '',
            },
            filterQueryValue: {
                'party': [null],
                'state': [null],
                'recent_vote': [null, null],
                'dob': [null],
                'in_office': [null],
            },
            filterQueryMap: '"filters":[{"and":[]}',
        };
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.fetchData().then(this.setState({
            isLoaded: true
        }));
    }

    async fetchData() {
        fetch(this.formatURL(), {
                crossDomain: true,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .then(result => {
                this.setState({
                    politicians: result,
                })
            })
            .catch(function(error) {
                console.log('Looks like there was a problem: \n', error);
            });
    }

    // Handles specific onClick conditions for Pagination
    async handleClick(event) {
        await this.setState({
            currentPage: event
        })
    }

    handleSortQuery(value) {
        let valueArray = value.split(" ")
        const {
            sortQueryValue
        } = this.state;
        let sortDirection = sortQueryValue[0];
        let sortField = sortQueryValue[1];

        let newSortAssignment = this.state.sortAssignment.slice()
        if (valueArray[0] == 0) {
            sortDirection = valueArray[1]
            newSortAssignment[0] = value
        } else {
            sortField = valueArray[1]
            newSortAssignment[1] = value
        }

        let sortQuery = '"order_by":[{"field":"' + sortField + '","direction":"' + sortDirection + '"}]'
        this.setState({
            sortQueryValue: [sortDirection, sortField],
            sortQueryMap: sortQuery,
            sortAssignment: newSortAssignment
        }, () => this.fetchData());
    }

    handleFilterQuery(value) {
        let valueArray = value.split(" ")
        const {
            filterQueryValue,
            filterAssignment
        } = this.state;
        let filterQuery = ''
        let filterArray = []
        console.log(valueArray)

        /* Updates filterQueryValue with new values */
        for (var keys in filterQueryValue) {
            if (keys == valueArray[0] && valueArray.length == 3) {
                console.log("INSIDE ARRAY LENGTH = 2")
                filterQueryValue[keys][0] = valueArray[1]
                filterQueryValue[keys][1] = valueArray[2]
                filterAssignment[keys] = value
            } else if (keys == valueArray[0] && valueArray.length == 2) {
                filterQueryValue[keys][0] = valueArray[1]
                filterAssignment[keys] = value
            }
        }

        /* Makes filterQuery*/
        for (var keys in filterQueryValue) {
            if (filterQueryValue[keys][0] != null && filterQueryValue[keys][0] != 'any') {
                if (filterQueryValue[keys].length == 1) {
                  filterArray.push('{"name":"' + keys + '","op":"eq","val":"' + filterQueryValue[keys][0] + '"}')
                } else if (filterQueryValue[keys].length == 2) {
                  console.log("INSIDE ARRAY LENGTH = 2")
                  filterArray.push('{"name":"' + keys + '","op":"ge","val":"' + filterQueryValue[keys][0] + '"},{"name":"' + keys + '","op":"le","val":"' + filterQueryValue[keys][1] + '"}')
                }
            }
        }

        /* Adds appropriate commas to queries */
        for (var i = 0; i < filterArray.length; i++) {
            filterQuery += filterArray[i];
            if (i !== filterArray.length - 1) {
                filterQuery += ','
            }
        }

        let result = '"filters":[{"and":[' + filterQuery + ']}'
        this.setState({
            filterQueryMap: result
        }, () => this.fetchData());
    }

    handleSearchQuery(value) {
        let mySearchQuery = ''
        let searchArray = []

        if (value != null && value != '') {
            // Sponsor party
            if (value.toLowerCase().includes("democrat")) {
                searchArray.push('{"name": "party", "op": "ilike", "val": "D"}')
            }
            if (value.toLowerCase().includes("republican")) {
                searchArray.push('{"name": "party", "op": "ilike", "val": "R"}')
            }
            if (value.toLowerCase().includes("independent")) {
                searchArray.push('{"name": "party", "op": "ilike", "val": "i"}')
                searchArray.push('{"name": "party", "op": "ilike", "val": "id"}')
            }

            // State
            searchArray.push('{"name": "state", "op": "ilike", "val": "%25' + value + '%25"}')

            // Pol Name
            searchArray.push('{"name": "first_name", "op": "ilike", "val": "%25' + value + '%25"}')
            searchArray.push('{"name": "last_name", "op": "ilike", "val": "%25' + value + '%25"}')
        }

        /* Adds the commas to the query at appropriate positions */
        for (var i = 0; i < searchArray.length; i++) {
            mySearchQuery += searchArray[i];
            if (i !== searchArray.length - 1) {
                mySearchQuery += ','
            }
        }

        let myValue = value
        if (myValue == '') {
            myValue = null
        }

        let myQuery = '{"or":[' + mySearchQuery + ']}'
        this.setState({
            searchQueryValue: myValue,
            searchQueryMap: myQuery
        }, () => this.fetchData());
    }

    createCards() {
        let source = this.state.politicians.objects
        var cards = [];
        var i = 0;
        for (var key in source) {

            var props = {
                myIndex: i,
                sentToPoliticianCard: source[key]
            }
            cards.push(<PoliticianCard {...props} sendSearchQuery={this.state.searchQueryValue}/>);
            i++
        }
        this.state.cardSource = cards;
    }

    formatURL() {
        const {
            sortQueryMap,
            filterQueryMap,
            searchQueryMap
        } = this.state;
        let filterQuery = ''
        let filterArray = []

        console.log(POLURL + '?q={' + sortQueryMap + ',' + filterQueryMap + ',' + searchQueryMap + ']}')
        return POLURL + '?q={' + sortQueryMap + ',' + filterQueryMap + ',' + searchQueryMap + ']}'
    }

    render() {
        this.createCards()
        const {
            cardSource,
            isLoaded,
            numCardsPerPage,
            currentPage,
            currentNumCards,
            searchQueryValue,
            sortAssignment,
            filterAssignment,
        } = this.state;
        console.log(cardSource)
        // Pagination
        const lastPageNum = currentPage * numCardsPerPage;
        const firstPageNum = lastPageNum - numCardsPerPage;
        const currentCards = cardSource.slice(firstPageNum, lastPageNum);
        const renderCards = currentCards.map((cardSource, index) => {
            return <div>{cardSource}</div>
        });

        function createDates(name_id, beg, last) {
            var result = [];
            for (var year = beg; year <= last; year++) {
                result.push(<MenuItem value={'' + name_id + ' ' + year + '-01-01' + ' ' + year + '-12-31'}>{year}</MenuItem>);
            }
            return result
        }



        if (!isLoaded) {
            return (<div> <p>Loading ...</p> </div>);
        } else {
            return (
                <div class="background-Pol page-body background-fixed">
        <div class="padding-Models">
          <div className="container">
            <div className="page-header">
              <h1 class="text-white center-title">Politicians</h1>
              <br></br>
            </div>
          </div>

          <div class="container-white-states">
          <div class="container">
            <div class="row">
              <div class="col">
              <TextField
                style={{width: "100%"}}
                id="politicianSearch"
                placeholder="Search ..."
                onChange={event => this.handleSearchQuery(event.target.value)}
              />
              </div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Sort Direction</InputLabel>
                  <Select
                    value={sortAssignment[0]}
                    onChange={event => this.handleSortQuery(event.target.value)}
                  >
                    <MenuItem value={'0 asc'}>Any</MenuItem>
                    <MenuItem value={'0 asc'}>Ascending</MenuItem>
                    <MenuItem value={'0 desc'}>Descending</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Sort By Attribute Name</InputLabel>
                  <Select
                    value={sortAssignment[1]}
                    onChange={event => this.handleSortQuery(event.target.value)}
                  >
                    <MenuItem value={'1 first_name'}>Any</MenuItem>
                    <MenuItem value={'1 first_name'}>Name</MenuItem>
                    <MenuItem value={'1 party'}>Party</MenuItem>
                    <MenuItem value={'1 state'}>State</MenuItem>
                    <MenuItem value={'1 in_office'}>In Office</MenuItem>
                    <MenuItem value={'1 recent_vote'}>Most Recent Vote</MenuItem>
                    <MenuItem value={'1 dob'}>Date of Birth</MenuItem>
                  </Select>
                </FormControl>
              </div>
          <div class="container">
            <div class="row">
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Party</InputLabel>
                  <Select
                    value={filterAssignment["party"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                  <MenuItem value={'party any'}>Any</MenuItem>
                  <MenuItem value={'party D'}>Democrats</MenuItem>
                  <MenuItem value={'party R'}>Republicans </MenuItem>
                  <MenuItem value={'party I'}>Independents</MenuItem>
                  <MenuItem value={'party '}>Unknown Party</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>State</InputLabel>
                  <Select
                    value={filterAssignment["state"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                  <MenuItem value={'state AA ZZ'}>Any</MenuItem>
                    <MenuItem value={'state AL AL'}>AL</MenuItem>
                    <MenuItem value={'state AK AK'}>AK</MenuItem>
                    <MenuItem value={'state AZ AZ'}>AZ</MenuItem>
                    <MenuItem value={'state AR AR'}>AR</MenuItem>
                    <MenuItem value={'state CA CA'}>CA</MenuItem>
                    <MenuItem value={'state CO CO'}>CO</MenuItem>
                    <MenuItem value={'state CT CT'}>CT</MenuItem>
                    <MenuItem value={'state DE DE'}>DE</MenuItem>
                    <MenuItem value={'state FL FL'}>FL</MenuItem>
                    <MenuItem value={'state GA GA'}>GA</MenuItem>
                    <MenuItem value={'state HI HI'}>HI</MenuItem>
                    <MenuItem value={'state ID ID'}>ID</MenuItem>
                    <MenuItem value={'state IL IL'}>IL</MenuItem>
                    <MenuItem value={'state IN IN'}>IN</MenuItem>
                    <MenuItem value={'state IA IA'}>IA</MenuItem>
                    <MenuItem value={'state KS KS'}>KS</MenuItem>
                    <MenuItem value={'state KY KY'}>KY</MenuItem>
                    <MenuItem value={'state LA LA'}>LA</MenuItem>
                    <MenuItem value={'state MD MD'}>MD</MenuItem>
                    <MenuItem value={'state ME ME'}>ME</MenuItem>
                    <MenuItem value={'state MA MA'}>MA</MenuItem>
                    <MenuItem value={'state MI MI'}>MI</MenuItem>
                    <MenuItem value={'state MN MN'}>MN</MenuItem>
                    <MenuItem value={'state MS MS'}>MS</MenuItem>
                    <MenuItem value={'state MO MO'}>MO</MenuItem>
                    <MenuItem value={'state MT MT'}>MT</MenuItem>
                    <MenuItem value={'state NE NE'}>NE</MenuItem>
                    <MenuItem value={'state NV NV'}>NV</MenuItem>
                    <MenuItem value={'state NH NH'}>NH</MenuItem>
                    <MenuItem value={'state NJ NJ'}>NJ</MenuItem>
                    <MenuItem value={'state NM NM'}>NM</MenuItem>
                    <MenuItem value={'state NY NY'}>NY</MenuItem>
                    <MenuItem value={'state NC NC'}>NC</MenuItem>
                    <MenuItem value={'state ND ND'}>ND</MenuItem>
                    <MenuItem value={'state OH OH'}>OH</MenuItem>
                    <MenuItem value={'state OK OK'}>OK</MenuItem>
                    <MenuItem value={'state OR OR'}>OR</MenuItem>
                    <MenuItem value={'state PA PA'}>PA</MenuItem>
                    <MenuItem value={'state RI RI'}>RI</MenuItem>
                    <MenuItem value={'state SC SC'}>SC</MenuItem>
                    <MenuItem value={'state SD SD'}>SD</MenuItem>
                    <MenuItem value={'state TN TN'}>TN</MenuItem>
                    <MenuItem value={'state TX TX'}>TX</MenuItem>
                    <MenuItem value={'state UT UT'}>UT</MenuItem>
                    <MenuItem value={'state VT VT'}>VT</MenuItem>
                    <MenuItem value={'state VA VA'}>VA</MenuItem>
                    <MenuItem value={'state WA WA'}>WA</MenuItem>
                    <MenuItem value={'state WV WV'}>WV</MenuItem>
                    <MenuItem value={'state WI WI'}>WI</MenuItem>
                    <MenuItem value={'state WY WY'}>WY</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Most Recent Vote</InputLabel>
                  <Select
                    value={filterAssignment["recent_vote"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                  <MenuItem value={'recent_vote any any'}>Any</MenuItem>
                  {createDates('recent_vote', 2001, 2019)}
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>Date of Birth</InputLabel>
                  <Select
                    value={filterAssignment["dob"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                  <MenuItem value={'dob any any'}>Any</MenuItem>
                  {createDates('dob', 1900, 2019)}
                  </Select>
                </FormControl>
              </div>
              <div class="col">
                <FormControl style={{width: "100%"}}>
                  <InputLabel>In Office</InputLabel>
                  <Select
                    value={filterAssignment["in_office"]}
                    onChange={event => this.handleFilterQuery(event.target.value)}
                  >
                    <MenuItem value={'in_office any'}>Any</MenuItem>
                    <MenuItem value={'in_office true'}>Yes</MenuItem>
                    <MenuItem value={'in_office false'}>No</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>
          </div>
          </div>
        </div>
          </div>

          <div className="container">
            <div className="card-container">
              {renderCards}
              <br/>
            </div>
          </div>
          <h3 class="text-white text-center"> Found {cardSource.length} sources </h3>
          <Pagination
                activePage={currentPage}
                itemsCountPerPage={numCardsPerPage}
                totalItemsCount={cardSource.length}
                onChange={this.handleClick}
                itemClass="page-item"
                linkClass="page-link"
                innerClass="pagination justify-content-center"
          />
        </div>
        </div>
            );
        };
    }
}

export default Politicians
