import React, {
    Component
} from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom'
import Highlighter from "react-highlight-words";

class PoliticianCard extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        //this.createCards()
    }

    render() {
        const searchQuery = this.props.sendSearchQuery
        const holder = this.props.sentToPoliticianCard
        const pathName = "/Politicians/" + holder.id
        const fullName = holder.first_name + " " + holder.last_name;
        const imgPath = "https://www.50states.com/images/redesign/flags/" + holder.state.toLowerCase() + "-largeflag.png"
        const party = (holder) => {
            if (holder.party == "R") {
                return "Republican"
            } else if (holder.party == "D") {
                return "Democrat"
            } else if (holder.party[0] == "I") {
                return "Independent"
            } else {
                return "Unknown Party"
            }
        }

        const isOffice = (holder) => {
            if (holder.in_office) {
                return "Yes"
            } else {
                return "No"
            }
        }

        const date = (time) => {
            var tokenized = time.split(" ", 4)
            var result = tokenized[0]
            for (var i = 1; i < 4; i++) {
                result = result.concat(" " + tokenized[i])
            }
            return result
        }

        const buildHighLight = (val) => {
            if (searchQuery != null || searchQuery != '') {
                return (
                    <td align="center">
          <Highlighter
            highlightClassName = "YourHighlightClass"
            searchWords = {[searchQuery]}
            autoEscape = {true}
            textToHighlight ={val}
          />
          </td>
                )
            } else {
                return (
                    <td align="center">{val}</td>
                )
            }
        }

        const highLightTitle = () => {
            if (searchQuery != null || searchQuery != '') {
                return (
                    <h5 class="card-title">
            <Highlighter
              highlightClassName = "YourHighlightClass"
              searchWords = {[searchQuery]}
              autoEscape = {true}
              textToHighlight ={fullName}
            />
          </h5>
                )
            } else {
                return (
                    <h5 class="card-title">{fullName}</h5>
                )
            }
        }

        const highLightParty = () => {
            if (searchQuery != null || searchQuery != '') {
                return (
                    <h6 class="card-subtitle mb-2 text-muted">
            <b>Party:</b> <Highlighter
              highlightClassName = "YourHighlightClass"
              searchWords = {[searchQuery]}
              autoEscape = {true}
              textToHighlight ={party(holder)}
            />
          </h6>
                )
            } else {
                return (
                    <h6 class="card-subtitle mb-2 text-muted">{party(holder)}</h6>
                )
            }
        }

        const highLightState = () => {
            if (searchQuery != null || searchQuery != '') {
                return (
                    <h6 class="card-subtitle mb-2 text-muted">
              <b>State:</b> <Highlighter
              highlightClassName = "YourHighlightClass"
              searchWords = {[searchQuery]}
              autoEscape = {true}
              textToHighlight ={holder.state}
            />
          </h6>
                )
            } else {
                return (
                    <h6 class="card-subtitle mb-2 text-muted">State: {holder.state}</h6>
                )
            }
        }

        return (
            <div class="card-politician">
      <a href = {pathName} class = "card-link">
      <img class="card-img-top-instances" src={imgPath} alt="Card image cap"/>
        <div class="card-body">
          {highLightTitle()}
          {highLightParty()}
          {highLightState()}
          <h6 class="card-subtitle mb-2 text-muted"><b>In office:</b> {isOffice(holder)}</h6>
          <h6 class="card-subtitle mb-2 text-muted"><b>Most recent vote:</b> {holder.recent_vote}</h6>
          <h6 class="card-subtitle mb-2 text-muted"><b>Date of birth:</b> {holder.dob}</h6>

        </div>
        </a>
      </div>
        )
    }
}

export default PoliticianCard
