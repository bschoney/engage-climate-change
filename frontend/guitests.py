import unittest
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC# available since 2.26.0
from selenium.webdriver.common.keys import Keys
import time

#NOTE: Must install & add path for geckodriver to run this. Dependency of selenium.

class main(unittest.TestCase):
    options = Options()
    options.headless = True

    def setUp(inst):# create a new Firefox session 
        inst.driver = webdriver.Firefox()
        inst.driver.get("http://localhost:3000/")

    def test_open(self):
        self.assertEqual("Engage Climate Change", self.driver.title)

    def test_politicians(self):
        l = self.driver.find_element_by_link_text("Politicians")
        l.click()

    def test_politicians_header(self):
        l = self.driver.find_element_by_link_text("Politicians")
        l.click()
        b = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(b.text, "Politicians")

    def test_states(self):
        l = self.driver.find_element_by_link_text("States")
        l.click()

    def test_environmentallegislation(self):
        l = self.driver.find_element_by_link_text("Environmental Legislation")
        l.click()

    def test_about(self):
        l = self.driver.find_element_by_link_text("About")
        l.click()

    def test_search(self):
        l = self.driver.find_element_by_id("engageSearch")
        l.send_keys('Alabama')
        time.sleep(2)
        l.send_keys(Keys.ENTER)
        time.sleep(2)

    def test_d(self):
        l = self.driver.find_element_by_tag_name("h1")
        self.assertEqual(l.text, "Welcome to Engage Climate Change!")

    def tearDown(inst):
        inst.driver.quit()

if __name__ == '__main__':
    unittest.main()
