import shlex
import subprocess
import json

api_key = "a72c9aff197a2de64229a555dabb8ff1"
states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
# http://api.eia.gov/geoset/?geoset_id=sssssss&regions=region1,region2,region3,...&api_key=YOUR_API_KEY_HERE[&start=|&num=][&end=][&out=xml|json]
def formatJson(unformatted):
	return json.dumps(unformatted, indent=4, sort_keys=True)

def getData(state):
	cmd = '''curl http://api.eia.gov/geoset/?geoset_id='''
	cmd += "SEDS.FFTCB.A" + "&regions="
	cmd += "USA-" + state
	cmd += "&api_key=" + api_key #+ [&start=|&num=][&end=][&out=xml|json]"
	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	unformatted = json.loads(stdout)
	return unformatted

def getRenewableData(state) :
	cmd = '''curl https://api.eia.gov/series/?api_key=''' + api_key + '''&series_id=ELEC.GEN.AOR-''' + state + '''-99.M'''
	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	unformatted = json.loads(stdout)
	return unformatted

# for state in states:
# 	print(formatJson(getRenewableData(state)))

# fEmissions = open("emmisions.json", "r").read()
# fRenewable = open("renewable.json", "r").read()
# emmissionsDict = json.loads(fEmissions)
# renewableDict = json.loads(fRenewable)

# for stateKey in emmissionsDict.keys():
# 	emmissionsDict[stateKey]["renewable"] = renewableDict[stateKey]["renewable"]

# print(formatJson(emmissionsDict))


############ Data USA #########################

# fEmissions = open("states-copy.json", "r").read()
# fPopulation = open("datausa.json", "r").read()
# populationDict = json.loads(fPopulation)
# emmissionsDict = json.loads(fEmissions)

# for pState in populationDict["data"]:
# 	for eStateKey in emmissionsDict.keys():
# 		eStateSentence = emmissionsDict[eStateKey]["emmisions"]["name"].lower()
# 		if pState["State"].lower() in eStateSentence:
# 			emmissionsDict[eStateKey]["population"] = pState["Population"]
# 			break

# print(formatJson(emmissionsDict))

################# State Area List (wikipedia) ####################
densityList = [("New Jersey", 1218),
("Rhode Island", 1021),
("Massachusetts", 871),
("Connecticut", 741),
("Maryland", 618),
("Delaware", 485),
("New York", 420),
("Florida", 378),
("Pennsylvania", 286),
("Ohio", 284),
("California", 251),
("Illinois", 231),
("Hawaii", 222),
("Virginia", 212),
("North Carolina", 206),
("Indiana", 184),
("Georgia", 177),
("Michigan", 175),
("South Carolina", 162),
("Tennessee", 160),
("New Hampshire", 148),
("Kentucky", 112),
("Louisiana", 108),
("Washington", 107),
("Wisconsin", 106),
("Texas", 105),
("Alabama", 95),
("Missouri", 88),
("West Virginia", 76),
("Minnesota", 68),
("Vermont", 67),
("Mississippi", 63),
("Arizona", 60),
("Arkansas", 57),
("Oklahoma", 57),
("Iowa", 55),
("Colorado", 52),
("Maine", 43),
("Oregon", 41),
("Utah", 36),
("Kansas", 36),
("Nevada", 26),
("Nebraska", 24),
("Idaho", 20),
("New Mexico", 17),
("South Dakota", 11),
("North Dakota", 10),
("Montana", 7),
("Wyoming", 6),
("Alaska", 1)]
# areaList = [("Alaska", 570640.95),
# ("Texas", 261231.71),
# ("California", 155779.22),
# ("Montana", 145545.8),
# ("New Mexico", 121298.15),
# ("Arizona", 113594.08),
# ("Nevada", 109781.18),
# ("Colorado", 103641.89),
# ("Oregon", 95988.01),
# ("Wyoming", 97093.14),
# ("Michigan", 56538.9),
# ("Minnesota", 79626.74),
# ("Utah", 82169.62),
# ("Idaho", 82643.12),
# ("Kansas", 81758.72),
# ("Nebraska", 76824.17),
# ("South Dakota", 75811),
# ("Washington", 66455.52),
# ("North Dakota", 69000.8),
# ("Oklahoma", 68594.92),
# ("Missouri", 68741.52),
# ("Florida", 53624.76),
# ("Wisconsin", 54157.8),
# ("Georgia", 57513.49),
# ("Illinois", 55518.93),
# ("Iowa", 55857.13),
# ("New York", 47126.4),
# ("North Carolina", 48617.91),
# ("Arkansas", 52035.48),
# ("Alabama", 50645.33),
# ("Louisiana", 43203.9),
# ("Mississippi", 46923.27),
# ("Pennsylvania", 44742.7),
# ("Ohio", 40860.69),
# ("Virginia", 39490.09),
# ("Tennessee", 41234.9),
# ("Kentucky", 39486.34),
# ("Indiana", 35826.11),
# ("Maine", 30842.92),
# ("South Carolina", 30060.7),
# ("West Virginia", 24038.21),
# ("Maryland", 9707.24),
# ("Hawaii", 6422.63),
# ("Massachusetts", 7800.06),
# ("Vermont", 9216.66),
# ("New Hampshire", 8952.65),
# ("New Jersey", 7354.22),
# ("Connecticut", 4842.36),
# ("Delaware", 1948.54),
# ("Rhode Island", 1033.81)]
fEmissions = open("states-copy.json", "r").read()
emmissionsDict = json.loads(fEmissions)

for stateTuple in densityList:
	for eStateKey in emmissionsDict.keys():
		eStateSentence = emmissionsDict[eStateKey]["emmisions"]["name"].lower()
		if stateTuple[0].lower() in eStateSentence:
			emmissionsDict[eStateKey]["density"] = stateTuple[1]
			break


print(formatJson(emmissionsDict))