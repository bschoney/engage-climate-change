import shlex
import subprocess
import json
import ast

API_key = "iBQ3wOsDX1oO1qs1pvwnM4tHc9abEZ3rLggWyH9H"
climateChangeQuery = "\"climate change\""
climateChangeSubject = "Climate change and greenhouse gases"

def formatJson(unformatted):
	return json.dumps(unformatted, indent=4, sort_keys=True)

def getBill(billUri):
	cmd = '''curl "''' + billUri + '''" -H "X-API-Key: "''' + API_key
	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	unformatted = json.loads(stdout)
	return unformatted

def getSponsor(sponsorUri):
	cmd = '''curl "''' + sponsorUri + '''" -H "X-API-Key: "''' + API_key
	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	unformatted = json.loads(stdout)
	return unformatted

def getBillsByQuery(query, page):
	cmd = '''curl "https://api.propublica.org/congress/v1/bills/search.json?query=''' + query + '''" -H "X-API-Key: "''' + API_key
	# Individual bill lookup
	# cmd = '''curl "https://api.propublica.org/congress/v1/116/bills/s1790.json"''' + ''' -H "X-API-Key: "''' + API_key
	
	cmd = '''curl "https://api.propublica.org/congress/v1/bills/search.json?'''
	cmd += '''offset=''' + str(page * 20)
	cmd += '''&query=''' + query # For searching all bills
	cmd += '''" -H "X-API-Key: "''' + API_key
	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	try:
		# stdout = json.dumps(x.decode("utf-8"))
		unformatted = json.loads(stdout)
		return unformatted
	except:
		return stdout

def getBillsBySubject(subject, page):
	cmd = '''curl "https://api.propublica.org/congress/v1/bills/search.json?query=''' + query + '''" -H "X-API-Key: "''' + API_key
	cmd = '''curl "https://api.propublica.org/congress/v1/bills/subjects/"'''
	cmd += subject + '''".json?'''
	cmd += '''offset=''' + str(page * 20)
	cmd += '''" -H "X-API-Key: "''' + API_key

	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	unformatted = json.loads(stdout)
	return unformatted

##################### GET BILLS #########################
# allBills = []
# for page in range(0,48):
# 	try:
# 		if page == 44:
# 			continue
# 		for bills in getBillsByQuery(climateChangeQuery, page)["results"]:
# 			bills = bills["bills"]

# 			for bill in bills:
# 				if bill["primary_subject"].lower() != "environmental protection":
# 					# print(formatJson(bill))
# 					bills.remove(bill)
# 				else:
# 					f = open(bill["bill_id"] + ".json", "a")
# 					f.write(formatJson(bill))
# 					f.close()
# 					print(str(formatJson(bill)) + ", ")
# 			allBills += bills
# 	except:
# 		print("Exception from page:", page)
# 		break


############### GET SPONSORS ##########################
# f = open("all_bills.json", "r").read()
# myJsonList = json.loads(f)
# allSponsors = []
# for bill in myJsonList:
# 	sponsorUri = bill["sponsor_uri"]
# 	if sponsorUri not in allSponsors:
# 		allSponsors.append(sponsorUri)

# for sponsorUri in allSponsors:
# 	sponsorUnformatted = getSponsor(sponsorUri)["results"][0]
# 	print(formatJson(sponsorUnformatted) + ", ")
# 	f = open(sponsorUnformatted["member_id"] + ".json", "a")
# 	f.write(formatJson(sponsorUnformatted))
# 	f.close()
