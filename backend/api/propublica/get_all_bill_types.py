from io import FileIO
import json

if __name__ == '__main__':
    all_bills = FileIO("all_bills.json")
    json_bills = json.loads(all_bills.read())
    result = set()
    for bill in json_bills:
        result.add(bill["bill_type"])
    print(result)