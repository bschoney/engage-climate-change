class User:
	def __init__(self, json):
		self.avatar_url = json["avatar_url"]
		self.id = json["id"]
		self.name = json["name"]
		self.state = json["state"] # e.g. "active"
		self.username = json["username"]
		self.web_url = json["web_url"]

class Issue:
	def __init__(self, json):
		self.dictionary = dict(json)

		self.assignees = [User(a) for a in json["assignees"]]	# List of assignees
		self.author = User(json["author"])
		self.closed_at = json["closed_at"]
		self.closed_by = json["closed_by"]
		self.confidential = json["confidential"]
		self.created_at = json["created_at"]
		self.description = json["description"]
		self.discussion_locked = json["discussion_locked"]
		self.downvotes = json["downvotes"]
		self.due_date = json["due_date"]
		self.has_tasks = json["has_tasks"]
		self.id = json["id"]	# Overall issue id e.g. 22103013
		self.iid = json["iid"]	# Local issue id e.g. 11
		self.labels = json["labels"]	# list of labels
		self.merge_requests_count = json["merge_requests_count"]
		self.milestone = json["milestone"]
		self.project_id = json["project_id"]
		self.state = json["state"]
		self.subscribed = json["subscribed"]
		# ''' These two have sub-attributes and aren't really necessary'''
		# self.task_completion_status = json["task_completion_status"]
		# self.time_stats = json["time_stats"]
		self.title = json["title"]
		self.updated_at = json["updated_at"]
		self.upvotes = json["upvotes"]
		self.user_notes_count = json["user_notes_count"]
		self.web_url = json["web_url"]
		self.weight = json["weight"]