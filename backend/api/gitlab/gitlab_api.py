import shlex
import subprocess
import json

API_key = open("PRIVATE_KEY", "r").read()
project_id = 12956347

def getIssuesList():
	'''cURL not Wget'''
	global project_id
	cmd = '''curl --header "PRIVATE-TOKEN: '''+ API_key + '''" https://gitlab.com/api/v4/projects/'''
	cmd += str(project_id)
	cmd += "/issues"
	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	parsed = json.loads(stdout)
	# print(json.dumps(parsed, indent=4, sort_keys=True))
	return [dict(i) for i in parsed]

def getCommitsList():
	global project_id
	cmd = '''curl --header "PRIVATE-TOKEN: '''+ API_key + '''" https://gitlab.com/api/v4/projects/'''
	cmd += str(project_id)
	cmd += "/repository/commits"
	args = shlex.split(cmd)
	process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	parsed = json.loads(stdout)
	# print(json.dumps(parsed, indent=4, sort_keys=True))
	return [dict(i) for i in parsed]

def getCommitCount(first_name):
	# Count commits for each person
	commitCounts = {}	# Emails as keys
	for commit in getCommitsList():
		name = commit["committer_email"]
		if name in commitCounts:
			commitCounts[name] += 1
		else:
			commitCounts[name] = 1

	# for author, count in commitCounts.items():
	# 	print(author, count)

	first_name = first_name.lower()
	try:
		if first_name == "david":
			return commitCounts["davidcdo12@utexas.edu"]
		elif first_name == "tony":
			return commitCounts["gonelli@cs.utexas.edu"]
		elif first_name == "barrett":
			return commitCounts["bschoney@utexas.edu"]
		elif first_name == "ansh":
			return commitCounts[""]
		elif first_name == "jack":
			return commitCounts[""]
		elif first_name == "ben":
			return commitCounts[""]
		else:
			return 0
	except:
		return 0

def getCloseCount(first_name):
	# Count commits for each person
	closeCounts = {}	# Usernames as keys
	for issue in getIssuesList():
		closer = issue["closed_by"]
		if closer is not None:
			username = closer["username"]
			if username in closeCounts:
				closeCounts[username] += 1
			else:
				closeCounts[username] = 1

	# for author, count in closeCounts.items():
	# 	print(author, count)

	first_name = first_name.lower()
	try:
		if first_name == "david":
			return closeCounts["davidcdo12"]
		elif first_name == "tony":
			return closeCounts["Gonelli"]
		elif first_name == "barrett":
			return closeCounts["bschoney"]
		elif first_name == "ansh":
			return closeCounts["anshpujara"]
		elif first_name == "jack":
			return closeCounts[""]
		elif first_name == "ben":
			return closeCounts[""]
		else:
			return 0
	except:
		return 0

##########################################################################
############################ Run code here ###############################
##########################################################################
# print(json.dumps(getCommitsList(), indent=4, sort_keys=True))
# print(getCommitsList())
if __name__== "__main__":
	groupMembers = ["David", "Tony", "Barrett", "Ansh", "Ben", "Jack"]

	print("")
	for member in groupMembers:
		print(member + " closed: " + str(getCloseCount(member)))
		print(member + " committed: " + str(getCommitCount(member)))

# print("Barrett closed:", getCloseCount("barrett"))
# print("Barrett committed:", getCommitCount("barrett"))

#api_url = "https://gitlab.com/api/v4/projects/" + API_key
#response = requests.request("GET", api_url + "/issues").json
#print(response)
