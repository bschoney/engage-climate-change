{
    "_links": {
        "award_emoji": "https://gitlab.com/api/v4/projects/12956347/issues/11/award_emoji",
        "notes": "https://gitlab.com/api/v4/projects/12956347/issues/11/notes",
        "project": "https://gitlab.com/api/v4/projects/12956347",
        "self": "https://gitlab.com/api/v4/projects/12956347/issues/11"
    },
    "assignee": null,
    "assignees": [],
    "author": {
        "avatar_url": "https://secure.gravatar.com/avatar/c89fddd28be759b6b57e154672ba0c92?s=80&d=identicon",
        "id": 4053570,
        "name": "Barrett Schonefeld",
        "state": "active",
        "username": "bschoney",
        "web_url": "https://gitlab.com/bschoney"
    },
    "closed_at": null,
    "closed_by": null,
    "confidential": false,
    "created_at": "2019-06-20T23:00:03.047Z",
    "description": "",
    "discussion_locked": null,
    "downvotes": 0,
    "due_date": null,
    "has_tasks": false,
    "id": 22109413,
    "iid": 11,
    "labels": [
        "Front-end"
    ],
    "merge_requests_count": 0,
    "milestone": null,
    "project_id": 12956347,
    "state": "opened",
    "subscribed": false,
    "task_completion_status": {
        "completed_count": 0,
        "count": 0
    },
    "time_stats": {
        "human_time_estimate": null,
        "human_total_time_spent": null,
        "time_estimate": 0,
        "total_time_spent": 0
    },
    "title": "Style the webpage using CSS",
    "updated_at": "2019-06-20T23:00:03.047Z",
    "upvotes": 0,
    "user_notes_count": 0,
    "web_url": "https://gitlab.com/bschoney/engage-climate-change/issues/11",
    "weight": null
}