import requests
import json
import unittest

ourURL = "https://api.engageclimatechange.world"

class apiTests(unittest.TestCase) :
	def testRoot(self):
		response = requests.request("GET", ourURL)
		assert(response.ok)

	def testPoliticians(self):
		response = requests.request("GET", ourURL + "/politicians")
		assert(response.ok)

	def testPoliticiansResults(self):
		response = requests.request("GET", ourURL + "/politicians")
		model = json.loads(response.text)
		for politician in model["objects"]:
			assert(politician["our_id"] < model["num_results"])

	def testBills(self):
		response = requests.request("GET", ourURL + "/bills")
		model = json.loads(response.text)
		for bill in model["objects"]:
			assert(bill["id"] < model["num_results"])

	def testBillsResults(self):
		response = requests.request("GET", ourURL + "/bills")
		assert(response.ok)

	def testStates(self):
		response = requests.request("GET", ourURL + "/states")
		assert(response.ok)

	def testStatesResults(self):
		response = requests.request("GET", ourURL + "/states")
		model = json.loads(response.text)
		assert(model["num_results"] == 50)
		assert(model["objects"][0]["id"] == "AK")

	def testStatesDetailed(self):
		response = requests.request("GET", ourURL + "/states/TX")
		assert(response.ok)

	def testStatesDetailedAttributes(self):
		response = requests.request("GET", ourURL + "/states/TX")
		model = json.loads(response.text)
		expected_attributes = ["bills", "emissions", "emissions_rec", "id", "landarea", "politicians", 
			"pop", "pop_density", "renewables", "renewables_rec", "statename"]
		for e in expected_attributes:
			assert(e in model)

	def testBillsDetailed(self):
		response = requests.request("GET", ourURL + "/bills/0")
		assert(response.ok)

	def testBillsDetailedAttributes(self):
		response = requests.request("GET", ourURL + "/bills/0")
		model = json.loads(response.text)
		expected_attributes = ["bill_id", "chamber", "govtrack", "id", "introduced", "last_vote", 
			"latest_action", "name", "politician_id", "sponsor", "sponsor_party", "state", "state_id", "summary"]
		for e in expected_attributes:
			assert(e in model)

	def testPoliticiansDetailed(self):
		response = requests.request("GET", ourURL + "/politicians")
		model = json.loads(response.text)
		pol_id = model["objects"][0]["id"]
		response = requests.request("GET", ourURL + "/politicians/" + pol_id)
		assert(response.ok)
		assert(pol_id == "B000711")
	
	def testPoliticiansDetailedAttributes(self):
		response = requests.request("GET", ourURL + "/politicians")
		model = json.loads(response.text)
		pol_id = model["objects"][0]["id"]
		response = requests.request("GET", ourURL + "/politicians/" + pol_id)
		model = json.loads(response.text)
		expected_attributes = ["bills", "dob", "facebook", "first_name", "id", "in_office", 
			"last_name", "our_id", "party", "recent_vote", "state", "state_id", "twitter", "url", "youtube"]
		for e in expected_attributes:
			assert(e in model)

if __name__ == '__main__':
	unittest.main()

